<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DevelopmentController extends MainController {

	/**
	 * Display a development page.
	 *
	 * @return View
	 */
	public function index() {
        $this->setCurrentLang();
        return view('development');
	}

}
