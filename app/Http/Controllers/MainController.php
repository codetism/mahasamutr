<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class MainController extends Controller {

    public function setCurrentLang() {
        if(\session('locale') == 'en') {
            App::setLocale('en');
        } else {
            App::setLocale('th');
        }
    }

    public function en() {
        Session::set('locale', 'en');
        return Redirect::back();
    }

    public function th() {
        Session::set('locale', 'th');
        return Redirect::back();
    }

}
