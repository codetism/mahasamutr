<?php namespace App\Http\Controllers;

use App\ConstructionUpdate;
use App\News;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class WelcomeController extends MainController {

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index() {
	    $this->setCurrentLang();
	    $featured_news = News::latest()->whereIsPublished('1')->first();
	    $featured_update = ConstructionUpdate::latest()->whereIsPublished('1')->first();
        return view('welcome', compact('featured_news', 'featured_update'));
    }

    public function home() {
		return Redirect::action('WelcomeController@index');
	}

}
