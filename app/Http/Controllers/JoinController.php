<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class JoinController extends MainController {

	/**
	 * Display a join-me page.
	 *
	 * @return View
	 */
	public function index() {
        $this->setCurrentLang();
        return view('joinme');
	}

}
