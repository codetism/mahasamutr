<?php namespace App\Http\Controllers;

use App\ConstructionUpdate;
use App\News;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


class NewsController extends MainController {

	/**
	 * Display a news page.
	 *
	 * @return View
	 */


	public function index() {

        $this->setCurrentLang();
        $news = News::latest()->published()->get();
        $constructions = ConstructionUpdate::latest()->published()->get();

		return view('news', compact("constructions", "news"));

	}


    public function detail($id) {
        $news = News::find($id);

        $news_galleries = $news->gallery;

        return view('news', compact('news_galleries'));
    }




}
