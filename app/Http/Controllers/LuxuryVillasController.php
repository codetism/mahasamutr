<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class LuxuryVillasController extends MainController {

	/**
	 * Display a villas page.
	 *
	 * @return View
	 */
	public function index() {
        $this->setCurrentLang();
        return view('luxury_villas');
	}

}
