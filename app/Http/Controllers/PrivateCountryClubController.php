<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PrivateCountryClubController extends MainController {

	/**
	 * Display a private country club page.
	 *
	 * @return View
	 */
	public function index() {
	    $this->setCurrentLang();
		return view('private_country_club');
	}

}
