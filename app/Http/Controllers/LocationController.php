<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class LocationController extends MainController {

	/**
	 * Display a location page.
	 *
	 * @return View
	 */
	public function index() {
	    $this->setCurrentLang();
		return view('location');
	}

}
