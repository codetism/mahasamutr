<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AboutController extends MainController {

	/**
	 * Display about us page.
	 *
	 * @return View
	 */
	public function index() {
	    $this->setCurrentLang();
		return view('about');
	}

}
