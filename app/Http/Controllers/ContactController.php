<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ContactController extends MainController {

	/**
	 * Display a contact us page.
	 *
	 * @return View
	 */
	public function index() {
        $this->setCurrentLang();
        return view('contact');
	}

}
