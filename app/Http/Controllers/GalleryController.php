<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class GalleryController extends MainController {

	/**
	 * Display a gallery page.
	 *
	 * @return View
	 */
	public function index() {
        $this->setCurrentLang();
        return view('gallery');
	}

}
