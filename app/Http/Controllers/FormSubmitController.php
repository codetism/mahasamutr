<?php namespace App\Http\Controllers;

use App\Contact;
use App\Member;
use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;

class FormSubmitController extends Controller {

    /**
     * Send Contact Form.
     *
     * @param Request $request
     *
     * @return Redirect
     */
    public function send(Request $request) {
        $this->validate($request,
                            [
                                'c-title' => 'required',
                                'c-firstname' => 'required',
                                'c-lastname' => 'required',
                                'c-mobile' => 'required',
                                'c-email' => 'required|email',
                                'c-interest' => 'required',
                                'c-hear' => 'required',
                                'c-message' => 'required'
                            ]
                        );

        $contact = new Contact();

        $contact->title      = $request->input('c-title');
        $contact->first_name = $request->input('c-firstname');
        $contact->last_name  = $request->input('c-lastname');
        $contact->mobile     = $request->input('c-mobile');
        $contact->email      = $request->input('c-email');
        $contact->address    = $request->input('c-address');
        $contact->interested = $request->input('c-interest');
        $contact->known_from = $request->input('c-hear');
        $contact->message    = $request->input('c-message');
        $contact->read       = false;

        $contact->save();
        $this->sendContactEmail($request);

        return Redirect::action('ContactController@index');
    }

    /**
     * Send contact e-mail
     *
     * @param Request $request
     *
     * @return void
     */
    public function sendContactEmail(Request $request) {
        $title      = $request->input('c-title');
        $first_name = $request->input('c-firstname');
        $last_name  = $request->input('c-lastname');
        $phone      = $request->input('c-mobile');
        $email      = $request->input('c-email');
        $address    = $request->input('c-address');
        $interest   = $request->input('c-interest');
        $hear       = $request->input('c-hear');
        $content    = $request->input('c-message');

        $dest = env('CONTACT_MAIL');

        Mail::send('contact_form',
                    [
                        'title'     => $title,
                        'first_name'=> $first_name,
                        'last_name' => $last_name,
                        'phone'     => $phone,
                        'email'     => $email,
                        'address'   => $address,
                        'interest'  => $interest,
                        'hear'      => $hear,
                        'content'   => $content,
                    ],
                    function ($m) use ($dest, $first_name, $last_name) {
            $m->from('info@mahasamutr.com', 'MahaSamutr Website');
            $m->to($dest)->subject('MahaSamutr - Contact from ' . $first_name . ' ' . $last_name);
        });
    }

    /**
     * Subscribe
     *
     * @param Request $request
     *
     * @return Redirect
     */
    public function subscribe(Request $request) {
        $this->validate($request, ['ns-email' => 'required|email']);
        $email =  $request->input('ns-email');
        if (!Subscriber::whereEmail($email)->exists()) {
            $response = $this->sendEmailSubscriber($email);
            if($response) {
                $subscriber = new Subscriber();
                $subscriber->email = $email;
                $subscriber->save();
            }
        }
        return Redirect::back();
    }

    /**
     * Send a subscribe e-mail back to customer
     *
     * @param string $email
     *
     * @return boolean
     */
    public function sendEmailSubscriber($email) {
        $subject = 'MahaSamutr - You are now subscribed!'; //INSERT SUBJECT HERE
        Mail::send('subscribe_feedback', ['email' => $email], function ($m) use ($email, $subject) {
            $m->from('info@mahasamutr.com', 'MahaSamutr');
            $m->to($email)->subject($subject);
        });
        if(count(Mail::failures()) > 0){
            return false;
        }
        return true;
    }

    /**
     * Member Register
     *
     * @param Request $request
     *
     * @return Redirect
     */
    public function join(Request $request) {
        $this->validate($request,
            [
                'c-title' => 'required',
                'c-firstname' => 'required',
                'c-lastname' => 'required',
                'c-mobile' => 'required',
                'c-email' => 'required|email',
                'c-interest' => 'required',
                'c-hear' => 'required',
                'c-message' => 'required'
            ]
        );

        $member = new Member();

        $title      = $member->title      = $request->input('c-title');
        $first_name = $member->first_name = $request->input('c-firstname');
        $last_name  = $member->last_name  = $request->input('c-lastname');
        $mobile     = $member->mobile     = $request->input('c-mobile');
        $email      = $member->email      = $request->input('c-email');
        $address    = $member->address    = $request->input('c-address');
        $interested = $member->interested = $request->input('c-interest');
        $known_from = $member->known_from = $request->input('c-hear');
        $message    = $member->message    = $request->input('c-message');

        $member->save();

        $this->sendJoinEmail($title,
                             $first_name,
                             $last_name,
                             $mobile,
                             $email,
                             $address,
                             $interested,
                             $known_from,
                             $message
                            );

        return Redirect::action('JoinController@index');
    }

    /**
     * Send a subscribe e-mail back to customer and notification e-mail to admin
     *
     * @param $title
     * @param $first_name
     * @param $last_name
     * @param $mobile
     * @param $email
     * @param $address
     * @param $interested
     * @param $known_from
     * @param $message
     *
     * @return void
     */
    public function sendJoinEmail($title,
                                  $first_name,
                                  $last_name,
                                  $mobile,
                                  $email,
                                  $address,
                                  $interested,
                                  $known_from,
                                  $message) {
        $subject = 'MahaSamutr - ' . $title . ' ' . $first_name . ' ' . $last_name . ' is now registered!'; //INSERT SUBJECT HERE
        Mail::send('register_feedback', ['first_name' => $first_name, 'last_name' => $last_name, 'email' => $email], function ($m) use ($email) {
            $m->from('info@mahasamutr.com', 'MahaSamutr');
            $m->to($email)->subject('MahaSamutr - Thank You For Your Registration');
        });
        Mail::send('user_join', ['title' => $title, 'first_name' => $first_name, 'last_name' => $last_name, 'mobile' => $mobile, 'email' => $email, 'address' => $address, 'interested' => $interested, 'known_from' => $known_from, 'content' => $message], function ($m) use ($subject) {
            $m->from('info@mahasamutr.com', 'MahaSamutr');
            $m->to(env('CONTACT_MAIL'))->subject($subject);
        });
    }

}
