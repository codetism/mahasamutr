<?php namespace App\Http\Controllers\Admin;

use App\News;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\ImageUploadContract;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use Session;
use File;

class NewsController extends Controller {


    private $upload_path = 'uploads/images/news';

    /** @var News $news */
    protected $news;

    /** @var ImageUploadContract $imageUpload */
    protected $imageUpload;

    /**
     * Inject the model.
     * @param ConstructionUpdate $construction
     * @param ImageUploadContract $imageUpload
     */

    public function __construct(News $news, ImageUploadContract $imageUpload)	{
        $this->news = $news;
        $this->imageUpload = $imageUpload;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $news = $this->news->all();
        return view('admin.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()	{
        $news = new News();
        return view('admin.news.show' , compact('news'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)	{

        $input =  $this->getRequestData($request);

        if(strlen($input['detail_thai']) == 0 && strlen($input['detail_english']) == 0){
            Session::flash('error', 'Detail field is required');
            return Redirect::route('admin.news.create');
        }

        if(strlen($input['headline_thai']) == 0 && strlen($input['headline_english']) == 0){
            Session::flash('error', 'Headline field is required');
            return Redirect::route('admin.news.create');
        }

        if(strlen($input['excerpt_thai']) > 255 || strlen($input['excerpt_english']) > 255) {
            flash()->error('Excerpt is longer than 255 characters.');
            return Redirect::route('admin.news.create');
        }

        $news = $this->news->create($input);
        $id = $news->id;

        if($news) {

            if($request['saveAndClose']) {
                Session::flash('flash_message', 'Create Success !!!');
                return Redirect::action('Admin\NewsController@index');
            }

            if($request['saveAndNew']) {
                Session::flash('flash_message', 'Create Success !!!');
                return Redirect::action('Admin\NewsController@create');
            }
            if($request['addImage']) {
                $gallery = $news->gallery;
                return view('admin.news.gallery' , compact('gallery' , 'news'));
            }

            Session::flash('flash_message', 'Create Success !!!');
            return Redirect::action('Admin\NewsController@edit', $id);

        }else {
            Session::flash('error', 'Create Error !!!');
            $news = $input;
            return view('admin.news.show' , compact('news'));
        }

    }

    //-----------------
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $news = News::find($id);
        if(!$news->read) {
            $news->read = true;
            $news->update();
        }
        return view('admin.news.show', compact('news'));
    }


    public function edit($id)	{
        $news = $this->news->find($id);
        if($news == null)
            return Redirect::action('Admin\NewsController@create');

        return view('admin.news.show' , compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id , Request $request)	{
        /** @var News $news */
        $news = $this->news->find($id);

        if($news) {
            $input =  $this->getRequestData($request , $news);

            if(strlen($input['detail_thai']) == 0 && strlen($input['detail_english']) == 0){
                Session::flash('error', 'Detail field is required');
                return Redirect::route('admin.news.edit', $id);
            }

            if(strlen($input['headline_thai']) == 0 && strlen($input['headline_english']) == 0){
                Session::flash('error', 'Headline field is required');
                return Redirect::route('admin.news.edit', $id);
            }

            $news->update($input);
            Session::flash('flash_message', 'Updated complete!!!');

            if($request['saveAndClose'])
                return Redirect::action('Admin\NewsController@index');
            if($request['saveAndNew'])
                return Redirect::action('Admin\NewsController@create');
            return Redirect::action('Admin\NewsController@edit', array('id' => $news->id));


        }	else 	{
            Session::flash('error', 'Item is no longer exist !!!');
            return Redirect::action('Admin\NewsController@create');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)	{
        $news = $this->news->find($id);
        if (!$news) 	{
            Session::flash('error', 'Item is no longer exist !!!');
            return Redirect::action('Admin\NewsController@index');
        }

        if($news['thumbnail'])	{
            $this->removeFile($news['thumbnail']);
        }
        $this->news->destroy($id);
        Session::flash('flash_message', 'Successfully delete  ' . $news->headline_english .' !!!');

        return Redirect::action('Admin\NewsController@index');
    }

    public function publish(Request $request)	{

        $input =  $request->all();
        $news = $this->news->find($input['id']);


        if($news) {
            if(($input['is_published'] == 1) && !$news->published_at)
                $input['published_at'] = Carbon::now();

            $news->update($input);
        }	else 	{
            Session::flash('error', 'Item is no longer exist !!!');
        }
        return Redirect::action('Admin\NewsController@index');

    }



    public function getRequestData(Request $request , News $news = null )	{
        $input =  $request->all();

        if($news == null)
            $news = new News();

        if ($request->file('thumbnail')) {
            $result = $this->imageUpload->store($request, 'thumbnail', $this->upload_path);
            if ($result['result'] == true) {
                $input['thumbnail'] = $result['success'];
                if ($news->thumbnail && File::exists(public_path($news->thumbnail)))
                    File::delete(public_path($news->thumbnail));
            }
        }

        if(!array_key_exists( 'is_published' , $input))
            $input['is_published'] = 0;

        if(($input['is_published'] == 1) && !$news->published_at)
            $input['published_at'] = Carbon::now();

        if(!array_key_exists('display_th', $input) || strlen($input['headline_thai']) == 0 || strlen($input['detail_thai']) == 0)
            $input['display_th'] = 0;

        if(!array_key_exists('display_en', $input) || strlen($input['headline_english']) == 0 || strlen($input['detail_english']) == 0)
            $input['display_en'] = 0;

        if(array_key_exists( 'order' , $input) ) 	{
            $input['order'] = intval($input['order']);
            if($input['order'] == 0)
                unset($input['order']);
        }
        return $input;
    }

    private function removeFile($oldFile)	{
        if (is_string($oldFile) && strlen($oldFile) > 0) {
            if(file_exists($oldFile)){
                \File::Delete($oldFile);
            }
        }
    }




}
