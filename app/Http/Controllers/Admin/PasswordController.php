<?php namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\MessageBag;

class PasswordController extends Controller
{

    public function getChange()
    {
        return view('admin.password.change');
    }

    public function postChange(ChangePasswordRequest $request)
    {
        $user = Auth::user();
        $messages = new MessageBag;
        if (!app('hash')->check($request['current_password'], $user->password)) {
            $messages->add('current_password', 'Incorrect current password.');
        }
        if ($request['new_password'] != $request['confirm_password']) {
            $messages->add('new_password', 'The new passwords do not match.');
        }
        if (!$messages->isEmpty()) {
            return redirect()->back()->withErrors($messages);
        }

        $user->password = bcrypt($request['new_password']);
        $user->save();

        $messages->add('current_password', 'Password successfully changed.');
        return redirect()->back()->with('messages', $messages);
    }

}
