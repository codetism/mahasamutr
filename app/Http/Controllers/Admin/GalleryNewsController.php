<?php namespace App\Http\Controllers\Admin;

use App\GalleryNews;
use App\Http\Requests;
use App\News;
use App\Http\Controllers\Controller;
use App\Services\ImageUploadContract;
use Illuminate\Http\Request;
use Session;

class GalleryNewsController extends Controller {

	/** @var GalleryNews $gallery */
	private $gallery;

	/** @var ImageUploadContract $imageUpload */
	private $imageUpload;

	private $upload_path = 'uploads/images/news';

	/**
	 * Inject the model.
	 * @param GalleryNews $gallery
	 * @param News $news
	 * @param imageUploadContract $imageUpload
	 */
	public function __construct(GalleryNews $gallery, News $news, ImageUploadContract $imageUpload)
	{
		$this->gallery = $gallery;
		$this->news = $news;
		$this->imageUpload = $imageUpload;
	}

	public function index($id)
	{
		$news = $this->news->find($id);

		if(!$news)
			return redirect(action('Admin\NewsController@index'));

		$gallery = $news->gallery;
		return view('admin.news.gallery' , compact('gallery' , 'news'));
	}

	public function store(Request $request)
	{
		$input =  $request->all();
		if(!isset($input['news_id']))
		{
			Session::flash('error', 'News Id not found.');
			return redirect(action('Admin\NewsController@index'));
		}

		$news_id = intval($input['news_id']);
		$news = $this->news->find($news_id);
		if(!$news)
		{
			Session::flash('error', 'News Id not found.');
			return redirect(action('Admin\NewsController@index'));
		}

		$result = $this->imageUpload->store($request , 'image_file' , $this->upload_path);
		foreach($result['success'] as $image_url)
		{
			$data = ['image' => $image_url , 'news_id' => $news_id];

			$this->gallery->create($data);
		}

		if($result['result'] == true)
			Session::flash('flash_message', 'Add Success !!!');
		else
		{
			Session::flash('error', $result['message']);
			if(count($result['success']) > 0)
				Session::flash('flash_message', 'Add Success - ' . implode( $result['success']));

		}
		return redirect(route('adminNewsGallery', ['id' => $news_id]));
	}

	public function destroy($id, Request $request)
	{

		$news = $this->news->find($id);
		$input =  $request->all();
		if(!isset($input['ids']) ||  !$news)
		{
			Session::flash('error', 'News Id not found.');
			return redirect(action('Admin\NewsController@index'));
		}

		$id_array = explode(',' , $input['ids']);
		foreach ( $id_array as $gallery_id_str)
		{
			$gallery_id = intval($gallery_id_str);
			$gallery = $this->gallery->find($gallery_id);
			if($gallery) {
				$this->gallery->destroy($gallery_id);
			}
		}

		Session::flash('flash_message', 'Delete Success !!!');
		return redirect(route('adminNewsGallery', ['id' => $news->id]));
	}

}
