<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Subscriber;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class SubscriberController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index() {
        $subscribers = Subscriber::all();
        return view('admin.subscriber.index', compact('subscribers'));
    }

    public function destroy($id) {
        $subscriber = Subscriber::find($id);
        $subscriber->delete();
        return Redirect::action('Admin\SubscriberController@index');
    }

}
