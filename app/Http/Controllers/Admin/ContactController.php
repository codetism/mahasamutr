<?php namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ContactController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$messages = Contact::all();
        return view('admin.contact.index', compact('messages'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$message = Contact::find($id);
        if(!$message->read) {
            $message->read = true;
            $message->update();
        }
        return view('admin.contact.show', compact('message'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
        $message = Contact::find($id);
        $message->delete();
        return Redirect::action('Admin\ContactController@index');
	}

}
