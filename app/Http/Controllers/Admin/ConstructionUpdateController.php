<?php namespace App\Http\Controllers\Admin;

use App\ConstructionUpdate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\ImageUploadContract;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


use Session;
use File;

class ConstructionUpdateController extends Controller {


    private $upload_path = 'uploads/images/constructions_update';

    /** @var ConstructionUpdate $construction */
    protected $construction;

    /** @var ImageUploadContract $imageUpload */
    protected $imageUpload;

    /**
     * Inject the model.
     * @param ConstructionUpdate $construction
     * @param ImageUploadContract $imageUpload
    */

    public function __construct(ConstructionUpdate $construction, ImageUploadContract $imageUpload)	{
        $this->construction = $construction;
        $this->imageUpload = $imageUpload;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
        $construction = $this->construction->all();
        return view('admin.construction_update.index', compact('construction'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()	{
        $construction = new ConstructionUpdate();
        return view('admin.construction_update.show' , compact('construction'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)	{

        $input =  $this->getRequestData($request);

        if(strlen($input['detail_thai']) == 0 && strlen($input['detail_english']) == 0){
            Session::flash('error', 'Detail field is required');
            return Redirect::route('admin.construction_update.create');
        }

        if(strlen($input['headline_thai']) == 0 && strlen($input['headline_english']) == 0){
            Session::flash('error', 'Headline field is required');
            return Redirect::route('admin.construction_update.create');
        }

        if(strlen($input['excerpt_thai']) > 255 || strlen($input['excerpt_english']) > 255) {
            flash()->error('Excerpt is longer than 255 characters.');
            return Redirect::route('admin.construction_update.create');
        }

        $construction = $this->construction->create($input);
        $id = $construction->id;

        if($construction) {

            if($request['saveAndClose']) {
                Session::flash('flash_message', 'Create Success !!!');
                return Redirect::action('Admin\ConstructionUpdateController@index');
            }

            if($request['saveAndNew']) {
                Session::flash('flash_message', 'Create Success !!!');
                return Redirect::action('Admin\ConstructionUpdateController@create');
            }
            if($request['addImage']) {
                $gallery = $construction->gallery;
                return view('admin.construction_update.gallery' , compact('gallery' , 'construction'));
            }

            Session::flash('flash_message', 'Create Success !!!');
            return Redirect::action('Admin\ConstructionUpdateController@edit', $id);

        }else {
            Session::flash('error', 'Create Error !!!');
            $construction = $input;
            return view('admin.construction_update.show' , compact('construction'));
        }

    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$message = ConstructionUpdate::find($id);
        if(!$message->read) {
            $message->read = true;
            $message->update();
        }
        return view('admin.construction_update.show', compact('message'));
	}


    public function edit($id)	{
        $construction = $this->construction->find($id);
        if($construction == null)
            return Redirect::action('Admin\ConstructionUpdateController@create');

        return view('admin.construction_update.show' , compact('construction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id , Request $request)	{
        /** @var News $news */
        $construction = $this->construction->find($id);

        if($construction) {
            $input =  $this->getRequestData($request , $construction);

            if(strlen($input['detail_thai']) == 0 && strlen($input['detail_english']) == 0){
                Session::flash('error', 'Detail field is required');
                return Redirect::route('admin.construction_update.edit', $id);
            }

            if(strlen($input['headline_thai']) == 0 && strlen($input['headline_english']) == 0){
                Session::flash('error', 'Headline field is required');
                return Redirect::route('admin.construction_update.edit', $id);
            }

            $construction->update($input);
            Session::flash('flash_message', 'Updated complete!!!');

            if($request['saveAndClose'])
                return Redirect::action('Admin\ConstructionUpdateController@index');
            if($request['saveAndNew'])
                return Redirect::action('Admin\ConstructionUpdateController@create');
            return Redirect::action('Admin\ConstructionUpdateController@edit', array('id' => $construction->id));


        }	else 	{
            Session::flash('error', 'Item is no longer exist !!!');
            return Redirect::action('Admin\ConstructionUpdateController@create');

        }
    }

    /**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function destroy($id)	{
        $construction = $this->construction->find($id);
        if (!$construction) 	{
            Session::flash('error', 'Item is no longer exist !!!');
            return Redirect::action('Admin\ConstructionUpdateController@index');
        }

        if($construction['thumbnail'])	{
            $this->removeFile($construction['thumbnail']);
        }
        $this->construction->destroy($id);
        Session::flash('flash_message', 'Successfully delete  ' . $construction->headline_english .' !!!');

        return Redirect::action('Admin\ConstructionUpdateController@index');
    }

    public function publish(Request $request)	{

        $input =  $request->all();
        $construction = $this->construction->find($input['id']);


        if($construction) {
            if(($input['is_published'] == 1) && !$construction->published_at)
                $input['published_at'] = Carbon::now();

            $construction->update($input);
        }	else 	{
            Session::flash('error', 'Item is no longer exist !!!');
        }
        return Redirect::action('Admin\ConstructionUpdateController@index');

    }



    public function getRequestData(Request $request , ConstructionUpdate $construction = null )	{
        $input =  $request->all();

        if($construction == null)
            $construction = new ConstructionUpdate();

        if ($request->file('thumbnail')) {
            $result = $this->imageUpload->store($request, 'thumbnail', $this->upload_path);
            if ($result['result'] == true) {
                $input['thumbnail'] = $result['success'];
                if ($construction->thumbnail && File::exists(public_path($construction->thumbnail)))
                    File::delete(public_path($construction->thumbnail));
            }
        }

        if(!array_key_exists( 'is_published' , $input))
            $input['is_published'] = 0;

        if(($input['is_published'] == 1) && !$construction->published_at)
            $input['published_at'] = Carbon::now();

        if(!array_key_exists('display_th', $input) || strlen($input['headline_thai']) == 0 || strlen($input['detail_thai']) == 0)
            $input['display_th'] = 0;

        if(!array_key_exists('display_en', $input) || strlen($input['headline_english']) == 0 || strlen($input['detail_english']) == 0)
            $input['display_en'] = 0;

        if(array_key_exists( 'order' , $input) ) 	{
            $input['order'] = intval($input['order']);
            if($input['order'] == 0)
                unset($input['order']);
        }
        return $input;
    }

    private function removeFile($oldFile)	{
        if (is_string($oldFile) && strlen($oldFile) > 0) {
            if(file_exists($oldFile)){
                \File::Delete($oldFile);
            }
        }
    }



}
