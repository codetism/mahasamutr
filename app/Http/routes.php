<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('home', 'WelcomeController@home');
Route::get('about', 'AboutController@index');
Route::get('contact', 'ContactController@index');
Route::get('development', 'DevelopmentController@index');
Route::get('gallery', 'GalleryController@index');
Route::get('location', 'LocationController@index');
Route::get('luxury-villas', 'LuxuryVillasController@index');
Route::get('news', 'NewsController@index');
Route::get('private-country-club', 'PrivateCountryClubController@index');
Route::get('join-me', 'JoinController@index');

Route::post('subscribe', 'FormSubmitController@subscribe');
Route::post('send', 'FormSubmitController@send');
Route::post('join', 'FormSubmitController@join');

Route::get('en', 'MainController@en');
Route::get('th', 'MainController@th');

Route::get('/migrate/{key?}', function($key = null) {
    if($key == env('DB_PASSWORD')){
        try {
            echo '<br>init migrate:install...';
            Artisan::call('migrate');
            echo 'done migrate';
        } catch (Exception $e) {
            Response::make($e->getMessage(), 500);
        }
    }else{
        App::abort(404);
    }
});

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => 'admin',
], function () {
    Route::get('/', 'SubscriberController@index');
    Route::resource('message', 'ContactController');
    Route::resource('member', 'MemberController');
    Route::controller('password', 'PasswordController');


    Route::resource('construction_update', 'ConstructionUpdateController');
    Route::post('construction_update/publish', 'ConstructionUpdateController@publish');
    Route::delete('construction_update/{id}', 'ConstructionUpdateController@destroy');

    Route::resource('news', 'NewsController');
    Route::post('news/publish', 'NewsController@publish');
    Route::delete('news/{id}', 'NewsController@destroy');

    Route::resource('galleryNews', 'GalleryNewsController');
    Route::get('news/{id}/gallery', ['as' => 'adminNewsGallery', 'uses' => 'GalleryNewsController@index']);
    Route::get('news/{id}/gallery/reorder', ['as' => 'adminNewsGalleryReorder', 'uses' => 'GalleryNewsController@reorder']);
    Route::post('news/gallery', ['as' => 'adminNewsGallerySave', 'uses' => 'GalleryNewsController@store']);
    Route::delete('news/gallery/{id}', ['as' => 'adminNewsGalleryDestroy', 'uses' => 'GalleryNewsController@destroy']);
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
