<?php namespace App\Http\Middleware;

use Closure;

class Admin extends Authenticate
{
    /**
     * Only allow users with 'admin' role to pass
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            if (in_array('admin', array_fetch($this->auth->user()->roles->toArray(), 'name'))) {
                return $next($request);
            } else {
                return redirect()->to('/');
            }
        } else {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('auth/login');
            }
        }
    }
}
