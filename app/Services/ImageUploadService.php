<?php
/**
 * Created by IntelliJ IDEA.
 * User: Joy
 * Date: 5/20/15
 * Time: 4:38 PM
 */

namespace App\Services;


use Illuminate\Http\Request;
use PhpSpec\Util\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Webpatser\Uuid\Uuid;

class ImageUploadService implements ImageUploadContract
{

    public function processFile(File $file, $destination_directory){

        $valid_extensions = array("jpeg", "jpg", "png", "JPEG", "JPG", "PNG");
        $valid_types = array("image/png", "image/jpg", "image/jpeg", "image/PNG", "image/JPG", "image/JPEG");

        if(!in_array($file->getClientOriginalExtension(), $valid_extensions))
        {
            return ['result' => false , 'message' => $file->getClientOriginalExtension() . ' Invalid extension - ' . $file->getClientOriginalName()];
        }
        if(!in_array($file->getClientMimeType(), $valid_types))
        {
            return ['result' => false , 'message' => $file->getClientMimeType() .' Invalid file type - ' . $file->getClientOriginalName()];
        }

        if( $file->getSize() > (2 * 1024 * 1024))
        {
            return ['result' => false , 'message' => $file->getSize() .' File too large (maximum file size 2M) - ' . $file->getClientOriginalName()];
        }

        $file_name = str_replace("+", "_", str_replace("%", "X", urlencode($file->getClientOriginalName())));
        if(mb_strwidth($file_name) > 30)
            $file_name = mb_strimwidth($file_name, 0, 30, "") . '.' . $file->getClientOriginalExtension();

        $target = $file->move($destination_directory, Uuid::generate() . '-' . $file_name);

        return  ['result' => true,
                 'success' => $target->getPathname()];
    }

    /**
     * Store the uploaded file
     *
     * @param Request $request
     * @param $field_name
     * @param string $destination_directory
     * @return Array of result and message
      */
    public function store(Request $request, $field_name, $destination_directory = 'uploads/images/')
    {
        if (!$request->hasFile($field_name)) {
            return ['result' => false, 'message' => $field_name . ' Not Found', 'success' => array()];
        }

        if(!is_array($request->file($field_name))){

            if( !$request->file($field_name)->isValid())
                return ['result' => false, 'message' => $field_name . ' Not Found', 'success' => array()];

            return $this->processFile( $request->file($field_name) , $destination_directory);
        }

        $data = ['result' => true, 'message' => 'NO update'];
        $files = $request->file($field_name);
        $success_upload = array();
        foreach($files as $file){
           $data= $this->processFile($file, $destination_directory);
            if($data['result'] == false)
                return ['result' => false, 'message' => $data['message'] , 'success' => $success_upload];
            array_push($success_upload , $data['success']);
            $data = ['result' => true, 'success' => $success_upload];
        }
        return $data;

    }
}