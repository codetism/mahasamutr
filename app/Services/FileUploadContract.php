<?php
/**
 * Created by IntelliJ IDEA.
 * User: Iamz
 * Date: 5/11/15
 * Time: 3:44 PM
 */

namespace App\Services;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

interface FileUploadContract
{

    /**
     * Store the uploaded file
     *
     * @param Request $request
     * @param $field_name
     * @param $dest_directory
     * @return File
     */
    public function store(Request $request, $field_name, $dest_directory = null);
}