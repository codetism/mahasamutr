<?php
/**
 * Created by IntelliJ IDEA.
 * User: Iamz
 * Date: 5/11/15
 * Time: 3:52 PM
 */

namespace App\Services;


use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;
use Uuid;

class FileUploadService implements FileUploadContract
{

    /**
     * Store the uploaded file
     *
     * @param Request $request
     * @param $field_name
     * @param string $dest_directory
     * @return false|File
     * @throws \Exception
     */
    public function store(Request $request, $field_name, $dest_directory = 'uploads/images/')
    {
        if (!$request->hasFile($field_name) || !$request->file($field_name)->isValid()) {
            return false;
        }

        $file = $request->file($field_name);
        $ext = $file->guessExtension();
        $target = $file->move($dest_directory, Uuid::generate() . '.' . $ext);

        $target = str_replace('\\', '/', $target);

        return $target;
    }
}