<?php
/**
 * Created by IntelliJ IDEA.
 * User: Iamz
 * Date: 5/11/15
 * Time: 3:44 PM
 */

namespace App\Services;

use Illuminate\Http\Request;

interface ImageUploadContract
{

    /**
     * Store the uploaded file
     *
     * @param Request $request
     * @param $field_name
     * @param $destination_directory
     * @return Array of result and message
     */
    public function store(Request $request, $field_name, $destination_directory = null);
}