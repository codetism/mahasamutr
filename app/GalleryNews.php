<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
/**
 * App\GalleryNews
 *
 * @property integer $id
 * @property integer $news_id
 * @property string $image
 * @property string $description_en
 * @property string $description_th
 * @property integer $order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\GalleryNews whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GalleryNews whereNewsId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GalleryNews whereImageLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GalleryNews whereDescriptionEn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GalleryNews whereDescriptionTh($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GalleryNews whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GalleryNews whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GalleryNews whereUpdatedAt($value)
 * @property-read \App\News $news
 * @method static \Illuminate\Database\Query\Builder|\App\GalleryNews whereImage($value)
 * @property string $description_english
 * @method static \Illuminate\Database\Query\Builder|\App\GalleryNews whereDescriptionEnglish($value)
 * @property string $description_thai
 * @method static \Illuminate\Database\Query\Builder|\App\GalleryNews whereDescriptionThai($value)
 */
class GalleryNews extends Model {

	public $timestamps = true;
    
    protected $fillable = [
        'image',
        'order',
        'description_english',
        'description_thai',
        'news_id'
    ];

    public function skipTimestamp() {
        $this->timestamps = false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function news()  {
        return $this->belongsTo('App\News','news_id');
    }

    public function delete()    {
        if ($this->image && File::exists(public_path($this->image)))
            File::delete(public_path($this->image));
        parent::delete();
    }

    public function description()   {
        if ((Session::get('locale') != 'en') && (strlen($this->description_thai) > 0))
            return $this->description_thai;
        return $this->description_english;
    }
}
