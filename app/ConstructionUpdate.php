<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Illuminate\Support\Facades\Session;

/**
 * App\News
 *
 * @property integer $id
 * @property string $headline_english
 * @property string $headline_thai
 * @property string $excerpt_english
 * @property string $excerpt_thai
 * @property string $detail_english
 * @property string $detail_thai
 * @property string $slug
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $thumbnail
 * @property boolean $is_published
 * @property \Carbon\Carbon $published_at
 * @property integer $order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\GalleryNews')->orderBy('order[] $gallery
 * @method static \Illuminate\Database\Query\Builder|\App\News whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereHeadlineEnglish($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereHeadlineThai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereExcerptEnglish($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereExcerptThai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereDetailEnglish($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereDetailThai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereMetaKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereMetaDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereThumbnail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereIsPublished($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News wherePublishedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereUpdatedAt($value)
 */

class ConstructionUpdate extends Model {

    public $timestamps = true;


    protected $fillable = [
        'headline_english',
        'headline_thai',
        'detail_english',
        'detail_thai',
        'excerpt_english',
        'excerpt_thai',

        'thumbnail',
        'is_published',
        'published_at',
        'order',

        'display_th',
        'display_en',
    ];

    protected $dates = ['published_at'];


    public function skipTimestamp() {
        $this->timestamps = false;
    }

    public function detail() {
        if((Session::get('locale') == 'th') && (strlen($this->detail_thai) > 0))
            return $this->detail_thai;
        elseif((Session::get('locale') == 'en') && (strlen($this->detail_english) == 0))
            return $this->detail_thai;
        else
            return $this->detail_english;
    }

    public function excerpt() {
        if((Session::get('locale') == 'th') && (strlen($this->excerpt_thai) > 0))
            return $this->excerpt_thai;
        elseif((Session::get('locale') == 'en') && (strlen($this->excerpt_english) == 0))
            return $this->excerpt_thai;
        else
            return $this->excerpt_english;
    }

    public function headline() {
        if((Session::get('locale') == 'th') && (strlen($this->headline_thai) > 0))
            return $this->headline_thai;
        elseif((Session::get('locale') == 'en') && (strlen($this->headline_english) == 0))
            return $this->headline_thai;
        else
            return $this->headline_english;
    }

    public function delete() {
        if ($this->thumbnail && File::exists(public_path($this->thumbnail)))
            File::delete(public_path($this->thumbnail));
        foreach($this->gallery as $gallery_image)
        {
            GalleryNews::destroy($gallery_image->id);
        }
        parent::delete();
    }

    public function getTheDate() {
        $time = strtotime($this->published_at);
        if(Session::get('locale') == 'th') {
            $th_month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
            $month = $th_month[date('n', $time) - 1];
        } else {
            $month = date('F', $time);
        }
        $year = date('Y', $time);
        return $month . ' ' . $year;
    }

    public function scopePublished($query)  {
        $query->where('is_published', '1');
    }

    public function scopeLocale($query)  {
        if(Session::get('locale') == 'th')
            $query->where('display_th', '1');
        else
            $query->where('display_en', '1');
    }
}
