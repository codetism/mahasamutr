<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model {

    protected $fillable = [
        'title',
        'first_name',
        'last_name',
        'mobile',
        'email',
        'address',
        'interested',
        'known_from',
        'message'
    ];

}
