var ms_tab = {
  activeClass: 'current',
  containerSelector: '[ms-tab]',
  contentSelector: '[ms-tab-content]',
  triggerData: 'trigger',
  animationTime: 200,
  init:function(){
    $(ms_tab.containerSelector).each(function(){
      var container = $(this);
      container.find(ms_tab.contentSelector).hide();
      var triggerSelector = '[data-' + ms_tab.triggerData + ']';
      container.find(triggerSelector).each(function(i){
        $(this).on('click',function(e){
          e.preventDefault();

          ms_tab.showContent($(this).data(ms_tab.triggerData));
        });

        // default first tab
        (!i) ? $(this).trigger('click') : 0;
      });
    });
  },
  showContent:function(id){
    if($(id).length){
      var container = $(id).parents(ms_tab.containerSelector);

      // trigger
      var triggerSelector = '[data-' + ms_tab.triggerData + ']';
      container.find(triggerSelector).each(function(){
        var trigger = $(this);
        if(trigger.data(ms_tab.triggerData) == id){
          trigger.addClass(ms_tab.activeClass);
        }else{
          trigger.removeClass(ms_tab.activeClass);
        }
      })

      // content
      container.find(ms_tab.contentSelector).each(function(){
        var content = $(this);

        if(content.is(id)){
          content.fadeIn(ms_tab.animationTime,function(){
            $(this).addClass(ms_tab.activeClass);

            // slide
            $(this).find('[b-slider]').each(function(){
              var slick = $(this).slick('getSlick');
              slick.refresh();
            });

            // animation
            $(this).find('[b-animation]').addClass('active');

            // vdo
            $(this).find('iframe').each(function(){
              $(this).attr('src',$(this).data('src'));
            })
          });
        }else{
          $(this).hide(0,function(){
            $(this).removeClass(ms_tab.activeClass);

            // animation
            $(this).find('[b-animation]').removeClass('active');

            // vdo
            $(this).find('iframe').each(function(){
              $(this).attr('src','');
            })
          });
        }
      });
    }
  }
}

site.ready.push(ms_tab.init);
