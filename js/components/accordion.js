var ms_accordion = {
  activeClass: 'active',
  rootSelector: '[ms-accordion]',
  nodeSelector: '[ms-accordion-node]',
  countSelector: '[ms-accordion-count]',
  buttonSelector: '[ms-accordion-bt]',
  animationTime: 300,
  init:function(){
    if(!$(ms_accordion.rootSelector).length) return;

    $(ms_accordion.rootSelector + ' ' + ms_accordion.nodeSelector).hide();
    ms_accordion.addEvent();

    // default first element
    $(ms_accordion.rootSelector).each(function(){
      jQuery(this).find(ms_accordion.buttonSelector).eq(0).trigger('click');
    });

    ms_accordion.showNumber();
  },
  addEvent:function(){
    $(ms_accordion.rootSelector + ' ' + ms_accordion.buttonSelector).on('click',function(e){
      e.preventDefault();

      var _this = $(this);
      var sideBarRoot = $(this).parents(ms_accordion.rootSelector);
      var currentLevel = $(this).parents(ms_accordion.nodeSelector).length;
      var currentList = $(this).siblings(ms_accordion.nodeSelector);

      // current
      if(!currentList.hasClass(ms_accordion.activeClass)){

        currentList.slideDown(ms_accordion.animationTime,function(){
          $(this).addClass(ms_accordion.activeClass);

        });

        $(this).addClass(ms_accordion.activeClass);
      }else{
        currentList.slideUp(ms_accordion.animationTime,function(){
          $(this).removeClass(ms_accordion.activeClass);

        });

        $(this).removeClass(ms_accordion.activeClass);
      }

      // hide previous
      sideBarRoot.find(ms_accordion.nodeSelector + '.' + ms_accordion.activeClass).each(function(){
        var level = $(this).parents(ms_accordion.nodeSelector).length;
        if(level >= currentLevel && currentList.get(0) != this){

          $(this).slideUp(ms_accordion.animationTime,function(){
            $(this).removeClass(ms_accordion.activeClass);
            $(this).find('[b-animation]').removeClass('active');
          });



          $(this).siblings(ms_accordion.buttonSelector).removeClass(ms_accordion.activeClass);
        }
      });
    });
  },
  removeEvent: function(){
    $(ms_accordion.rootSelector + ' ' + ms_accordion.buttonSelector).off('click');
  },
  showNumber: function(){
    $(ms_accordion.countSelector).each(function(){
      var btSelector = $(this).closest($(ms_accordion.buttonSelector));
      var currentList = btSelector.siblings(ms_accordion.nodeSelector);
      var count = currentList.children().length;

      $(this).html(count);
    });
  }
}

site.ready.push(ms_accordion.init);
