<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('gallery_news', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('news_id')->unsigned();;
            $table->string('image');
            $table->string('description_english')->nullable();
            $table->string('description_thai')->nullable();
            $table->integer('order')->default(1000);;
            $table->timestamps();


            $table->foreign('news_id')
                ->references('id')
                ->on('news');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('gallery_news');
	}

}
