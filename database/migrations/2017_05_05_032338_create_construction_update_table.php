<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstructionUpdateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('construction_updates', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('headline_english');
            $table->string('headline_thai');
            $table->longText('detail_english');
            $table->longText('detail_thai');
            $table->string('thumbnail')->nullable();

            $table->tinyInteger('is_published')->default(0);
            $table->timestamp('published_at')->nullable();

            $table->string('excerpt_english');
            $table->string('excerpt_thai');

            $table->tinyInteger('display_en')->default(1);
            $table->tinyInteger('display_th')->default(1);

            $table->integer('order')->default(1000);

            $table->timestamps();

        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('construction_updates');
	}

}
