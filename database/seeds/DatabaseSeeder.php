<?php

use App\Role;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
    }

}

class UsersRolesSeeder extends Seeder {

    public function run()
    {
        DB::table('users_roles')->delete();
        DB::table('users')->delete();
        DB::table('roles')->delete();

        $user = User::create(['name' => 'admin', 'email' => 'admin@example.com', 'password' => bcrypt('admin')]);

        $role = Role::create(['name' => 'admin']);

        $user->roles()->attach($role->id);
    }
}
