var ms_gallery = {
  gridSelector: '#ms-gallery',
  itemSelector: '#ms-gallery .c-block',
  shuffle: '',
  init: function(){
    if(!jQuery(ms_gallery.gridSelector).length) return;

    ms_gallery.initShuffle();
    ms_gallery.initFilter();
    ms_gallery.initGallery();
  },
  initShuffle: function(){
    var Shuffle = window.shuffle;

    Shuffle.ShuffleItem.Scale.INITIAL = 0.9;
    Shuffle.ShuffleItem.Scale.HIDDEN = 0.9;
    Shuffle.ShuffleItem.Scale.VISIBLE = 1;

    Shuffle.options = {
      group: Shuffle.ALL_ITEMS, // Initial filter group.
      speed: 600, // Transition/animation speed (milliseconds).
      easing: 'cubic-bezier(0.645, 0.045, 0.355, 1)', // CSS easing function to use.
      itemSelector: '*', // e.g. '.picture-item'.
      sizer: null, // Element or selector string. Use an element to determine the size of columns and gutters.
      gutterWidth: 0, // A static number or function that tells the plugin how wide the gutters between columns are (in pixels).
      columnWidth: 0, // A static number or function that returns a number which tells the plugin how wide the columns are (in pixels).
      delimeter: null, // If your group is not json, and is comma delimeted, you could set delimeter to ','.
      buffer: 1, // Useful for percentage based heights when they might not always be exactly the same (in pixels).
      columnThreshold: 0.01, // Reading the width of elements isn't precise enough and can cause columns to jump between values.
      initialSort: null, // Shuffle can be initialized with a sort object. It is the same object given to the sort method.
      // throttle: throttle, // By default, shuffle will throttle resize events. This can be changed or removed.
      throttleTime: 300, // How often shuffle can be called on resize (in milliseconds).
      staggerAmount: 15, // Transition delay offset for each item in milliseconds.
      staggerAmountMax: 250, // Maximum stagger delay in milliseconds.
      useTransforms: true, // Whether to use transforms or absolute positioning.
    };


    var element = jQuery(ms_gallery.gridSelector)[0];
    var sizer = jQuery(ms_gallery.itemSelector)[0];

    ms_gallery.shuffle = new Shuffle(element, {
      itemSelector: '.c-block',
      sizer: sizer
    });
  },
  initFilter: function(){
    jQuery('.gallery-tab li a').on('click',function(){
      jQuery('.gallery-tab li a').removeClass('current');
      jQuery(this).addClass('current');
      var group = jQuery(this).data('group');
      ms_gallery.shuffle.filter(group);
    });

    jQuery('#submenu-luxury').on('click',function(){
      jQuery('.gallery-tab li a').eq(0).trigger('click');
    });

    jQuery('#submenu-country').on('click',function(){
      jQuery('.gallery-tab li a').eq(1).trigger('click');
    });

    jQuery('.gallery-tab li a').eq(0).trigger('click');
  },
  initGallery: function(){

    $('#gallery-modal .content').on('afterChange',function(event,slick,current){

      slick.$slides.each(function(index,item){
        var $iframe = jQuery(item).find('iframe');

        if(jQuery(item).is('.slick-current')){
          $iframe.attr('src',$iframe.data('src'));
        }else{
          $iframe.attr('src','');
        }
      });
    });

    jQuery(document).on('click', '[ms-lightbox]', function(){
      var $clicked = jQuery(this);
      var type = $clicked.data('groups');
      var $content = jQuery('#gallery-modal .content');
      b_slide_slick.destorySlick($content);

      var i = 0;
      var index = 0;
      jQuery('[ms-lightbox]').each(function(){
        var $this = jQuery(this);
        if(type == $this.data('groups')[0]){

          if($clicked[0] === $this[0]){
            index = i;
          }

          var size_class = '';
          if($this.is('.gallery-block-sm')){
            size_class +=  'gallery-block-sm ';
          }

          if($this.is('.gallery-block-lg')){
            size_class += 'gallery-block-lg';
          }

          // var img_src = $this.find('img').attr('src');
          var $picture = $this.find('figure').clone();
          var title = $this.find('figcaption').attr('data-caption') || '';

          var vdo = $this.find('figure').data('vdo') || '';


          if(vdo){
            $content.append('<div><div class="gallery-modal-block"><div class="c-block embed-responsive embed-responsive-16by9"><iframe data-src="https://www.youtube.com/embed/CtjamNMGxRQ?controls=0&showinfo=0&rel=0&autoplay=1&loop=0" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div></div></div>');
          }else{
            // $content.append('<div><div class="gallery-modal-block"><div class="c-block ratio-gallery ' + size_class + '"><figure><img src=' + img_src + '></figure><figcaption class="h4 text-white">' + title + '</figcaption></div></div></div>');
            var $item = jQuery('<div><div class="gallery-modal-block"><div class="c-block ratio-gallery ' + size_class + '"><figcaption class="h5 text-white">' + title + '</figcaption></div></div></div>');
            $item.find('.ratio-gallery').prepend($picture);
            $content.append($item);
          }


          i++;
        }
      });

      b_modal.showModal("#gallery-modal");
      b_slide_slick.initSlick($content);
      var s = $content.slick('getSlick');
      s.slickGoTo(index,true);
    });
  }
}

site.ready.push(ms_gallery.init);
