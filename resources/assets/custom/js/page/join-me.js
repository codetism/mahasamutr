var ms_joinme = {
  expandSelector: '.celeb-item',
  contentSelector: '.celeb-content-container',
  expandClass: 'celeb-is-expand',
  isAnimate: false,
  init: function(){
    ms_joinme.initExpand();
    ms_joinme.initForm();
    ms_joinme.initScollbar();
  },
  initExpand:function(){
    jQuery(ms_joinme.expandSelector).on("click",function(){
      if(ms_joinme.isAnimate) return;
      ms_joinme.isAnimate = true;

      var $current = jQuery(this);
      if($current.hasClass(ms_joinme.expandClass)) return;

      var $last = jQuery(ms_joinme.expandSelector + '.' + ms_joinme.expandClass);
      if($last.length){
        var $lastContent = $last.find(ms_joinme.contentSelector);
        $last.removeClass(ms_joinme.expandClass);
        $last.css('margin-bottom',0);
        $lastContent.slideUp(300, function(){
          ms_joinme.showCeleb($current);
        });

      }else{
        ms_joinme.showCeleb($current);
      }

    });
  },
  showCeleb: function($celeb){
    $celeb.addClass(ms_joinme.expandClass);
    var $content = $celeb.find(ms_joinme.contentSelector);
    $celeb.css('margin-bottom',$content.innerHeight() + 15);

    $content.slideDown(300,function(){
      ms_joinme.isAnimate = false;
      b_scroll_to_tweenmax.to($celeb.offset().top + $celeb.innerHeight());
    });
  },
  initForm: function(){
    jQuery('#join-me-form').on('submit',function(e){
      e.preventDefault();

      // NOTE: show popup
      b_modal.showModal("#thank-modal");
    });
  },
  initScollbar: function(){
    $(".scrollbar-content").mCustomScrollbar({
  		scrollbarPosition: 'outside'
  	});
  }
}

site.ready.push(ms_joinme.init);
