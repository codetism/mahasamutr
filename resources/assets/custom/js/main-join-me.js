jQuery(document).ready(function($){

  $('a[href="#"]').click(function(e) {
    e.preventDefault()
  });


  //
  $('.hamburger').on('click', function(e) {
    e.preventDefault();

    if ($('body').hasClass('menu-is-active')) {
      closeMobileMenu();
    } else {
      showMobileMenu();
    }
  });

		var mainHeader = $('.header'),
		//this applies only if secondary nav is below intro section
		belowNavHeroContent = $('.sub-nav-hero'),
		headerHeight = mainHeader.height();

	//set scrolling variables
	var scrolling = false,
		previousTop = 0,
		currentTop = 0,
		scrollDelta = 10,
		scrollOffset = 157;

	$(window).on('scroll', function(){
		if( !scrolling ) {
			scrolling = true;
			(!window.requestAnimationFrame)
				? setTimeout(autoHideHeader, 250)
				: requestAnimationFrame(autoHideHeader);
		}
	});

	$(window).on('resize', function(){
		headerHeight = mainHeader.height();

    if(jQuery(window).width() > 991){
      jQuery('.main-menu-container').attr('style','');
    }
	});


	function autoHideHeader() {
		var currentTop = $(window).scrollTop();

		( belowNavHeroContent.length > 0 )
			? checkStickyNavigation(currentTop) // secondary navigation below intro
			: checkSimpleNavigation(currentTop);

	   	previousTop = currentTop;
		  scrolling = false;
	}

	function checkSimpleNavigation(currentTop) {
		//there's no secondary nav or secondary nav is below primary nav
    // if (previousTop - currentTop > scrollDelta) {
    // 	//if scrolling up...
    // 	mainHeader.removeClass('is-fixed');
    // } else if( currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
    // 	//if scrolling down...
    // 	mainHeader.addClass('is-fixed');
    // }

      if(currentTop > jQuery('.main section').eq(1).offset().top){
        mainHeader.addClass('is-fixed');
      }else{
        mainHeader.removeClass('is-fixed');
      }


	}

	// NAV

	// var mainContent = $('.main'),
	// 	header = $('.header'),
	// 	sidebar = $('.main-menu-container');
	//click on item and show submenu
	// $('.has-children > a').on('click', function(event){
	// 	var mq = checkMQ(),
	// 		selectedItem = $(this);
	// 	if( mq == 'mobile' || mq == 'tablet' ) {
	// 		event.preventDefault();
	// 		if( selectedItem.parent('li').hasClass('selected')) {
	// 			selectedItem.parent('li').removeClass('selected');
	// 		} else {
	// 			sidebar.find('.has-children.selected').removeClass('selected');
	// 			selectedItem.parent('li').addClass('selected');
	// 		}
	// 	}
	// });

	// function checkMQ() {
	// 	//check if mobile or desktop device
	// 	return window.getComputedStyle(document.querySelector('.main'), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "");
	// }

	// sidebar.children('ul').menuAim({
  //       activate: function(row) {
  //         $('.main-menu').addClass('menu-is-expand');
  //       	$(row).addClass('hover');
  //       },
  //       deactivate: function(row) {
  //         $('.main-menu').removeClass('menu-is-expand');
  //       	$(row).removeClass('hover');
  //       },
  //       exitMenu: function() {
  //       	sidebar.find('.hover').removeClass('hover');
  //       	return true;
  //       },
  //       submenuSelector: ".has-children",
  //   });
  jQuery('.touch .main-menu-container .has-children').on('click',function(e){
    var $this = jQuery(this);
    if($this.is('.hover')){
      return;
    }else{
      e.preventDefault();
      $this.addClass('hover');
    }
  });

  jQuery('.main-menu-container .has-children').hover(function(){
      var $this = jQuery(this);
      $this.children('ul').show(0,function(){
        $this.addClass('hover');
        $('.main-menu').addClass('menu-is-expand');
    });

  },function(){
    var $this = jQuery(this);
    $('.main-menu').removeClass('menu-is-expand');

    $this.children('ul').hide(0,function(){
      $this.removeClass('hover');
    });
  });


	//
    //
    // SCROLL TO TOP
    //
    //
    var topBtn = $('.scroll-top');

	scrollToTop();

    function scrollToTop() {
        if (topBtn.length) {

            topBtn.hide();

            $(window).scroll(function() {
                var foot = $('.footer').offset().top + 67;


                var h = window.innerHeight ? window.innerHeight : $(window).height();
                var st = $(this).scrollTop();
                if (st > 100) {
                    //topBtn.stop(true, true).fadeIn();
                    topBtn.stop(true, true).show(function() {
                        topBtn.addClass('is-visible');
                    });
                } else {
                    //topBtn.stop(true, true).fadeOut();
                    topBtn.stop(true, true);
                    topBtn.removeClass('is-visible');
                }
                if (st - 20 > foot - h) {
                    topBtn.css({
                        bottom: st - (foot - h) + "px"
                    })
                } else {
                    topBtn.css("bottom", "");
                }
            });

        }
    }

});

function onResizeNav(){
  if(jQuery(window).width() > 991){
    showMobileMenu();
    $('body').removeClass('overflow-hidden');
  }else{
    $('.main-menu-container').hide(0);
    $('body').removeClass('menu-is-active');
    $('body').removeClass('overflow-hidden');
    $('.hamburger').removeClass('is-active');
  }
}

function closeMobileMenu(){

  $('.main-menu-container').fadeOut(400,function(){
    $('body').removeClass('menu-is-active');
    $('body').removeClass('overflow-hidden');
    $('.hamburger').removeClass('is-active');
  });
}

function showMobileMenu(){

  $('.main-menu-container').show(0, function() {
    $('.hamburger').addClass('is-active');
    $('body').addClass('menu-is-active');
    $('body').addClass('overflow-hidden');
  });
}

site.resize.push(onResizeNav);
