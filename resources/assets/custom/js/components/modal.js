var b_modal = {
  activeClass: 'is-visible',
  modalSelector: '[b-modal]',
  closeSelector: '[b-modal-close]',
  triggerData: 'modal-trigger',
  overlaySelector: '.modal-overlay',
  init: function() {
    // modal
    $(b_modal.modalSelector).each(function() {
      var modal = $(this);
      $(this).hide();
      $(this).on('click', function(e) {
        // normal
        if (e.target == $(this)[0]) {
          b_modal.closeModal('#' + $(this).attr('id'));
        }
      });
    });

    // trigger
    $('[data-' + b_modal.triggerData + ']').each(function() {
      $(this).on('click', function(e) {
        e.preventDefault();
        var id = $(this).data(b_modal.triggerData);
        b_modal.showModal(id);
      });
    });

    // close
    $(b_modal.closeSelector).each(function() {
      $(this).on('click', function(e) {
        e.preventDefault();
        var modalId = $(this).parents(b_modal.modalSelector).attr('id');
        b_modal.closeModal('#' + modalId);
      });
    });

    $(b_modal.overlaySelector).each(function() {
      $(this).on('click', function(e) {
        e.preventDefault();
        var modalId = $(this).parents(b_modal.modalSelector).attr('id');
        b_modal.closeModal('#' + modalId);
      });
    });
  },
  showModal: function(id) {
    if ($(id).length) {
      // clear animation
      $(id).removeClass(b_modal.activeClass);

      if(id != '#youtube-modal') {
        $(id).show(0, function() {
          $(this).addClass(b_modal.activeClass).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
            $('body').addClass('overflow-hidden');
          });

          // slide
          $(this).find('[b-slider]').each(function(){
            var slick = $(this).slick('getSlick');
            slick.refresh();
          });
        });
      } else {
        $(id).show(0, function() {
          $(this).addClass(b_modal.activeClass);
          $('body').addClass('overflow-hidden');
          var video = '<iframe src="https://www.youtube.com/embed/-xdVVEOBedE?controls=0&showinfo=0&rel=0&autoplay=1&loop=0" frameborder="0" allowfullscreen></iframe>';

          $(this).find('.video-embed').each(function() {
            $(this).html(video);
          });
          TweenMax.to($(id), .5, {
            autoAlpha: 1,
            ease: Power2.easeInOut,
            delay: .75
          })
        });
      }


    }
  },
  closeModal: function(id) {
    if ($(id).length) {

      if(id != '#youtube-modal') {
        // $(id).removeClass(b_modal.activeClass).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
        //   $(this).hide();
        //   $('body').removeClass('overflow-hidden');
        // });
        $(id).removeClass(b_modal.activeClass);
        $(id).hide(0);
        $('body').removeClass('overflow-hidden');
      } else {
        TweenMax.to($(id), .5, {
          autoAlpha: 0,
          ease: Power2.easeInOut,
          onComplete: b_modal.setCloseModal(id)
        })
      }
      $(id).find('iframe').each(function(){
        jQuery(this).attr('src','');
      });
    }
  },
  closeAllModal: function() {
    $('[data-' + b_modal.triggerData + ']').each(function() {
      b_modal.closeModal($(this).data(b_modal.triggerData));
    });
  },
  setCloseModal: function(id) {
    $(id).hide().removeClass(b_modal.activeClass);
    $('body').removeClass('overflow-hidden');
    $(id).find('.video-embed').each(function() {
      $(this).html("");
    });
  }
}

site.ready.push(b_modal.init);
