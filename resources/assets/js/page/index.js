var ms_index = {
  vdoSelector: '#index-vdo',
  vdoDOM: '',
  captionSelector: '#index-caption',
  hideClass: 'is-hide',
  init: function(){
    if(!jQuery(ms_index.vdoSelector).length) return;

    ms_index.vdoDOM = jQuery(ms_index.vdoSelector)[0];

    ms_index.setVDO();
    ms_index.loop();
  },
  loop: function(){
    requestAnimFrame(ms_index.loop);


    var currentTime = ms_index.vdoDOM.currentTime;
    // console.log(currentTime);

    if((currentTime > 7 && currentTime < 8) || (currentTime > 19 && currentTime < 20) ){
      // show caption
      if(!jQuery(ms_index.captionSelector).parent().hasClass('ms-animate')){
        jQuery(ms_index.captionSelector).parent().addClass('ms-animate');
      }
    }

    if((currentTime > 12 &&  currentTime < 13) || (currentTime > 24 && currentTime < 25)){
      // hide caption
      if(!jQuery(ms_index.captionSelector).hasClass(ms_index.hideClass)){
        jQuery(ms_index.captionSelector).addClass(ms_index.hideClass);
      }
    }

    if((currentTime > 17 && currentTime < 18) || (currentTime > 0 && currentTime < 1) ){
      // remove ms-animate
      if(jQuery(ms_index.captionSelector).hasClass(ms_index.hideClass)){
        jQuery(ms_index.captionSelector).parent().removeClass('ms-animate');
        jQuery(ms_index.captionSelector).removeClass(ms_index.hideClass);
      }
    }

    // old
    // if((currentTime > 1 && currentTime < 2) || (currentTime > 15 && currentTime < 16)){
    //   // show caption
    //   if(!jQuery(ms_index.captionSelector).parent().hasClass('ms-animate')){
    //     jQuery(ms_index.captionSelector).parent().addClass('ms-animate');
    //   }
    // }
    //
    // if(currentTime > 8 &&  currentTime < 9 || (currentTime > 23 && currentTime < 24)){
    //   // hide caption
    //   if(!jQuery(ms_index.captionSelector).hasClass(ms_index.hideClass)){
    //     jQuery(ms_index.captionSelector).addClass(ms_index.hideClass);
    //   }
    // }
    //
    // if(currentTime > 12 && currentTime < 13 || (currentTime > 27 && currentTime < 28)){
    //   // remove ms-animate
    //   if(jQuery(ms_index.captionSelector).hasClass(ms_index.hideClass)){
    //     jQuery(ms_index.captionSelector).parent().removeClass('ms-animate');
    //     jQuery(ms_index.captionSelector).removeClass(ms_index.hideClass);
    //   }
    // }
  },
  onLoad: function(){
    if(!jQuery(ms_index.vdoSelector).length) return;

    b_modal.showModal("#promotion-modal");

    setTimeout(function(){
      // clear
      jQuery(ms_index.captionSelector).parent().removeClass('ms-animate');
      jQuery(ms_index.captionSelector).removeClass(ms_index.hideClass);
    },500);


  },
  setVDO:function(){
    var $area = jQuery('.hero-video');
    var $vdo = jQuery('.hero-video .video');
    var showRatio = $area.width() / $area.height();
    var videoRatio = 2520 / 1080;
    if(videoRatio < showRatio){
      $vdo.width($area.width());
      $vdo.height($vdo.width() / videoRatio);
      jQuery('.hero-video .video').addClass('video-wide');
    }else{
      $vdo.height($area.height());
      $vdo.width( $vdo.height() * videoRatio);
      // jQuery('.hero-video .video').css('top','50%');
      jQuery('.hero-video .video').removeClass('video-wide');
    }
  }
};

site.ready.push(ms_index.init);
site.load.push(ms_index.onLoad);
site.resize.push(ms_index.setVDO);
