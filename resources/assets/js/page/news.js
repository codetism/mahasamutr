var ms_news = {
  newsTriggerSelector: '#events-and-activities [data-modal-trigger]',
  nextModalSelector: '.news-modal .modal-next',
  prevModalSelector: '.news-modal .modal-prev',
  currentIndex: 0,
  init: function(){
    ms_news.initNewsGallery();
    ms_news.initScrollBar();
  },
  initNewsGallery: function(){
    jQuery(document).on('click', ms_news.newsTriggerSelector, function(e){
      var index = jQuery(this).index();
      ms_news.currentIndex = index;
      ms_news.updateModalNav();
    });

    jQuery('.news-modal .modal-next').on('click',function(){
      ms_news.currentIndex = ms_news.currentIndex + 1;

      ms_news.showCurrentModal();
    });
    jQuery('.news-modal .modal-prev').on('click',function(){
      ms_news.currentIndex = ms_news.currentIndex - 1;

      ms_news.showCurrentModal();
    });
  },
  initScrollBar: function(){
    $(".scrollbar-content").mCustomScrollbar({
  		scrollbarPosition: 'outside'
  	});
  },
  destroyScrollBar: function(){
    $(".scrollbar-content").mCustomScrollbar("destroy");
  },
  showCurrentModal: function(){
    var modal = jQuery(ms_news.newsTriggerSelector).eq(ms_news.currentIndex).data('modal-trigger');
    $(modal).addClass('skip-close');
    b_modal.closeAllModal();
    b_modal.showModal(modal);
    ms_news.updateModalNav();
  },
  updateModalNav: function(){
    var totalNews = jQuery(ms_news.newsTriggerSelector).length - 1;

    jQuery(ms_news.prevModalSelector).show(0);
    jQuery(ms_news.nextModalSelector).show(0);

    switch(ms_news.currentIndex){
      case 0:
        jQuery(ms_news.prevModalSelector).hide(0);
        break;
      case totalNews:
        jQuery(ms_news.nextModalSelector).hide(0);
        break;
    }
  },
  onResize: function(){
    if(jQuery(window).width < 992){
      ms_news.destroyScrollBar();
    }else{
      ms_news.initScrollBar();
    }
  }
}

site.ready.push(ms_news.init);
