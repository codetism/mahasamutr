var ms_luxury = {
  init: function(){
    ms_luxury.initScrollBar();
  },
  initScrollBar: function(){
    $(".faq-container").mCustomScrollbar({
  		scrollbarPosition: 'inside'
  	});
  }
}

site.ready.push(ms_luxury.init);
