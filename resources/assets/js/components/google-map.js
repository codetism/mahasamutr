var ms_google_map = {
    selector: '[ms-google-map]',
    defaultData: {
        lat: 12.531466,
        lng:  99.945809,
        top: 50,
        left: 50,
        zoom: 15,
        minZoom: 2,
        marker: '',
        title: '',
        removeControl: false,
        scrollwheel: false
    },
    mapData: 'map',
    key: 'AIzaSyDYRrwQ7xuWU-HnGmQEWE6FiXNQG741qKI',
    init: function() {
        if (!$(ms_google_map.selector).length) return;

        var s = document.createElement('script');
        s.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?callback=ms_google_map.createMap&key=' + ms_google_map.key);
        s.setAttribute('id', 'map-script');
        document.body.appendChild(s);
    },
    createMap: function() {
        if (!$(ms_google_map.selector).length) return;

        var customMapTypeId = 'custom_style';
        var customMapType = new google.maps.StyledMapType([{
            featureType: "all",
            elementType: "labels",
            stylers: [{
                visibility: "on"
            }]
        }, {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#f5f5f5"
            }
          ]
        },
        {
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#f5f5f5"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#bdbdbd"
            }
          ]
        },
        {
          "featureType": "landscape",
          "stylers": [
            {
              "color": "#b2c8cc"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#eeeeee"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#e5e5e5"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#bfd6d0"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#bed4d9"
            },
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#c5d7db"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#6a7679"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#c3dadf"
            }
          ]
        },
        {
          "featureType": "transit.line",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#e5e5e5"
            }
          ]
        },
        {
          "featureType": "transit.station",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#eeeeee"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#6daeb7"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        }], {
            name: 'Custom Style'
        });

        $(ms_google_map.selector).each(function() {
            var data = $(this).data(ms_google_map.mapData) || {};
            var d = $.extend({}, ms_google_map.defaultData, data);
            $(this).data(ms_google_map.mapData,d);

            var latLng = new google.maps.LatLng(d.lat,d.lng);

            var map = new google.maps.Map(this, {
                zoom: d.zoom,
                minZoom: d.minZoom,
                center: latLng,
                mapTypeControl: false,
                scrollwheel: d.scrollwheel,
                disableDefaultUI: d.removeControl
            });

            map.mapTypes.set(customMapTypeId, customMapType);
            map.setMapTypeId(customMapTypeId);

            $(this).data('gg-map',map);
            ms_google_map.adjustPos(map,d);

            // add event
            google.maps.event.addDomListener(window, 'resize', function() {
              ms_google_map.adjustPos(map,d);
            });

            // marker
            if(d.marker){
              var marker = new google.maps.Marker({
                  position: latLng,
                  map: map,
                  icon: d.marker,
                  title: d.title,
                  optimized: false
              });

              marker.addListener('click', function() {
                console.log('click marker');
                var link = "https://www.google.com/maps/preview/@" + d.lat + "," + d.lng + "," + d.zoom + "z";
                window.open(link, "_blank");
              });
            }else{
              var defaultMarker = new google.maps.OverlayView();
              defaultMarker.draw = function () {
                var self = this;
                var div = this.div;

                if (!div) {

                  div = this.div = document.createElement('div');

                  div.className = 'marker';
                  div.title = d.title;

                  google.maps.event.addDomListener(div, "click", function(event) {
                    google.maps.event.trigger(self, "click");
                  });

                  var panes = this.getPanes();
                  panes.overlayImage.appendChild(div);
                }

                var point = this.getProjection().fromLatLngToDivPixel(latLng);

                if (point) {
                  div.style.left = point.x + 'px';
                  div.style.top = point.y + 'px';
                }
              };
              defaultMarker.setMap(map);
            }
        });
    },
    adjustPos: function(map,data){
      google.maps.event.trigger(map, 'resize');
      var center = new google.maps.LatLng(data.lat,data.lng);
      map.setCenter(center);
      var container = map.getDiv();
      var dx = $(container).innerWidth() * (50 - data.left) * 0.01;
      var dy = $(container).innerHeight() * (50 - data.top) * 0.01;
      map.panBy(dx,dy);
    },
    redraw:function(){
      $(ms_google_map.selector).each(function(){
        var map  = $(this).data('gg-map');
        var data = $(this).data(ms_google_map.mapData);
        ms_google_map.adjustPos(map,data);
      });
    }
}

site.ready.push(ms_google_map.init);
