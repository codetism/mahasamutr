var ms_parallax_section = {
  controller: '',
  selector: '[ms-parallax-section]',
  init: function(){
    if($(ms_parallax_section.selector).length){
      ms_parallax_section.controller = new ScrollMagic.Controller();

      jQuery(ms_parallax_section.selector).each(function(){
        var $section = jQuery(this);
        var $img = $section.find('[ms-parallax-item]');
        var scene = new ScrollMagic.Scene({
                  triggerElement: $section[0],
                  triggerHook: 0,
                  duration: '100%'
                })
                // .addIndicators({name: "1 (duration: 0)"})
                .addTo(ms_parallax_section.controller);

        var yData = jQuery(window).height() * 0.95;
        // scene.setTween(jQuery(this)[0], {y: yData, ease: Linear.easeNone});
        scene.setTween($img, {y: yData, ease: Linear.easeNone});

      });
    }
  }
}

site.ready.push(ms_parallax_section.init);
