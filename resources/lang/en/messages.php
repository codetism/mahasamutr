<?php

return [
//  GENERAL
    'current' => 'EN',

//  MENU
    'menu_about' => 'About',
    'menu_location' => 'Location',
    'menu_pcc' => 'Private Country Club',
    'menu_lv' => 'Luxury Villa',
    'menu_gallery' => 'Gallery',
    'menu_news' => 'News',
    'menu_development' => 'Development',
    'menu_contact_us' => 'Contact Us',
    'menu_concept' => 'Concept',
    'menu_mh_lagoon' => 'Mahasamutr Lagoon',
    'menu_sports_amenities' => 'Sports & Amenities',
    'menu_membership_benefits' => 'Membership Benefits',
    'menu_faq' => 'FAQ<span style="font-size: 8px;">s</span>',
    'menu_plots' => 'Plots & Layouts',
    'menu_vdo' => 'Video Walk Through',
    'menu_owner_benefits' => 'Owner\'s Benefit',
    'menu_cc' => 'Country Club',
    'menu_event' => 'Events & Activities',
    'menu_construction' => 'Construction Updates',
    'menu_team' => 'Team',
    'menu_fs' => 'Fact Sheet',

//  FOOTER
    'footer_address' => 'MahaSamutr Soi Hua Hin 112, Hua Hin, Prachuap Khiri Khan 77110 Thailand',
    'footer_subscription' => 'Newsletter Subscription',
    'footer_email_field' => 'Your Email Address',

//  WEB ELEMENTS
    'button_read_more' => 'Read More',
    'button_gallery' => 'Gallery',

//  HOMEPAGE
//  - BANNER
    'home_banner_l1' => 'Close your eyes',
    'home_banner_l2' => '... listen to the sound',
    'home_banner_l3' => 'of mahasamutr',
//  - ABOUT
    'home_about_title'      => 'ABOUT MAHASAMUTR',
    'home_about_desc_p1_l1' => 'Join Hua Hin\'s most exclusive waterside community, with the award-winning MahaSamutr Villas,',
    'home_about_desc_p1_l2' => 'featuring MahaSamutr Country Club, Hua Hin\'s first private country club',
    'home_about_desc_p1_l3' => 'set alongside Asia\'s largest Crystal Lagoon.',
    'home_about_desc_p2_l1' => 'With services and property management provided by PACE, MahaSamutr is more than',
    'home_about_desc_p2_l2' => 'just a home, more than investment in the township, but is becoming a part of your family\'s legacy',
    'home_about_desc_p2_l3' => 'for generations to come - a tranquil environment to relax and unwind.',
//  - LOCATION
    'home_location_title_l1' => 'LOCATION',
    'home_location_title_l2' => 'HUA HIN',
    'home_location_desc_l1'  => 'MahaSamutr is located approximately 200 kilometers south of Bangkok',
    'home_location_desc_l2'  => 'and 4 kilometers from Hua Hin beach, easy to reach by car',
    'home_location_desc_l3'  => 'in around 2.5 hours from Bangkok.',
//  - PRIVATE COUNTRY CLUB
    'home_pcc_title'   => 'PRIVATE COUNTRY CLUB',
    'home_pcc_desc_l1' => 'MahaSamutr Country Club is Hua Hin’s first true private country club, adding to',
    'home_pcc_desc_l2' => 'the graceful township\'s rich history. Bringing a sense of privacy and exclusivity, members enjoy',
    'home_pcc_desc_l3' => 'a wide range of facilities created as a home away from home.',
//  - GALLERY
    'home_gallery_l1' => 'DISCOVER THE PURE JOY',
    'home_gallery_l2' => '...OF RESORT LIVING',
//  - LUXURY VILLAS
    'home_lv_title'   => 'LUXURY VILLAS',
    'home_lv_desc_l1' => '80 graceful, modern homes featuring bespoke design',
    'home_lv_desc_l2' => 'and landscaped elements, providing the best in',
    'home_lv_desc_l3' => 'waterside living at MahaSamutr Villas, Hua Hin.',
//  - MAHASAMUTR NEWS
    'home_news_title' => 'MAHASAMUTR NEWS',
    'home_construction_l1' => 'CONSTRUCTION',
    'home_construction_l2' => 'UPDATES',
    'home_events_l1' => 'EVENTS &',
    'home_events_l2' => 'ACTIVITIES',

//  ABOUT
//  - ABOUT
    'about_main_title_l1'   => 'About MahaSamutr',
    'about_main_title_l2'   => 'embrace the natural beauty',
    'about_main_desc_p1_l1' => 'MahaSamutr brings together the timeless resort life for which Hua Hin is renowned and the innovation of PACE,',
    'about_main_desc_p1_l2' => 'Thailand\'s leading luxury property developer.',
    'about_main_desc_p2_l1' => 'MahaSamutr, meaning “the Ocean”, provides unique resort living of 80 award-winning luxury villas (MahaSamutr Villas),',
    'about_main_desc_p2_l2' => 'featuring Hua Hin’s first private country club (MahaSamutr Country Club), set around Asia\'s largest man-made Crystal Lagoon.',
//  - Own a luxury property in HUA HIN
    'about_own_title_l1' => 'own a luxury',
    'about_own_title_l2' => 'property in HUA&nbsp;HIN',
    'about_own_desc_p1'  => 'Overlooking the MahaSamutr Lagoon are 80 highly private, gracefully modern and bespoke luxury villas, providing villa owners best waterside living in a natural setting. Villa owners and their families also enjoy a lifetime membership at MahaSamutr Country Club.',
    'about_own_desc_p2'  => 'MahaSamutr brings country club members and villa owners the sea and beach ambience in a safe controlled luxury environment for year round family leisure and recreational activities.',
    'about_own_desc_p3'  => 'As the first of its kind, your entire family can play and experience the ultimate waterfront living. Next to the man-made beach, discover dining, sports, facilities and services to embrace the natural beauty of MahaSamutr.',
    'about_own_desc_p4'  => 'Set to become a new landmark in the popular resort town of Hua Hin, MahaSamutr is uplifting the destination’s high-end property and service industries to a new level of luxury.',
//      - NEW
    'about_new_own_desc_p1'  => 'MahaSamutr brings country club members and villa owners the sea and beach ambience in a safe controlled luxury environment for year round family leisure and recreational activities.',
    'about_new_own_desc_p2'  => 'MahaSamutr is set to become a new landmark in the popular resort town of Hua Hin, uplifting the destination’s high-end property and service industries to a new level of luxury.',


//  LOCATION
//  - LOCATION
    'location_banner_title_l1' => 'MAHASAMUTR',
    'location_banner_title_l2' => 'LOCATION',
    'location_banner_desc_l1'  => 'Located approximately 200 kilometers south of Bangkok and',
    'location_banner_desc_l2'  => '4 kilometers from Hua Hin beach, MahaSamutr is easy to reach',
    'location_banner_desc_l3'  => 'by car in around 2.5 hours from Bangkok.',
//  - HUA HIN
    'location_hua_hin_title'      => 'HUA HIN',
    'location_hua_hin_desc_p1_l1' => 'The natural beauty and serene atmosphere of Hua Hin inspired members of the Royal Family,',
    'location_hua_hin_desc_p1_l2' => 'aristocrats and Bangkok\'s elite to build bungalows here, beginning in the early 20<sup>th</sup> century.',
    'location_hua_hin_desc_p2_l1' => 'As the popularity of the township has grown, so has the supporting infrastructure, with world-class',
    'location_hua_hin_desc_p2_l2' => 'shopping, hospitality and dining added to a wide range of sports and natural attractions.',
//  - TOWNSHIP
    'location_township_desc_p1_l1' => 'A township with historical charm,',
    'location_township_desc_p1_l2' => 'Hua Hin is famed for its beaches, seaside and',
    'location_township_desc_p1_l3' => 'numerous sports.',
    'location_township_desc_p2_l1' => 'Golf enthusiasts also will welcome access to some',
    'location_township_desc_p2_l2' => 'of the finest golf courses in Thailand in the area.',
//  - HUA HIN 112
    'location_112_title'   => 'SOI HUA HIN 112',
    'location_112_desc_l1' => 'Access MahaSamutr easily with a choice of 2 main roads; Phetkasem or Bypass Road. Located in a quiet and peaceful area,',
    'location_112_desc_l2' => 'MahaSamutr is mere minutes away from the local city, Hua Hin beach, shopping and much more.',

//  PRIVATE COUNTRY CLUB
//  - BANNER
    'pcc_banner_l1' => 'PRIVATE',
    'pcc_banner_l2' => 'COUNTRY CLUB',
//  - CONCEPT
    'pcc_concept_title'      => 'Concept',
    'pcc_concept_desc_p1_l1' => 'MahaSamutr Country Club is Hua Hin\'s first private country club, adding to the graceful township\'s',
    'pcc_concept_desc_p1_l2' => 'rich history. Bringing a sense of privacy and exclusivity, members enjoy a wide range of facilities',
    'pcc_concept_desc_p1_l3' => 'created as a home away from home with grounds spread of 75 Rai (120,000 sq.m.).',
    'pcc_concept_desc_p2_l1' => 'Whether sharing time with friends and family, or simply to find time for yourself, MahaSamutr provides',
    'pcc_concept_desc_p2_l2' => 'a true sense of community for business and for pleasure to enjoy year round.',
    'pcc_concept_brochure' => '/downloads/Mini Brochure MHSM A5 Eng VER Final 2017-03-02 Creat.pdf',
    'pcc_concept_download' => 'Download Brochure',
//  - LAGOON
    'pcc_lagoon_title_l1'   => 'Mahasamutr',
    'pcc_lagoon_title_l2'   => 'Lagoon',
    'pcc_lagoon_desc_p1_l1' => 'The centerpiece of MahaSamutr is the stunning man-made 45 Rai (72,000 sq.m.)',
    'pcc_lagoon_desc_p1_l2' => 'MahaSamutr Lagoon, owned by MahaSamutr Country Club.',
    'pcc_lagoon_desc_p2_l1' => 'As the largest in Asia, the Lagoon is more than 800 meters long, filled with crystal',
    'pcc_lagoon_desc_p2_l2' => 'clear sparkling waters. The quality is carefully maintained, providing a haven for',
    'pcc_lagoon_desc_p2_l3' => 'water sports year round, with depths ranging from the shallows of an artificial',
    'pcc_lagoon_desc_p2_l4' => 'sand beach, up to 2.5 meters.',
//  - LAGOON DESCRIPTION
//      - CRYSTAL LAGOON
        'pcc_crystal_lagoon_title_l1'   => 'THAILAND’S FIRST',
        'pcc_crystal_lagoon_title_l2'   => 'CRYSTAL LAGOON',
        'pcc_crystal_lagoon_desc_p1'    => 'The MahaSamutr Lagoon showcases the experience and innovation of Crystal Lagoons®, a patented technology providing clean and pristine waters 365 days per year.',
        'pcc_crystal_lagoon_desc_p2'    => 'Crystal Lagoons® is an award-winning leading company specializing in creating extraordinary water features, with patented technology used in more than 600 projects in more than 60 countries worldwide.',
        'pcc_crystal_lagoon_desc_p3'    => 'Crystal Lagoons® uses a sophisticated monitoring system to minimize chemicals while consistently maintaining high water standards, enabling water features of previously unimagined size to be operated and managed effectively.',
//      - WATER FEATURES
        'pcc_water_features_title_l1'   => 'Extraordinary',
        'pcc_water_features_title_l2'   => 'Water Features',
        'pcc_water_features_desc_p1'    => 'Using eco-friendly and sustainable technology is a driving force of Crystal Lagoons®. Using up to 100 times less chemical additives than conventional swimming pool systems, most lagoons can also be highly energy-efficient, using as little as 2% of the energy required to operate traditional filtration systems.',
        'pcc_water_features_desc_p2'    => 'Since the lagoon operates on a closed circuit, it has very low level of water consumption, consuming 50% less water than a waterpark of the same size.',
        'pcc_water_features_desc_p3'    => 'The technology provides crystal clear swimming waters, perfect for families and water sports enthusiasts to enjoy.',
//      - WATERSIDE LIVING
        'pcc_waterside_title_l1' => 'A New Standard',
        'pcc_waterside_title_l2' => 'of Waterside Living',
        'pcc_waterside_desc_p1'  => 'The Lagoon provides a focal point for the entire MahaSamutr development, offering a new standard of waterside living to be enjoyed all year round.',
        'pcc_waterside_desc_p2'  => 'You can enjoy a full spectrum of recreational water activities, perfect for children and adults alike, such as swimming, kayaking, paddle-boarding and sailing.',
        'pcc_waterside_desc_p3'  => 'As the first of its kind, your entire family can play or relax in a safe and controlled environment.',
        'pcc_waterside_desc_p4'  => 'Savor the luxury of living by Asia’s largest Crystal Lagoon, in the heart of Hua Hin.',
//  - SPORT & AMENITIES
    'pcc_sa_title_s1' => 'sports ',
    'pcc_sa_title_s2' => '& amenities',
//  - WATER SPORTS
    'pcc_water_sports_title'   => 'Water Sports',
    'pcc_water_sports_desc_p1' => 'MahaSamutr Country Club provides members with access to the Lagoon as well as an indoor competition swimming pool and children’s swimming&nbsp;pool.',
    'pcc_water_sports_desc_p2' => 'Each swimming area is monitored by experienced lifeguards. Together, these zones provide a large range of varied sports with dedicated trainers at each available upon request, enabling each member to reach their goals, whether it is learning a new sport, or mastering one for competition.',
    'pcc_water_sports_paddle_boarding' => 'Paddle-Boarding',
    'pcc_water_sports_windsurfing' => 'Windsurfing',
    'pcc_water_sports_kayaking' => 'Kayaking',
    'pcc_water_sports_swimming' => 'Swimming',
    'pcc_water_sports_sailing' => 'Sailing',
//  - OTHER SPORTS
    'pcc_other_sports_title_l1'                                  => 'Other sports',
    'pcc_other_sports_title_l2'                                  => '& FITNESS',
    'pcc_other_sports_desc_l1'                                   => 'Enjoy a wide range of sports with dedicated facilities with a well-trained team',
    'pcc_other_sports_desc_l2'                                   => 'of fitness professionals, for each option both indoor and outdoor.',
    'pcc_other_sports_desc_indoor_subsection'                    => 'Indoor',
    'pcc_other_sports_desc_indoor_list_aerobics'                 => 'Aerobics',
    'pcc_other_sports_desc_indoor_list_badminton'                => 'Badminton',
    'pcc_other_sports_desc_indoor_list_basketball'               => 'Basketball',
    'pcc_other_sports_desc_indoor_list_cardio_vascular_training' => 'Cardio Vascular Training',
    'pcc_other_sports_desc_indoor_list_fitness_center'           => 'Fitness Center',
    'pcc_other_sports_desc_indoor_list_group_training'           => 'Group Training',
    'pcc_other_sports_desc_indoor_list_pilates_yoga'             => 'Pilates / Yoga',
    'pcc_other_sports_desc_indoor_list_squash'                   => 'squash',
    'pcc_other_sports_desc_indoor_list_volleyball'               => 'volleyball',
    'pcc_other_sports_desc_outdoor_subsection'                   => 'Outdoor',
    'pcc_other_sports_desc_outdoor_list_jogging_path'            => 'Jogging Path',
    'pcc_other_sports_desc_outdoor_list_rugby'                   => 'Rugby',
    'pcc_other_sports_desc_outdoor_list_football'                => 'Football',
    'pcc_other_sports_desc_outdoor_list_tennis'                  => 'Tennis',
//  - CLUB AMENITIES & SERVICES
    'pcc_club_amenities_services_title_l1'  => 'CLUB AMENITIES',
    'pcc_club_amenities_services_title_l2'  => '& SERVICES',
    'pcc_club_amenities_services_desc_p1'   => 'MahaSamutr Country Club has designed specific programs and events to meet the needs of everyone in the family.',
    'pcc_club_amenities_services_desc_p2'   => 'Featuring some of the town’s best restaurants, MahaSamutr Country Club features dining options to match every occasion, along with on-property accommodation exclusively for members.',
    'pcc_club_amenities_services_desc_p3'   => 'MahaSamutr Country Club also provides the perfect destination for corporate hospitality, including press events, meetings, retreats or team building.',
//  - MEMBERSHIP BENEFITS
    'pcc_membership_benefits_title_s1'  => 'MEMBERSHIP',
    'pcc_membership_benefits_title_s2'  => ' BENEFITS',
    'pcc_membership_benefits_desc_p1'   => 'All categories of Membership receive immediate benefits while the Country Club is under development. For the complete list of privileges, please see <a href="/downloads/MHSM Benefits_07092017.pdf">"Immediate Benefits"</a>.',
    'pcc_membership_benefits_desc_p2'   => 'The Country Club is not only for leisure and family moment but is also ideal for members to combine business with pleasure whilst taking advantage of our many benefits. Members will enjoy the use of endless variety of club facilities and tailor made activities. The Country Club staff provide discreet and professional services at a superb and consistent level, plus an unwavering commitment to make the club the members\' home away from home.',
//      - INDIVIDUAL
        'pcc_individual_title'   => 'INDIVIDUAL',
        'pcc_individual_desc_p1' => 'An Individual Member who is 21 years of age or older',
        'pcc_individual_desc_p2' => 'All members are able to benefit from the clubs’ network of personal and professional affiliations. In addition, the social opportunities for an Individual member includes welcoming their friends to the club as their guests and attending a full activity calendar of tasting, workshops, seminars and sporting events.',
//      - FAMILY
        'pcc_family_title'   => 'FAMILY',
        'pcc_family_desc_p1' => 'An Individual with Spouse and all children under 21 years of age',
        'pcc_family_desc_p2' => 'All family members enjoy a lot of activities that the Club provides and receive under one roof. The Club programming will be designed to serve the entire family.',
//      - CORPORATE
        'pcc_corporate_title'   => 'CORPORATE',
        'pcc_corporate_desc_p1' => 'A membership for two nominees from the same company',
        'pcc_corporate_desc_p2' => 'Members appreciate the ability to network with fellow members and their guests, enhancing their social and professional circles of affiliation. The Country Club provides luxurious and well-orchestrated meeting and convention services in different venues designed to accommodate meetings and group activities in a range of sizes.',
//  - FAQs
    'pcc_faqs_title_s1' => 'FAQ',
    'pcc_faqs_title_s2' => 's',
    // CONTENT HERE

//  LUXURY VILLAS
//  - BANNER
    'lv_banner_l1' => 'MAHASAMUTR',
    'lv_banner_l2' => 'LUXURY VILLAS',
//  - MAIN
    'lv_villa_concept_title'   => 'VILLA concept',
    'lv_villa_concept_desc_p1' => '80 graceful, modern homes featuring bespoke design and landscaped elements, providing the best in waterside living at MahaSamutr.',
    'lv_villa_concept_desc_p2' => 'Reflecting the natural greenery and rolling hills for which the city is famous, a heavy emphasis on natural materials sees each Villa blending into the environment, with customizing roof and sloping rooflines melting into the waters.',
//  - AWARD-WINNING LUXURY VILLA
    'lv_award_title_l1'            => 'AWARD-WINNING',
    'lv_award_title_l2'            => 'LUXURY VILLAS',
    'lv_award_desc_p1'             => 'MahaSamutr Villas has been recognized both in Thailand and across South East Asia with awards for its property :',
    'lv_award_desc_th_award_title' => 'Thailand Property Awards 2016',
    'lv_award_desc_th_award_1'     => 'Best Villa Development (Thailand)',
    'lv_award_desc_th_award_2'     => 'Best Luxury Villa Development (Hua Hin)',
    'lv_award_desc_th_award_3'     => 'Best Residential Architectural Design',
    'lv_award_desc_se_award_title' => 'South East Asia Property Awards 2016',
    'lv_award_desc_se_award_1'     => 'Best Villa/Housing Development',
    'lv_award_desc_se_award_2'     => 'Best Residential Architectural Design',
//  - DESIGN CONCEPT
    'lv_design_concept_title'   => 'DESIGN CONCEPT',
    'lv_design_concept_desc_p1' => 'MahaSamutr Villas range in size from 447-587 sq.m. providing 4 bedrooms and 5 bathrooms, with a large living/dining area for entertaining, overlooking the Lagoon.',
    'lv_design_concept_desc_p2' => 'The two different roofline styles gently shelter each residence while creating a harmonized roofline across the property.',
    'lv_design_concept_desc_p3' => 'Each Villa is carefully placed to optimize natural light, views and privacy, including parking for 3 vehicles and maid’s quarters supported by a full range of property management services.',
    'lv_design_concept_desc_p4' => 'Interior design reflects a heavy emphasis on natural materials, creating a sense of peace and tranquility.',
//  - BENEFITS
    'lv_benefits_title'   => 'BENEFITS',
    'lv_benefits_desc_p1' => 'You and your family can enjoy MahaSamutr Country Club and are enabled to all the facilities throughout the duration of ownership of the Villa property.',
    'lv_benefits_desc_p2' => 'The facilities and activities are designed for sharing with family, or to achieve your own personal goals. A wide range of sports, dining, recreation and business facilities deliver a unique and long lasting value to be enjoyed every day.',
//  PLOTS & LAYOUTS
    'lv_plots_layouts_title_s1'     => 'PLOTS & ',
    'lv_plots_layouts_title_s2'     => 'LAYOUTS',
    'lv_plots_layouts_bedrooms'     => 'Bedrooms',
    'lv_plots_layouts_bathrooms'    => 'Bathrooms',
    'lv_plots_layouts_parking'      => '',
    'lv_plots_layouts_parking_unit' => 'Parking Spaces',
    'lv_plots_layouts_maid_rooms'   => 'Maid Rooms',
    'lv_plots_layouts_usable_area'  => 'Usable Area',
    'lv_plots_layouts_sqm'          => 'sq.m',
    'lv_plots_layouts_land_area'    => 'Land Area',
    'lv_plots_layouts_sqw'          => 'sq.wah',
//  WALK THROUGH VIDEO
    'lv_vdo_title_s1' => 'WALK THROUGH',
    'lv_vdo_title_s2' => ' VIDEO',

//  GALLERY
//  - PRIVATE COUNTRY CLUB & LUXURY VILLAS
    'gallery_main_title_l1'            => 'PRIVATE COUNTRY CLUB',
    'gallery_main_title_l2'            => '& LUXURY VILLAS',
    'gallery_tab_luxury_villas'        => 'PRIVATE COUNTRY CLUB',
    'gallery_tab_private_country_club' => 'LUXURY VILLAS',

//  NEWS
//  - CONSTRUCTION UPDATES
    'news_construction_title_s1'          => 'CONSTRUCTION',
    'news_construction_title_s2'          => ' UPDATES',
    'news_construction_desc_feb'          => 'February',
    'news_construction_desc_2017'         => '2017',
    'news_construction_desc_mh_lagoon'    => 'MahaSamutr Lagoon',
    'news_construction_desc_luxury_villa' => 'Luxury Villas',
    'news_construction_desc_villa_phase'  => 'Villa Phase',
    'news_construction_desc_no'           => 'No.',
    'news_construction_country_club'      => 'Country Club',
    'news_construction_sports_field'      => 'Sports Field',

//  DEVELOPMENT
//  - PACE DEV
    'dev_pace_desc_p1_l1' => 'PACE is Thailand’s leader in luxury development and hospitality. Providing a full range of lifestyle properties and services,',
    'dev_pace_desc_p1_l2' => 'PACE meets the needs of today and tomorrow using world-class design, introducing new trends to improve',
    'dev_pace_desc_p1_l3' => 'the urban landscape and enhance people\'s lives.',
    'dev_pace_desc_p2_l1' => 'PACE has a proven record of delivering luxury Bangkok condominiums including Ficus Lane, Saladaeng Residences and',
    'dev_pace_desc_p2_l2' => 'MahaNakhon, Thailand’s tallest building featuring The Ritz-Carlton Residences, Bangkok. From city living to beach side property and beyond,',
    'dev_pace_desc_p2_l3' => 'PACE delivers innovation and finest specification with the most sought after locations.',
    'dev_pace_desc_p3_l1' => 'The company also delivers innovation in the retail market with a strategy to build a portfolio of global food & beverage brands,',
    'dev_pace_desc_p3_l2' => 'including ownership of global brand DEAN & DELUCA.',
//  - TEAM
    'dev_team_title'   => 'TEAM',
    'dev_team_desc_l1' => 'MahaSamutr features a world-class team who are responsible for the design, construction and operation of',
    'dev_team_desc_l2' => 'Hua Hin’s first Private Country Club and Freehold Luxury Villa development.',
//      - CRYSTAL LAGOONS CORPORATION
        'dev_crystal_lagoon_title'   => 'Crystal Lagoons Corporation',
        'dev_crystal_lagoon_desc_l1' => 'The Crystal Lagoons has created and developed a pioneering',
        'dev_crystal_lagoon_desc_l2' => 'concept and state-of-the-art technology, which allows lagoons of',
        'dev_crystal_lagoon_desc_l3' => 'unlimited size to be built and maintained at very low cost, anywhere',
        'dev_crystal_lagoon_desc_l4' => 'in the world. Operating in more than 60 countries worldwide,',
        'dev_crystal_lagoon_desc_l5' => 'www.crystal-lagoons.com',
//      - THAI OBAYASHI
        'dev_thai_obayashi_title'   => 'Thai Obayashi',
        'dev_thai_obayashi_desc_l1' => 'Thai Obayashi is one of Thailand’s leading contractors, with a',
        'dev_thai_obayashi_desc_l2' => 'rich history, since founding in 1974. Engaged for construction of',
        'dev_thai_obayashi_desc_l3' => 'MahaSamutr’s lagoon, infrastructure, and villas, Thai Obayashi,',
        'dev_thai_obayashi_desc_l4' => 'with the support of the parent company, has served as a pioneer',
        'dev_thai_obayashi_desc_l5' => 'for the Thai construction industry.',
        'dev_thai_obayashi_desc_l6' => 'www.thaiobayashi.co.th',
//      - INTERNATIONAL LEISURE CONSULTANCES (ILC)
        'dev_ilc_title'   => 'International Leisure Consultants (ILC)',
        'dev_ilc_desc_l1' => 'ILC are Asia’s leading leisure, hospitality, spa, golf and',
        'dev_ilc_desc_l2' => 'recreation specialists. With a successful record of',
        'dev_ilc_desc_l3' => 'performance in the past 25+ years and more than',
        'dev_ilc_desc_l4' => '40 Club properties throughout Asia, ILC brings',
        'dev_ilc_desc_l5' => 'a wealth of experience and specialist knowledge as',
        'dev_ilc_desc_l6' => 'development and management consultants to the',
        'dev_ilc_desc_l7' => 'MahaSamutr Country Club.',
        'dev_ilc_desc_l8' => 'www.ilc-world.com',
//      - CBRE
        'dev_cbre_title'      => 'CBRE (Thailand)',
        'dev_cbre_desc_p1_l1' => 'The first international real estate consultant firm established in Bangkok',
        'dev_cbre_desc_p1_l2' => 'in June 1988, have grown to be the market leader in real estate services',
        'dev_cbre_desc_p1_l3' => 'in Thailand.',
        'dev_cbre_desc_p2_l1' => 'With their continued expansion, they now have approximately',
        'dev_cbre_desc_p2_l2' => '800 property professionals, enabling the firm to provide strategic advice',
        'dev_cbre_desc_p2_l3' => 'and execution for sales and leasing for all types of property, property and',
        'dev_cbre_desc_p2_l4' => 'project management, valuation and advisory, and research and consulting.',
        'dev_cbre_desc_p2_l5' => 'www.cbre.co.th',
//      * SHARED WORD
        'dev_shared_visit' => 'Visit: ',
//  - FACT SHEET
    'dev_factsheet_title' => 'Factsheet',
//      - COUNTRY CLUB
        'dev_factsheet_cc_title'            => 'MahaSamutr Country Club',
        'dev_factsheet_cc_developer_s1'     => 'Developer',
        'dev_factsheet_cc_developer_s2'     => 'PACE Country Club Co.,Ltd',
        'dev_factsheet_cc_location_s1'      => 'Location',
        'dev_factsheet_cc_location_s2'      => 'Soi Hua Hin 112',
        'dev_factsheet_cc_project_type_s1'  => 'Project Type',
        'dev_factsheet_cc_project_type_s2'  => 'Private Country Club and Lagoon',
        'dev_factsheet_cc_tenure_s1'        => 'Tenure',
        'dev_factsheet_cc_tenure_s2'        => 'Lifetime Membership',
        'dev_factsheet_cc_property_area_s1' => 'Property Area',
        'dev_factsheet_cc_property_area_s2' => 'approximately 120,000 sq.m (75 Rai)',
        'dev_factsheet_cc_lagoon_area_s1'   => 'Lagoon Area',
        'dev_factsheet_cc_lagoon_area_s2'   => 'approximately 72,000 sq.m (45 Rai)',
        'dev_factsheet_cc_club_house_s1'    => 'Club House Area',
        'dev_factsheet_cc_club_house_s2'    => '23,000 sq.m',
//      - VILLAS
        'dev_factsheet_lv_title'            => 'MahaSamutr Villas',
        'dev_factsheet_lv_developer_s1'     => 'Developer',
        'dev_factsheet_lv_developer_s2'     => 'PACE Development Corporation Plc.',
        'dev_factsheet_lv_location_s1'      => 'Location',
        'dev_factsheet_lv_location_s2'      => 'Soi Hua Hin 112',
        'dev_factsheet_lv_project_type_s1'  => 'Project Type',
        'dev_factsheet_lv_project_type_s2'  => 'Luxury Villas',
        'dev_factsheet_lv_tenure_s1'        => 'Tenure',
        'dev_factsheet_lv_tenure_s2'        => 'Freehold',
        'dev_factsheet_lv_property_area_s1' => 'Property Area',
        'dev_factsheet_lv_property_area_s2' => 'approximately 88,000 sq.m (55 Rai)',
        'dev_factsheet_lv_total_villas_s1'  => 'Total Villas',
        'dev_factsheet_lv_total_villas_s2'  => '80',
        'dev_factsheet_lv_villa_details_s1' => 'Villa Details',
        'dev_factsheet_lv_villa_details_s2' => '4 Bedrooms / 5 Bathrooms / 3 Parking Spaces',

//  CONTACT
    'contact_info_title' => 'Information',
    'contact_info_desc'  => 'To find more about MahaSamutr Country Club and Luxury Villas, please fill in the form below. Our Sales Representatives will provide you with a prompt response.',
//  - COUNTRY CLUB ENQUIRIES
    'contact_cc_enquiries_title' => 'Country Club Enquiries :',
//      - BANGKOK
        'contact_bkk_title'      => 'Bangkok Office',
        'contact_bkk_address_l1' => 'MahaNakhon CUBE, L1 Floor,',
        'contact_bkk_address_l2' => '96 Narathiwas Road, Silom,',
        'contact_bkk_address_l3' => 'Bangkok, 10500 Thailand',
        'contact_bkk_address_l4' => 'Tel. + 66 (0) 2 237 1414',
        'contact_bkk_address_l5' => 'Fax + 66 (0) 2 237 1415',
        'contact_bkk_address_l6' => 'Email : <a href="mailto:info@mahasamutr.com" class="link">info@mahasamutr.com</a>',
//      - HUA HIN
        'contact_huahin_title'      => 'Hua Hin Office',
        'contact_huahin_address_l1' => 'MahaSamutr Soi 112, Hua Hin,',
        'contact_huahin_address_l2' => 'Prachuap Khiri Khan 77110',
        'contact_huahin_address_l3' => 'Tel. + 66 (0) 32 907 900',
        'contact_huahin_address_l4' => 'Fax. + 66 (0) 32 510 315',
        'contact_huahin_address_l5' => 'Email : <a href="mailto:mahasamutr@cbre.co.th" class="link">info@mahasamutr.com</a>',
//  - VILLAS ENQUIRIES
    'contact_huahin_enquiries_title' => 'Luxury Villas Enquiries :',
    'contact_huahin_lv_address_l1'   => 'MahaSamutr Soi 112, Hua Hin,',
    'contact_huahin_lv_address_l2'   => 'Prachuap Khiri Khan 77110',
    'contact_huahin_lv_address_l3'   => 'Tel. + 66 (0) 32 907 900',
    'contact_huahin_lv_address_l4'   => 'Email : <a href="mailto:mahasamutr@cbre.co.th" class="link">info@mahasamutr.com</a>',
//  - CONTACT SECTION
    'contact_section_title'         => 'Title',
//      - TITLE
        'contact_section_mr'        => 'Mr.',
        'contact_section_mrs'       => 'Mrs.',
        'contact_section_miss'      => 'Miss',
    'contact_section_firstname'     => 'First Name',
    'contact_section_lastname'      => 'Last Name',
    'contact_section_mobile'        => 'Mobile No.',
    'contact_section_email'         => 'Email',
    'contact_section_address'       => 'Mailing Address',
    'contact_section_interested'    => 'Interested In',
//      - INTERESTED
        'contact_section_lv'        => 'Luxury Villas',
        'contact_section_pcc'       => 'Private Country Club - Lifetime Membership',
    'contact_section_hear'          => 'How did you hear about us?',
//      - HEAR
        'contact_section_news_mags' => 'Newspaper/Magazine',
        'contact_section_billboard' => 'Site Fence/Billboard',
        'contact_section_friend'    => 'Friend',
        'contact_section_social'    => 'Social Media',
        'contact_section_website'   => 'Website',
        'contact_section_other'     => 'Other',
    'contact_section_message'       => 'Message',

];