@extends('layouts.joinme_template')

@section('title')
    MahaSamutr
@endsection

@section('header')

@section('content')
    <section class="join-me-hero" style="padding-top: 3.625em;">
        <div class="container">
            <div class="headline">
                <h1 class="text-uppercase text-center">Join Me</h1>
                <div class="row">
                    <div class="col-lg-7 push-lg-3 offset-lg-1 col-xl-6 push-xl-3 offset-xl-2">
                        <p>MahaSamutr Country Club is Hua Hin’s first private country club, set alongside Asia’s largest Crystal Lagoon®, featuring MahaSamutr Luxury Villas. Bringing a sense of privacy and exclusivity, MahaSamutr provides a full spectrum of recreational water activities, sports, entertainment and facilities in a safe and luxurious environment.</p>
                        <p>MahaSamutr provides a true sense of community for business and for pleasure to enjoy year round. The extensive range of entertainment options, events calendar and programs provided have been created to meet the needs of members of every age, from children to the young at heart with individual, family, and corporate memberships.</p>
                    </div>
                    <div class="col-lg-3 pull-lg-7 col-xl-3 pull-xl-6">
                        <div class="card">
                            <img src="/custom/img/private-country-club/card.png" alt="">
                            <small>*Membership terms and conditions apply</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="join-me-banner">
        <div class="container-fluid">
            <div b-slider data-slick='{"autoplay":true,"autoplaySpeed": 5000,"arrows":false,"dots":true,"pauseOnHover":false,"pauseOnFocus": false}'>
                <div>
                    <img src="/custom/img/join-me/banner/3.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="join-me-celeb">
        <div class="container-fluid">
            <div class="headline" ms-scroll-trigger>
                <h3 class="text-uppercase"><span class="txt-1">Find out why our celebrity members joined</span><span class="txt-2">Mahasamutr country club</span></h3>
            </div>
            <div class="celeb-container">
                <div class="celeb-item">
                    <div class="celeb-thumb">
                        <img src="/custom/img/join-me/celeb/1.jpg" alt="">
                        <div class="thumb-overlay">
                            <div class="outer">
                                <div class="inner">
                                    <h6 class="text-white">Burin Boonvisut</h6>
                                    <div class="arrow-down"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="celeb-content-container">
                        <div class="celeb-content">
                            <div class="content-img">
                                <img src="/custom/img/join-me/celeb/1.jpg" alt="">
                            </div>
                            <div class="content-text">
                                <h5 class="name">Burin Boonvisut<span>Musician</span></h5>
                                <blockquote><h5>“Because music takes me to a special place.”</h5></blockquote>
                                <p>Arguably the smoothest crooner of our generation, Burin first came into the public spotlight as Groove Rider’s charismatic frontman. Since then, he has carved out a solo career as a celebrated singer, songwriter, and producer, lending his innate talents to countless collaborations across all musical genres. Whether alone at the microphone or backed by an entire orchestra, his music takes him—and his audience—to a place that feels like home.</p>
                                <button class="vdo-button" style="display: none;"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="celeb-item">
                    <div class="celeb-thumb">
                        <img src="/custom/img/join-me/celeb/2.jpg" alt="">
                        <div class="thumb-overlay">
                            <div class="outer">
                                <div class="inner">
                                    <h6 class="text-white">Nat Sarasas</h6>
                                    <div class="arrow-down"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="celeb-content-container">
                        <div class="celeb-content">
                            <div class="content-img">
                                <img src="/custom/img/join-me/celeb/2.jpg" alt="">
                            </div>
                            <div class="content-text">
                                <h5 class="name">Nat Sarasas<span>Hotelier and Cultural Entrepreneur</span></h5>
                                <blockquote><h5>“Because solitude is bliss”</h5></blockquote>
                                <p>Traveler of the world and master of the wild, Nat is an avid adventurer who is as comfortable in a sleeping bag under the stars as he is tucked in under a canopy bed of a 6-star luxury resort. Though his perpetual travels take him to the most exciting and far-flung corners of the earth, he still finds solace in simple activities, such as burying his nose in a good book, leisurely paddle boarding in the ocean, or a enjoying a relaxing weekend at ‘Baan Samlom’, his home in Hua Hin.</p>
                                <button class="vdo-button" style="display: none;"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="celeb-item">
                    <div class="celeb-thumb">
                        <img src="/custom/img/join-me/celeb/3.jpg" alt="">
                        <div class="thumb-overlay">
                            <div class="outer">
                                <div class="inner">
                                    <h6 class="text-white">M.L. Korkrita ‘Larn’ Kritakara</h6>
                                    <div class="arrow-down"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="celeb-content-container">
                        <div class="celeb-content">
                            <div class="content-img">
                                <img src="/custom/img/join-me/celeb/3.jpg" alt="">
                            </div>
                            <div class="content-text">
                                <h5 class="name">M.L. Korkrita ‘Larn’ Kritakara<span>DJ and Entrepreneur</span></h5>
                                <blockquote><h5>“Because a day on the water</h5><h5>is better than any day on land.”</h5></blockquote>
                                <p>Beat-maker and founder of ICONIC, a contemporary tile showroom-cum-design and music studio, Larn is not one for staying still. He has long been known for his eclectic interests and abilities: perhaps most impressive of all, his passion for extreme water sports (he’s the president of the Kiteboarding Association of Thailand). Whether windsurfing or kiteboarding, he can often be spotted gliding effortlessly through the air above his famed Hua Hin residence, 'Baan Bor Jued'.</p>
                                <button class="vdo-button" style="display: none;"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="celeb-item">
                    <div class="celeb-thumb">
                        <img src="/custom/img/join-me/celeb/4.jpg" alt="">
                        <div class="thumb-overlay">
                            <div class="outer">
                                <div class="inner">
                                    <h6 class="text-white">Jay, Jareyadee ‘Ple’ and Jake Spencer</h6>
                                    <div class="arrow-down"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="celeb-content-container">
                        <div class="celeb-content">
                            <div class="content-img">
                                <img src="/custom/img/join-me/celeb/4.jpg" alt="">
                            </div>
                            <div class="content-text">
                                <h5 class="name">Jay, Jareyadee ‘Ple’ and Jake Spencer<span>Entrepreneurs</span></h5>
                                <blockquote><h5>“Because families that play together stay together.”</h5></blockquote>
                                <p>This former perennial “it couple" has added one more to their entourage, proving that three is not a crowd but rather completeness. Since the birth of their first child, Jake, both Jay and Ple, who together founded Woof Pack, an architectural, interior, and design consultancy, have shifted their priorities to all things family orientated. The trio’s collective playtime now comprises pursuits and activities that can be enjoyed by all in the family.</p>
                                <button class="vdo-button" style="display: none;"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="celeb-item">
                    <div class="celeb-thumb">
                        <img src="/custom/img/join-me/celeb/5.jpg" alt="">
                        <div class="thumb-overlay">
                            <div class="outer">
                                <div class="inner">
                                    <h6 class="text-white">Polpat ‘Moo’ Asavaprapha</h6>
                                    <div class="arrow-down"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="celeb-content-container">
                        <div class="celeb-content">
                            <div class="content-img">
                                <img src="/custom/img/join-me/celeb/5.jpg" alt="">
                            </div>
                            <div class="content-text">
                                <h5 class="name">Polpat ‘Moo’ Asavaprapha<span>Fashion Designer and Entrepreneur</span></h5>
                                <blockquote><h5>“Because I love being at the center of all the action.”</h5></blockquote>
                                <p>Founder of Asava, one of the foremost fashion labels in Thailand, as well as Sava Dining, the brand’s eatery offshoot, Moo’s life is an endless flurry of beautiful fabrics, fabulous functions, and fantastical flavors. Uncompromising in both work and play, his leisurely endeavors demand the same kind of attention to detail, refinement, and consideration that he devotes to his own creations.</p>
                                <button class="vdo-button" style="display: none;"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="join-me-contact">
        <div class="container-fluid">
            <div class="join-me-form-container contact">
                <h3 class="join-me-title">Register</h3>
                <div class="contact-form">
                    <form id="join-me-form" action="" method="post" accept-charset="UTF-8" autocomplete="off">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>{!! Lang::get('messages.contact_section_title') !!}*</label>
                                    <select id="title" name="c-title" b-select-picker data-select-picker='{"placeholder":"Title"}'>
                                        <option value="" selected>{!! Lang::get('messages.contact_section_title') !!}*</option>
                                        <option value="Mr.">{!! Lang::get('messages.contact_section_mr') !!}</option>
                                        <option value="Mrs.">{!! Lang::get('messages.contact_section_mrs') !!}</option>
                                        <option value="Miss">{!! Lang::get('messages.contact_section_miss') !!}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label>{!! Lang::get('messages.contact_section_firstname') !!}*</label>
                                    <input id="first_name" name="c-firstname" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>{!! Lang::get('messages.contact_section_lastname') !!}*</label>
                                    <input id="last_name" name="c-lastname" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>{!! Lang::get('messages.contact_section_mobile') !!}*</label>
                                    <input id="mobile" name="c-mobile" type="tel" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>{!! Lang::get('messages.contact_section_email') !!}*</label>
                                    <input id="email" name="c-email" type="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>{!! Lang::get('messages.contact_section_address') !!}</label>
                                    <textarea id="address" name="c-address" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>{!! Lang::get('messages.contact_section_interested') !!}*</label>
                                    <select id="interested" name="c-interest" b-select-picker data-select-picker='{"placeholder":"Interested in"}' required>
                                        <option value="" selected>Interested in*</option>
                                        <option value="Private Country Club">PRIVATE COUNTRY CLUB</option>
                                        <option value="Luxury Villas">LUXURY VILLAS</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>{!! Lang::get('messages.contact_section_hear') !!}*</label>
                                    <select id="known_from" name="c-hear" b-select-picker data-select-picker='{"placeholder":"How did you hear about us?"}' required>
                                        <option value="" selected>How did you hear about us?</option>
                                        <option value="Newspaper/Magazine">Newspaper/Magazine</option>
                                        <option value="Site Fence/Billboard">Site Fence/Billboard</option>
                                        <option value="Friend">Friend</option>
                                        <option value="Social Media">Social Media</option>
                                        <option value="Website">Website</option>
                                        <option value="Other">Other (Please note)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>{!! Lang::get('messages.contact_section_message') !!}</label>
                                    <textarea id="message" name="c-message" class="form-control" rows="3" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="contact-form-bottom clearfix">
                            <span class="contact-field float-left">*Compulsory fields</span>
                            <button type="submit" class="btn-contact float-right" onclick="send()">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('modal')
    <div id="thank-modal" class="modal thank-modal" b-modal>
        <div class="modal-wrapper">
            <div class="modal-overlay"></div>
            <div class="va-outer">
                <div class="va-inner">
                    <div class="modal-content">
                        <div class="container-fluid">
                            <div class="content">
                                <div class="inner-content">
                                    <h6>Thank You For Your Registration</h6>

                                    <p>Your registration is currently being processed. We appreciate your interest in MahaSamutr Country Club & Luxury Villas.</p>
                                    <p>For any further enquiries, please contact <a href="tel:+66032907900">+66 (0) 32 907 900</a> or <a href="mailto:info@mahasamutr.com">info@mahasamutr.com</a>.  </p>

                                    <button b-modal-close id="modal-close">OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function() {
            $('#join-me-form').on('submit', function(e) {
                e.preventDefault(e);
                $.ajax({
                    type:"POST",
                    url:'/join',
                    data:$(this).serialize(),
                    dataType: 'json',
                    success: function(data){
                        console.log('success');
                    },
                    error: function(data){
                        console.log('error');
                    }
                });
            });
        });
        $('#modal-close').click(function() {
            $(':input','#join-me-form')
                .removeAttr('checked')
                .removeAttr('selected')
                .not(':button, :submit, :reset, :hidden, :radio, :checkbox')
                .val('');
        });
    </script>
@endsection
