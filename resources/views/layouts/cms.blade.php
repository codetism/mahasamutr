<!DOCTYPE html>
<html>
<head lang="en">
        <meta charset="UTF-8">
        <title>@yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>


        @yield('header')
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="#" class="navbar-brand">
                Content Management System
            </a>
            <button type="button" data-target="#userMenu, #mainMenu" data-toggle="collapse" class="navbar-toggle collapsed">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div id="userMenu" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ action('Admin\PasswordController@getChange') }}">Change Password</a></li>
                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
            </ul>
        </div>

    </div>

</nav>

<div class="container-fluid">

    <div class="row">
        <div class="col-md-2">
            <div role="navigation" id="mainMenu" class="collapse navbar-collapse">
                <ul class="nav nav-pills nav-stacked">
                    <li id="cms-subscriber" class="">
                        <a href="{{ url('/admin') }}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Subscriber</a>
                    </li>
                    <li id="cms-contact" class="">
                        <a href="{{ url('/admin/message') }}"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Contact Message</a>
                    </li>
                    <li id="cms-member" class="">
                        <a href="{{ url('/admin/member') }}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Member</a>
                    </li>
                    <li id="cms-member" class="">
                        <a href="{{ url('/admin/construction_update') }}"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Construction Update</a>
                    </li>
                    <li id="cms-member" class="">
                        <a href="{{ url('/admin/news') }}"><span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span> News</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-10">
            @yield('content')
        </div>
    </div>
</div>
<script>
    $(document).bind("ajaxSend", function (elm, xhr, s) {
        if (s.type == "DELETE" || s.type == "POST") {
            xhr.setRequestHeader('X-CSRF-Token', '{{ csrf_token() }}');
        }
    });
</script>
    @yield('script')
</body>
</html>