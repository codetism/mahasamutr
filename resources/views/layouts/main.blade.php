<!---------------------- start : metatag.php ---------------------->
<?php
    $full_name = $_SERVER[ 'PHP_SELF' ];
    $page_name = Route::getCurrentRoute()->getPath();
    $lang = strpos( $full_name, '/en/' ) ? 'en' : 'th';

    $menu = 'Menu';
    $home = 'Home';
    $about = 'About Us';
?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]> <html class="lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->

    <head>
        <!-– Google Tag Manager -–>
        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-WRWD22Q');
        </script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-97000617-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!-– End Google Tag Manager –->

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="HandheldFriendly" content="true">

        <title>
            @yield('title')
        </title>
        <meta name="description" content="">
        <meta name="keywords" content="">

        <!-- GOOGLE PLUS -->
        <meta itemprop="name" content="">
        <meta itemprop="description" content="">
        <meta itemprop="image" content="">
        <meta itemprop="url" content="">
        <meta itemprop="alternateName" content="">

        <!-- META OPEN GRAPH (FB) -->
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:title" content="">
        <meta property="og:description" content="">
        <meta property="og:image" content="">
        <meta property="og:site_name" content="">
        <meta property="og:locale" content="">
        <meta property="og:locale:alternate" content="">

        <!-- TWITTER CARD -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:title" content="">
        <meta name="twitter:description" content="">
        <meta name="twitter:url" content="">
        <meta name="twitter:image:src" content="">

        <!-- Add to homescreen for Chrome on Android -->
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="application-name" content="">

        <!-- Add to homescreen for Safari on iOS -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="">

        <!-- Tile icon for Win8 (144x144 + title color) -->
        <meta name="msapplication-TileImage" content="/favicon/apple-touch-icon-144x144.png">
        <meta name="msapplication-TileColor" content="">

        <!--[if IE]><link rel="shortcut icon" href="/favicon/favicon.ico"><![endif]-->
        <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
        <meta name="msapplication-square70x70logo" content="/favicon/smalltile.png"/>
        <meta name="msapplication-square150x150logo" content="/favicon/mediumtile.png"/>
        <meta name="msapplication-wide310x150logo" content="/favicon/widetile.png"/>
        <meta name="msapplication-square310x310logo" content="/favicon/largetile.png"/>

        <!-- Color the status bar on mobile devices -->
        <meta name="theme-color" content="">
        <!-- CSS -->
        <link rel="stylesheet" href="/css/main.min.css">
        <link rel="stylesheet" href="/css/custom.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="/js/vendors/html5shiv/html5shiv.min.js"></script>
        <script type="text/javascript" src="/js/vendors/respond.js/respond.min.js"></script>
        <![endif]-->

        <style>
            @media (min-width: 992px){
                .header-top{
                    width: 80%;
                }
                .header-bottom.container {
                    margin-left: 0px;
                    margin-right: 0px;
                }
                .menu-top ul li:after {
                    content: "";
                    position: absolute;
                    top: -4px;
                    right: -20px;
                    display: block;
                    background-image: none;
                    background-repeat: no-repeat;
                    width: 10px;
                    height: 21px;
                }

                .menu-top {
                    display: block;
                    position: absolute;
                    right: 0;
                    top: 0;
                    padding: 35px 15px 35px 44px;
                }
                .lang {
                    vertical-align: middle;
                    display: block;
                    max-width: 100%;
                    height: auto;
                    color: #4FBDC7;
                    margin: 0px;
                    font-size: 14px;
                    letter-spacing: 1px;
                }
            }
            .header-top .logo-icon {
                margin-left: 15px;
            }
            @media(min-width: 992px) and (max-width: 1106px) {
                header.is-fixed .menu-top {
                    width: 76px;
                    margin-right: 15px;
                    padding: 25px 0 28px 0;
                }
                header.is-fixed .menu-top .join-me-bt {
                    margin-right: 0;
                }
                header.is-fixed .menu-top #en {
                    margin-left: 13px;
                }
            }
            .footer-menu li a {
                padding-top: 4px;
            }
        </style>
    </head>
<!---------------------- end : metatag.php ---------------------->

    <body class="">
        <!–- Google Tag Manager (noscript) -–>
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WRWD22Q" height="0" width="0" style="display:none;visibility:hidden">
            </iframe>
        </noscript>
        <!–- End Google Tag Manager (noscript) -–>

<!---------------------- start : loading.php ---------------------->
        <div class="loading-container" b-loader>
            <div class="loading"><img src="/img/logo/mahasamutr-logo-icon.svg" alt="Mahasamutr"></div>
        </div>
<!---------------------- end : loading.php ---------------------->

        <div id="page">

<!---------------------- start : header.php ---------------------->
            <header class="header">
                <div ms-parallax style="position: relative;">
                    <?php
                        if(!\Illuminate\Support\Facades\Session::get('locale')) {
                            $session_en = false;
                            $session_th = true;
                        } else {
                            $session_en = (\Illuminate\Support\Facades\Session::get('locale') == 'en');
                            $session_th = (\Illuminate\Support\Facades\Session::get('locale') == 'th');
                        }
                    ?>
                    <div class="menu-top">
                        <ul>
                            <li>
                                <a href="/join-me" class="join-me-bt text-uppercase"><span>Join me</span></a>
                            </li>
                            <li style="margin-right: 0px;">
                                <a id="en" href="/en">
                                    <p class="lang" style="padding-right: 5px;">
                                        <span style="<?php if($session_en) echo 'border-bottom: solid 2px #00B2C0' ?>">EN</span> |
                                    </p>
                                </a>
                            </li>
                            <li>
                                <a id="th" href="/th">
                                    <p class="lang">
                                        <span style="<?php if($session_th) echo 'border-bottom: solid 2px #00B2C0' ?>">TH</span>
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="header-top container" ms-parallax>
                    <a href="/" class="logo"><img src="/img/logo/mahasamutr-logo-1.png" alt="Mahasamutr"></a>
                </div>
                <div class="header-top" ms-parallax>
                    <a href="/" class="logo-icon"><img src="/img/logo/mahasamutr-logo-icon.svg" alt="Mahasamutr"></a>
                </div>

                <div class="header-bottom container" ms-parallax>
                    <nav class="main-menu-container">
                        <ul class="main-menu anim-line-bottom">
                            <li><a href="/about" class="<?php echo ($page_name=='about')?'current':'';?>">{!! Lang::get('messages.menu_about') !!}</a>
                            </li>
                            <li><a href="/location" class="<?php echo ($page_name=='location')?'current':'';?>">{!! Lang::get('messages.menu_location') !!}</a>
                            </li>
                            <li class="has-children"><a href="/private-country-club" class="<?php echo ($page_name=='private-country-club')?'current':'';?>">{!! Lang::get('messages.menu_pcc') !!}</a>
                                <ul>
                                    <li><a href="/private-country-club#concept" b-scroll-to="#concept">{!! Lang::get('messages.menu_concept') !!}</a>
                                    </li>
                                    <li><a href="/private-country-club#mahasamutr-lagoon" b-scroll-to="#mahasamutr-lagoon">{!! Lang::get('messages.menu_mh_lagoon') !!}</a>
                                    </li>
                                    <li><a href="/private-country-club#water-sport" b-scroll-to="#water-sport">{!! Lang::get('messages.menu_sports_amenities') !!}</a>
                                    </li>
                                    <li><a href="/private-country-club#memberships-and-benefits" b-scroll-to="#memberships-and-benefits">{!! Lang::get('messages.menu_membership_benefits') !!}</a>
                                    </li>
                                    <li><a href="/private-country-club#faqs" b-scroll-to="#faqs">{!! Lang::get('messages.menu_faq') !!}</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-children"><a href="/luxury-villas" class="<?php echo ($page_name=='luxury-villas')?'current':'';?>">{!! Lang::get('messages.menu_lv') !!}</a>
                                <ul>
                                    <li><a href="/luxury-villas#villa-concept" b-scroll-to="#villa-concept">{!! Lang::get('messages.menu_concept') !!}</a></li>
                                    <li><a href="/luxury-villas#plots-layout" b-scroll-to="#plots-layout">{!! Lang::get('messages.menu_plots') !!}</a></li>
                                    <li><a href="/luxury-villas#virtual-tour" b-scroll-to="#virtual-tour">{!! Lang::get('messages.menu_vdo') !!}</a></li>
                                    <li><a href="/luxury-villas#owners-benefits" b-scroll-to="#owners-benefits">{!! Lang::get('messages.menu_owner_benefits') !!}</a></li>
                                    <li><a href="/luxury-villas#faqs" b-scroll-to="#faqs">{!! Lang::get('messages.menu_faq') !!}</a></li>
                                </ul>
                            </li>
                            <li class="has-children"><a href="/gallery" class="<?php echo ($page_name=='gallery')?'current':'';?>">{!! Lang::get('messages.menu_gallery') !!}</a>
                                <ul>
                                    <li><a href="/gallery#country-club" id="submenu-country" b-scroll-to="#ms-gallery-container">{!! Lang::get('messages.menu_cc') !!}</a></li>
                                    <li><a href="/gallery#luxury-villas" id="submenu-luxury" b-scroll-to="#ms-gallery-container">{!! Lang::get('messages.menu_lv') !!}</a></li>
                                </ul>
                            </li>
                            <li class="has-children"><a href="/news" class="<?php echo ($page_name=='news')?'current':'';?>">{!! Lang::get('messages.menu_news') !!}</a>
                                <ul>
                                    <li><a href="/news#events-and-activities" b-scroll-to="#events-and-activities">{!! Lang::get('messages.menu_event') !!}</a></li>
                                    <li><a href="/news#construction-update" b-scroll-to="#construction-update">{!! Lang::get('messages.menu_construction') !!}</a></li>
                                </ul>
                            </li>
                            <li class="has-children"><a href="/development" class="<?php echo ($page_name=='development')?'current':'';?>">{!! Lang::get('messages.menu_development') !!}</a>
                                <ul>
                                    <li><a href="/development#team" b-scroll-to="#team">{!! Lang::get('messages.menu_team') !!}</a>
                                    </li>
                                    <li><a href="/development#fact-sheet" b-scroll-to="#fact-sheet">{!! Lang::get('messages.menu_fs') !!}</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="/contact" class="<?php echo ($page_name=='contact')?'current':'';?>">{!! Lang::get('messages.menu_contact_us') !!}</a>
                            </li>
                        </ul>
                        <div class="menu-social-link">
                            <a href=""><i class="icon-facebook"></i></a>
                            <a href=""><i class="icon-instagram"></i></a>

                        </div>
                    </nav>

                    <div class="hamburger">
                        <div class="hamburger-outer">
                            <div class="hamburger-inner">
                                <div class="hamburger-line"></div>
                            </div>
                        </div>
                    </div>
                </div>
                @yield('header')
            </header>
<!---------------------- end : header.php ---------------------->

            <main class="main">
                @yield('content')
            </main>

<!---------------------- start : footer.php ---------------------->
            <footer class="footer">
                <div class="footer-top">
                    <div class="section-full">
                        <figure>
                            <picture>
                                <source srcset="/img/footer/bg_w_1800.jpg" media="(min-width: 1366px)">
                                <img src="/img/footer/bg_w_1366.jpg" alt="">
                            </picture>
                        </figure>
                    </div>
                    <div class="footer-top-inner">
                        <div class="container">
                            <a href="/" class="footer-logo d-inline-block">
                                <img src="/img/logo/mahasamutr-footer-logo.png" alt="">
                            </a>
                            <address>
                                <p class="text-white">{!! Lang::get('messages.footer_address') !!}</p>
                                <ul>
                                    <li><i class="icon-phone"></i> +66 (0) 32 907 900</li>
                                    <li><a href="mailto:info@mahasamutr.com"><i class="icon-envelope"></i> info@mahasamutr.com</a>
                                    </li>
                                </ul>
                            </address>
                            <div class="footer-newsletter">
                                <h4 class="text-uppercase">{!! Lang::get('messages.footer_subscription') !!}</h4>
                                <form class="newsletter-form" action="/subscribe" method="post" accept-charset="UTF-8" autocomplete="off">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="email" name="ns-email" id="ns-email" value="" placeholder="{!! Lang::get('messages.footer_email_field') !!}">
                                    <button type="submit">Submit</button>
                                </form>
                            </div>
                            <div class="footer-menu">
                                <ul class="footer-main-menu d-flex justify-content-between">
                                    <li><a href="/about">{!! Lang::get('messages.menu_about') !!}</a>
                                    </li>
                                    <li><a href="/location">{!! Lang::get('messages.menu_location') !!}</a>
                                    </li>
                                    <li><a href="/private-country-club">{!! Lang::get('messages.menu_pcc') !!}</a>
                                    </li>
                                    <li><a href="/luxury-villas">{!! Lang::get('messages.menu_lv') !!}</a>
                                    </li>
                                    <li><a href="/gallery">{!! Lang::get('messages.menu_gallery') !!}</a>
                                    </li>
                                    <li><a href="/news">{!! Lang::get('messages.menu_news') !!}</a>
                                    </li>
                                    <li><a href="/development">{!! Lang::get('messages.menu_development') !!}</a>
                                    </li>
                                    <li><a href="/contact">{!! Lang::get('messages.menu_contact_us') !!}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="container">
                        <div class="footer-item social-link">
                            <p>Follow Us :</p>
                            <ul>
                                <li><a href="https://www.facebook.com/MahaSamutr/" target="_blank"><i class="icon-facebook"></i></a>
                                </li>
                                <li><a href="https://www.instagram.com/mahasamutr_huahin/" target="_blank"><i class="icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer-item copyright">
                            <ul>
                                <li><a href="#" data-modal-trigger="#disclamer-modal">Disclaimer</a>
                                </li>
                                <li>&copy; 2017 MahaSamutr Country Club & Luxury Villas Hua Hin. All rights reserved.</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="scroll-top" b-scroll-to="top"><span>TOP</span></div>
<!---------------------- end : footer.php ---------------------->

        </div>

<!---------------------- start : js.php ---------------------->
    <!---------------------- start : disclaimer-modal.php ---------------------->
        <div id="disclamer-modal" class="modal" b-modal>
            <div class="modal-wrapper">
                <div class="modal-overlay"></div>
                <div class="va-outer">
                    <div class="va-inner">
                        <div class="container">
                            <div class="row justify-content-lg-center">
                                <div class="col-lg-10">
                                    <div class="modal-content" b-animation>
                                        <div class="scrollbar-wrapper">
                                            <div class="scrollbar-content">
                                                <p class="disclamer-title text-uppercase">Disclaimer for MahaSamutr Villas</p>
                                                <p>Name of the Project: MahaSamutr Villas; Land Developer: Pace Development Corporation Public Company Limited; Chief Executive Officer: Mr. Sorapoj Techakraisri; Registered Capital: 4,078,028,197 Baht (Registered Capital Paid: 3,758,028,197 Baht); Registered Address: No. 87/2, CRC Tower, All Seasons Place, 45th Floor, Unit 3, Wireless Road, Pathumwan, Lumpini, Bangkok; Land Development License No. 13/2559; Construction Commencement: October 2015; Construction Completion (approx.): December 2017; Site Location: Tabtai Sub-district, Hua Hin District, Prachuapkhirikhan Province; MahaSamutr Villas are located on land numbers: 112, 528 and 531; title deed numbers: 50303 (partial), 78633 (partial) and 78636 ; MahaSamutr Villas Area (approx.) : 54 Rai 98.7 Square Wah; Land and building are encumbered with Siam Commercial Bank Public Company Limited; Upon the payment completion in accordance with the contract, the Land Developer shall transfer the ownership over the land to the Buyer by December 2017; the advertisement pictures are illustrative. Images and specifications may be subject to change, and the Land Developer reserves the right to make adjustments as a result of changes to regulations or laws at time of construction; and/or, inconsistencies arising from illustrations or artist impressions which shall serve only as indications for general concepts of the project. The areas of Land Allocation Project are not include Lagoon and Country Club, which is privately owned by Pace Country Club Company Limited. The Country Club’s membership is subject to its by-laws, rules, and requirements. The developer reserves the right to amend any information and/or conditions contained herein at its sole discretion.</p>
                                                <p class="disclamer-title text-uppercase">Disclaimer for MahaSamutr Country Club</p>
                                                <p>Name of the Project: MahaSamutr Country Club; Developer: Pace Country Club Company Limited, having its 99.97% shares held by Pace Development Corporation Public Company Limited; Chief Executive Officer: Mr. Sorapoj Techakraisri; Registered Address: No. 87/2, CRC Tower, All Seasons Place, 45th Floor, Wireless Road, Pathumwan, Lumpini, Bangkok; Site Location: Hua Hin District, Prachuap Khiri Khan Province; the advertisement pictures are illustrative. Images and specifications may be subject to change, and the Developer reserves the right to make adjustments to the information and/or illustrations as a result of changes to regulations or laws at time of construction; and/or, inconsistencies arising from illustrations or artist impressions which shall serve only as indications for general concepts of the project. Lagoon and Country Club is privately owned by Pace Country Club Company Limited. The Country Club’s membership is subject to its by-laws, rules, and requirements. The developer reserves the right to amend any information and/or conditions contained herein at its sole discretion.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-close-bt" b-modal-close>
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="512px" height="512px" viewBox="232 -232 512 512" style="enable-background:new 232 -232 512 512;" xml:space="preserve">
                    <polygon points="607.8,-88.7 596.3,-99.8 488.2,12.1 380,-99.8 368.5,-88.7 477.1,23.7 368.5,136 380,147.2 488.2,35.2
                    596.3,147.2 607.8,136 499.3,23.7 		"/>
                </svg>
            </div>
        </div>
    <!---------------------- end : disclaimer-modal.php ---------------------->


        <script src="/js/vendors/jquery/jquery.min-2.1.1.js"></script>
        <script src="/js/vendors/modernizr/modernizr.js"></script>
        <script src="/js/vendors/jquery.mousewheel.min.js"></script>
        <script src="/js/vendors/gsap/TweenMax.min.js"></script>
        <script src="/js/vendors/gsap/plugins/ScrollToPlugin.min.js"></script>
        <script src="/js/vendors/scrollmagic/ScrollMagic.min.js"></script>
        <script src="/js/vendors/scrollmagic/plugins/animation.gsap.min.js"></script>
        <script src="/js/vendors/scrollmagic/plugins/debug.addIndicators.min.js"></script>
        <script src="/js/vendors/picturefill/picturefill.min.js"></script>
        <script src="/js/vendors/jquery-placeholder/jquery.placeholder.min.js"></script>
        <script src="/js/vendors/select2/select2.min.js"></script>
        <script src="/js/vendors/slick-carousel/slick.min.js"></script>
        <script src="/js/vendors/shuffle.min.js"></script>
        <script src="/js/vendors/jquery.mCustomScrollbar.min.js"></script>

        <script src="/js/base/global.js"></script>
        <script src="/js/base/base.js"></script>

        <script src="/js/utility/scroll-to-tweenmax.js"></script>
        <script src="/js/utility/viewport.js"></script>
        <script src="/js/utility/detect.js"></script>
        <script src="/js/utility/b-mousewheel.js"></script>

        <script src="/js/components/loader.js"></script>
        <script src="/js/components/menu.js"></script>
        <script src="/js/components/modal.js"></script>
        <script src="/js/components/accordion.js"></script>
        <script src="/js/components/tab.js"></script>
        <script src="/js/components/scroll-scrollmagic.js"></script>
        <script src="/js/components/ms-parallax.js"></script>
        <script src="/js/components/ms-parallax-section.js"></script>
        <script src="/js/components/select-picker.js"></script>
        <script src="/js/components/google-map.js"></script>

        <script src="/js/features/slider/slide-slick.js"></script>

        <script src="/js/page/index.js"></script>
        <script src="/js/page/gallery.js"></script>
        <script src="/js/page/news.js"></script>
        <script src="/js/page/luxury.js"></script>

        <script src="/js/main.js"></script>
<!---------------------- end : js.php ---------------------->
        <script>
            $(document).ready(function() {
                $('.langen').click(function ($event) {
                    $event.preventDefault();
                    var url = $('#langen').attr('href');
                    $.ajax({type:'get', url:url}).done(function () {
                        window.location.reload();
                    });
                });
                $('.langth').click(function ($event) {
                    $event.preventDefault();
                    var url = $('#langth').attr('href');
                    $.ajax({type:'get', url:url}).done(function () {
                        window.location.reload();
                    });
                });
                if(<?php echo $session_th ?>) {
                    var btn = $('.btn span');
                    btn.css('top', '-5px');
                    btn.css('bottom', '5px');
                }
            });
        </script>
        @yield('script')

        @yield('modal')

    </body>

</html>
