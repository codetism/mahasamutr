@extends('layouts.main')

@section('title')
    Gallery | MahaSamutr
@endsection

@section('header')

@section('content')
    <section class="section-hero hero-gallery">
        <div class="container-fluid">
            <div class="hero-block">
                <div class="hero-full" ms-parallax>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/banner.jpg" media="(min-width: 1366px)">
                            <img src="/img/gallery/banner.jpg" alt="">
                        </picture>
                    </figure>
                    <div class="wrapper-panel">
                        <div class="play" style="display:none;">
                            <a href="#" class="button-play video-link" data-video="D-6k6VsjsUU">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="104px" height="104px" viewBox="23.9 -25.9 104 104" style="enable-background:new 23.9 -25.9 104 104;"
                                     xml:space="preserve">
                                    <path d="M75.9-25.9c-28.7,0-52,23.3-52,52s23.3,52,52,52s52-23.3,52-52S104.6-25.9,75.9-25.9z M75.9,76C48.4,76,26,53.6,26,26.1
                                            c0-27.5,22.4-49.9,49.9-49.9s49.9,22.4,49.9,49.9C125.9,53.6,103.5,76,75.9,76z"/>
                                    <path d="M96.4,22.1L70.1,5.4c-1.5-0.9-3.4-1-5-0.2c-1.5,0.8-2.6,2.4-2.6,4.1v33.4c0,1.7,1.1,3.3,2.6,4.1c0.7,0.4,1.6,0.6,2.4,0.6
                                            c0.9,0,1.8-0.3,2.6-0.7l26.3-16.7c1.4-0.9,2.2-2.4,2.2-4C98.6,24.5,97.8,23,96.4,22.1z M95.4,28.5L69.1,45.2c-0.9,0.6-2.1,0.6-3,0.1
                                            c-0.9-0.5-1.6-1.4-1.6-2.5V9.4c0-1,0.7-2,1.6-2.5c0.4-0.2,1-0.4,1.4-0.4c0.5,0,1.1,0.1,1.5,0.4l26.3,16.7c0.8,0.5,1.3,1.4,1.3,2.4
                                            C96.6,27.1,96.2,28,95.4,28.5z"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="scroll-down" b-scroll-to="#ms-gallery-container">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 34">
                            <path class="st0" d="M11,33L11,33C5.5,33,1,28.5,1,23V11C1,5.5,5.5,1,11,1h0c5.5,0,10,4.5,10,10v12C21,28.5,16.5,33,11,33z"></path>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                            <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                            <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                        </svg>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section id="ms-gallery-container" class="gallery">
        <div class="container-fluid">
            <div class="headline text-center" ms-scroll-trigger>
                <h3 class="text-uppercase">
                    <span class="txt-1 d-block">{!! Lang::get('messages.gallery_main_title_l1') !!}</span>
                    <span class="txt-2 d-block">{!! Lang::get('messages.gallery_main_title_l2') !!}</span>
                </h3>
            </div>
            <ul class="gallery-tab" ms-scroll-trigger>
                <li><a href="#" class="btn current" data-group="luxury"><span>{!! Lang::get('messages.gallery_tab_luxury_villas') !!}</span></a></li>
                <li><a href="#" class="btn" data-group="country"><span>{!! Lang::get('messages.gallery_tab_private_country_club') !!}</span></a></li>
            </ul>
            <div id="ms-gallery" class="gallery-block">
                <!-- 1 -->
                <div class="c-block ratio-gallery ratio-gallery-vertical gallery-top" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_01_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_01_w_1337.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/luxury-villas/luxury_villas_01_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-lg gallery-bottom" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_02_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_02_w_1333.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/luxury-villas/luxury_villas_02_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-sm" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_03_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_03_w_1329.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/luxury-villas/luxury_villas_03_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-sm" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_04_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_04_w_1346.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/luxury-villas/luxury_villas_04_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <img src="/img/gallery/luxury-villas/luxury_villas_13.jpg" alt="">
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-sm" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_06_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_06_w_1340.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/luxury-villas/luxury_villas_06_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-sm" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_07_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_07_w_1396.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/luxury-villas/luxury_villas_07_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_11_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_11_w_1386.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/luxury-villas/luxury_villas_11_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-sm" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_09_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_09_w_1337.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/luxury-villas/luxury_villas_09_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-sm" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_10_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_10_w_1331.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/luxury-villas/luxury_villas_10_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-sm" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_08_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_08_w_1338.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/luxury-villas/luxury_villas_08_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-sm" ms-lightbox data-groups='["luxury"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_12_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/luxury-villas/luxury_villas_12_w_1332.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/luxury-villas/luxury_villas_12_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <!-- 2 -->
                <div class="c-block ratio-gallery ratio-gallery-horizontal" ms-lightbox data-groups='["country"]'>
                    <figure>

                        <picture>
                            <source srcset="/img/gallery/country-club/country_club_01_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/country-club/country_club_01_w_1425.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/country-club/country_club_01_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-vertical" ms-lightbox data-groups='["country"]'>
                    <figure>
                        <picture>

                            <source srcset="/img/gallery/country-club/country_club_02_w_988.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/country-club/country_club_02_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery" ms-lightbox data-groups='["country"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/country-club/150930_pool white wall.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/country-club/150930_pool white wall.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/country-club/150930_pool white wall.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-sm" ms-lightbox data-groups='["country"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/country-club/country_club_04_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/country-club/country_club_04_w_1319.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/country-club/country_club_04_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery" ms-lightbox data-groups='["country"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/country-club/country_club_05_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/country-club/country_club_05_w_1418.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/country-club/country_club_05_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-sm" ms-lightbox data-groups='["country"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/country-club/country_club_06_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset=/img/gallery/country-club/country_club_06_w_1327.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/country-club/country_club_06_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-gallery ratio-gallery-sm" ms-lightbox data-groups='["country"]'>
                    <figure>
                        <picture>
                            <source srcset="/img/gallery/country-club/country_club_07_w_2050.jpg" media="(min-width: 1366px)">
                            <source srcset="/img/gallery/country-club/country_club_07_w_1334.jpg" media="(min-width: 768px)">
                            <img src="/img/gallery/country-club/country_club_07_w_768.jpg" alt="">
                        </picture>
                        <figcaption class="d-flex justify-content-center align-items-center">
                            <i class="icon-photo-camera"></i>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </section>
@endsection

@saection('script')

@section('modal')
    <div id="gallery-modal" class="modal gallery-modal" b-modal>
        <div class="modal-wrapper">
            <div class="modal-overlay"></div>
            <div class="va-outer">
                <div class="va-inner">
                    <div class="modal-content">
                        <div class="container-fluid">
                            <div class="content">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-close-bt" b-modal-close>
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="512px" height="512px" viewBox="232 -232 512 512" style="enable-background:new 232 -232 512 512;" xml:space="preserve">
                <polygon points="607.8,-88.7 596.3,-99.8 488.2,12.1 380,-99.8 368.5,-88.7 477.1,23.7 368.5,136 380,147.2 488.2,35.2
                    596.3,147.2 607.8,136 499.3,23.7"/>
            </svg>
        </div>
    </div>
    <div class="video-container-modal">
        <a href="#" class="video-modal-close">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                width="512px" height="512px" viewBox="232 -232 512 512" style="enable-background:new 232 -232 512 512;" xml:space="preserve">
                <polygon points="607.8,-88.7 596.3,-99.8 488.2,12.1 380,-99.8 368.5,-88.7 477.1,23.7 368.5,136 380,147.2 488.2,35.2
                    596.3,147.2 607.8,136 499.3,23.7"/>
            </svg>
        </a>
    </div>
@endsection
