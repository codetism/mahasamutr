@extends('layouts.main')

@section('title')
    Location | MahaSamutr
@endsection

@section('header')

@section('content')
    <section class="section-hero location">
        <div class="container-fluid">
            <div class="hero-block">
                <div class="hero-full" ms-parallax>
                    <figure>
                        <picture>
                            <source srcset="/img/location/banner_w_2560.jpg" media="(min-width: 1920px)">
                            <source srcset="/img/location/banner_w_2023.jpg" media="(min-width: 1366px)">
                            <img src="/img/location/banner_w_1366.jpg" alt="">
                        </picture>
                    </figure>
                    <div class="wrapper-panel">
                        <div class="hero-content" ms-scroll-trigger>
                            <h3 class="text-white text-uppercase"><span class="txt-1 d-block">{!! Lang::get('messages.location_banner_title_l1') !!}</span><span class="txt-2 d-block">{!! Lang::get('messages.location_banner_title_l2') !!}</span></h3>
                            <p class="text-white"><span class="d-md-block">{!! Lang::get('messages.location_banner_desc_l1') !!}</span>
                                <span class="d-md-block">{!! Lang::get('messages.location_banner_desc_l2') !!}</span>
                                <span class="d-md-block">{!! Lang::get('messages.location_banner_desc_l3') !!}</span></p>
                        </div>
                    </div>
                </div>
                <div class="scroll-down" b-scroll-to="#huahin">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 34">
                        <path class="st0" d="M11,33L11,33C5.5,33,1,28.5,1,23V11C1,5.5,5.5,1,11,1h0c5.5,0,10,4.5,10,10v12C21,28.5,16.5,33,11,33z"></path>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                    </svg>
                    <div></div>
                </div>
            </div>
        </div>
    </section>
    <section id="huahin" class="location-2">
        <div class="container-fluid">
            <div class="c-block ratio-location" >
                <figure>
                    <picture>
                        <source srcset="/img/location/hua-hin_w_2560.jpg" media="(min-width: 1920px)">
                        <source srcset="/img/location/hua-hin_w_2025.jpg" media="(min-width: 1366px)">
                        <img src="/img/location/hua-hin_w_1366.jpg" alt="">
                    </picture>
                </figure>
                <div class="c-block-content" ms-scroll-trigger>
                    <h3 class="text-uppercase text-white">{!! Lang::get('messages.location_hua_hin_title') !!}</h3>
                    <p class="text-white"><span class="d-sm-block">{!! Lang::get('messages.location_hua_hin_desc_p1_l1') !!}</span> <span class="d-sm-block">{!! Lang::get('messages.location_hua_hin_desc_p1_l2') !!}</span></p>
                    <p class="text-white"><span class="d-sm-block">{!! Lang::get('messages.location_hua_hin_desc_p2_l1') !!}</span> <span class="d-sm-block">{!! Lang::get('messages.location_hua_hin_desc_p2_l2') !!}</span></p>
                </div>
            </div>
        </div>
    </section>
    <section class="location-3">
        <div class="container-fluid">
            <div class="location-3-blocks">
                <div class="c-block ratio-location">
                    <figure>
                        <picture>
                            <img src="/img/location/hua-hin-2.jpg" alt="">
                        </picture>
                    </figure>
                </div>
                <div class="c-block-content text-center">
                    <div class="outer">
                        <div class="inner" ms-scroll-trigger>
                            <h5 class="text-uppercase text-white">
                                <span>{!! Lang::get('messages.location_township_desc_p1_l1') !!}</span>
                                <span>{!! Lang::get('messages.location_township_desc_p1_l2') !!}</span>
                                <span>{!! Lang::get('messages.location_township_desc_p1_l3') !!}</span>
                            </h5>
                            <h5 class="text-uppercase text-white">
                                <span>{!! Lang::get('messages.location_township_desc_p2_l1') !!}</span>
                                <span>{!! Lang::get('messages.location_township_desc_p2_l2') !!}</span>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="soi112 text-center">
        <div class="container-fluid">
            <div class="headline" ms-scroll-trigger>
                <div class="luxury-logo">
                    <img src="/img/bg/screen-logo.png">
                </div>
                <h3>{!! Lang::get('messages.location_112_title') !!}</h3>
                <p>
                    <span class="d-sm-block">{!! Lang::get('messages.location_112_desc_l1') !!}</span>
                    <span class="d-sm-block">{!! Lang::get('messages.location_112_desc_l2') !!}</span>
                </p>
            </div>
            <div class="c-block ratio-soi112">
                <figure>
                    <picture>
                        <picture>
                            <source srcset="/img/location/hua-hin-3_w_2560.jpg" media="(min-width: 1920px)">
                            <source srcset="/img/location/hua-hin-3_w_2051.jpg" media="(min-width: 1366px)">
                            <img src="/img/location/hua-hin-3_w_1366.jpg" alt="">
                        </picture>
                    </picture>
                </figure>
            </div>
        </div>
    </section>
    <section class="map">
        <a target="_blank" href="https://www.google.co.th/maps/place/MahaSamutr+Country+Club+%26+Luxury+Villas+Hua+Hin/@12.52348,99.935879,11z/data=!4m12!1m6!3m5!1s0x30fdac8313243b4b:0xa448676654d5ebe2!2sMahaSamutr+Country+Club+%26+Luxury+Villas+Hua+Hin!8m2!3d12.523475!4d99.935882!3m4!1s0x30fdac8313243b4b:0xa448676654d5ebe2!8m2!3d12.523475!4d99.935882?hl=en">
            <div class="container-fluid">
                <div class="map-outer" ms-scroll-trigger>
                    <div class="map-inner" ms-google-map data-map='{"title":"Head Office", "marker":"/img/location/marker.png"}'>
                    </div>
                </div>
            </div>
        </a>
    </section>
@endsection

@section('script')
