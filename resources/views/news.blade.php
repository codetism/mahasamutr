@extends('layouts.main')

@section('title')
    News | MahaSamutr
@endsection

@section('header')

@section('content')
    <section class="section-hero">
        <div class="container-fluid">
            <div class="hero-block">
                <div class="hero-full">
                    <figure ms-parallax>
                        <picture>
                            <source srcset="/img/news/banner_w_2560.jpg" media="(min-width: 1920px)">
                            <source srcset="/img/news/banner_w_2053.jpg" media="(min-width: 1366px)">
                            <img src="/img/news/banner_w_1366.jpg" alt="">
                        </picture>
                    </figure>
                </div>
                <div class="scroll-down" b-scroll-to="#events-and-activities">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 34">
                        <path class="st0" d="M11,33L11,33C5.5,33,1,28.5,1,23V11C1,5.5,5.5,1,11,1h0c5.5,0,10,4.5,10,10v12C21,28.5,16.5,33,11,33z">
                        </path>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 ">
                        </polygon>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 ">
                        </polygon>
                    </svg>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="events-and-activities" class="events-and-activities text-center">
        <div class="container-fluid" ms-scroll-trigger>
            <div class="events-and-activities-wrapper">
                @foreach($news as  $key => $new)
                @if($key == 0)
                <div class="c-block c-block-events c-block-events-lg" data-modal-trigger="#news-{{$key}}-modal">
                    <div class="c-block-inner">
                        <figure>
                            <picture>
                                @if($new->gallery())
                                    @foreach($new->gallery as $key=>$image)
                                        @if($key == 0)
                                            <source srcset="{{ url($image->image) }}" media="(min-width: 1366px)">
                                            <img src="{{ url($image->image) }}" alt="">
                                        @endif
                                    @endforeach
                                @endif
                            </picture>
                            <figcaption class="c-block-content d-flex">
                                <div class="events-content align-self-center ml-auto mr-auto">
                                    <h4 class="events-date text-white">{{ date_format($new->published_at,"j M Y") }}</h4>
                                    <p class="events-desc text-white"><span class="d-block">{{ $new->excerpt() }}</span></p>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                @else
                <div class="c-block c-block-events" data-modal-trigger="#news-{{$key}}-modal">
                    <div class="c-block-inner">
                        <figure>
                            <picture>
                                @if($new->gallery())
                                    @foreach($new->gallery as $key=>$image)
                                        @if($key == 0)
                                            <source srcset="{{ url($image->image) }}" media="(min-width: 1366px)">
                                            <img src="{{ url($image->image) }}" alt="">
                                        @endif
                                    @endforeach
                                @endif
                            </picture>
                            <figcaption class="c-block-content d-flex">
                                <div class="events-content align-self-center ml-auto mr-auto">
                                    <h4 class="events-date text-white">{{ date_format($new->published_at,"j M Y") }}</h4>
                                    <p class="events-desc text-white"><span class="d-block">{{ $new->excerpt() }}</span></p>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                @endif

                @endforeach

            </div>
            <a href="" class="btn btn-events" style="display: none">
                <span>Read More</span>
            </a>
        </div>
    </section>
    <section id="construction-update" class="construction-update text-center">
        <div class="container-fluid">
            <div class="headline" ms-scroll-trigger>
                <h3 class="text-uppercase"><span>{!! Lang::get('messages.news_construction_title_s1') !!}</span>{!! Lang::get('messages.news_construction_title_s2') !!}</h3>
            </div>
        </div>
        <div class="construction-slider" b-slider data-slick='{"slidesToShow": 1, "centerMode": true, "centerPadding": "17%"}' ms-scroll-trigger>
            @foreach($constructions as  $construction)
                <div class="c-block ratio-construction">
                    <figure>
                        <picture>
                            <source srcset="{{ $construction->thumbnail }}" media="(min-width: 1366px)">
                            <img src="{{ $construction->thumbnail }}" alt="">
                        </picture>
                    </figure>
                    <div class="c-block-content">
                        <h6 class="construction-title text-uppercase"> {{ $construction->headline() }}</h6>
                        <div class="construction-list">{!! $construction->detail() !!}</div>
                    </div>
                </div>
            @endforeach

        </div>
    </section>
@endsection

@section('script')

@section('modal')

@foreach($news as  $key => $new)
<div id="news-{{$key}}-modal" class="modal news-modal" b-modal>
    <div class="modal-wrapper">
        <div class="modal-overlay"></div>
        <div class="modal-outer">
            <div class="modal-inner">
                <div class="modal-container">
                    <div class="row justify-content-lg-center">
                        <div class="col-lg-10">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="news-modal-items">
                                        <div class="news-modal-item news-modal-gallery">
                                            @if($new->gallery())
                                            <div id="events-slider-{{$key}}" class="events-slider" b-slider data-slick='{"asNavFor":"#events-thumb-slider-{{$key}}", "arrows":false, "fade":true, "autoplay":false, "autoplaySpeed":6000}'>
                                                @foreach($new->gallery as $image)
                                                <div class="c-block ratio-events-modal">
                                                    <figure>
                                                        <picture>
                                                            <source srcset="{{ url($image->image) }}" media="(min-width: 1366px)">
                                                            <img src="{{ url($image->image)}}" alt="">
                                                        </picture>
                                                    </figure>
                                                </div>
                                                @endforeach
                                            </div>
                                            <div id="events-thumb-slider-{{$key}}" class="events-thumb-slider" b-slider data-slick='{"asNavFor":"#events-slider-{{$key}}", "arrows":true, "slidesToShow":3}'>
                                                @foreach($new->gallery as $image)
                                                <div class="c-block ratio-events-modal">
                                                    <figure>
                                                        <picture>
                                                            <source srcset="{{ url($image->image) }}" media="(min-width: 1366px)">
                                                            <img src="{{ url($image->image)}}" alt="">
                                                        </picture>
                                                    </figure>
                                                </div>
                                                @endforeach
                                            </div>
                                            @endif
                                        </div>
                                        <div class="news-modal-item news-modal-content">
                                            <div class="scrollbar-wrapper">
                                                <div class="scrollbar-content">
                                                    <h6 class="text-center"><span class="d-md-block">{{ $new->headline() }}</span></h6>
                                                    <p class="date">{{ date_format($new->published_at,"j M Y") }}</p>
                                                    {!! $new->detail() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-nav">
        <button class="modal-arrow modal-prev"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="36.7px" height="72px" viewBox="16.9 -15.4 36.7 72" style="enable-background:new 16.9 -15.4 36.7 72;" xml:space="preserve"><polygon class="modal-arrow-item" points="52.9,56.6 16.9,20.6 52.9,-15.4 53.6,-14.7 18.3,20.6 53.6,55.9 "/></svg></button>
        <button class="modal-arrow modal-next"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="36.7px" height="72px" viewBox="16.9 -15.4 36.7 72" style="enable-background:new 16.9 -15.4 36.7 72;" xml:space="preserve"><polygon class="modal-arrow-item" points="17.6,-15.4 53.6,20.6 17.6,56.6 16.9,55.9 52.2,20.6 16.9,-14.7 "/></svg></button>
    </div>
    <div class="modal-close-bt" b-modal-close>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             width="512px" height="512px" viewBox="232 -232 512 512" style="enable-background:new 232 -232 512 512;" xml:space="preserve">
                <polygon points="607.8,-88.7 596.3,-99.8 488.2,12.1 380,-99.8 368.5,-88.7 477.1,23.7 368.5,136 380,147.2 488.2,35.2
                596.3,147.2 607.8,136 499.3,23.7"/>
            </svg>
    </div>
</div>

@endforeach
@endsection
