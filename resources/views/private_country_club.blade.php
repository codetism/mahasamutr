@extends('layouts.main')

@section('title')
Private Country Club | MahaSamutr
@endsection

@section('header')

@section('content')
<section class="section-hero private-country-club">
    <div class="container-fluid">
        <div class="hero-block">
            <div class="hero-full">
                <figure ms-parallax>
                    <picture>
                        <img src="/img/private-country-club/banner.jpg" alt="" style="padding-top:0">
                    </picture>
                </figure>
                <div class="hero-content wrapper-panel" ms-scroll-trigger>
                    <h1 class="text-white text-uppercase"><span class="txt-1 d-block">{!! Lang::get('messages.pcc_banner_l1') !!}</span><span class="txt-2 d-block">{!! Lang::get('messages.pcc_banner_l2') !!}</span></h1>
                </div>
            </div>
            <div class="scroll-down" b-scroll-to="#concept">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 34">
                    <path class="st0" d="M11,33L11,33C5.5,33,1,28.5,1,23V11C1,5.5,5.5,1,11,1h0c5.5,0,10,4.5,10,10v12C21,28.5,16.5,33,11,33z">
                    </path>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                    <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 ">
                    </polygon>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                    <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 ">
                    </polygon>
                </svg>
                <div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Concept -->
<section id="concept" class="concept">
    <div class="container-fluid">
        <div class="concept-blocks">
            <div class="c-block ratio-concept">
                <figure>
                    <picture>
                        <source srcset="/img/private-country-club/country-club_w_2672.jpg" media="(min-width: 1920px)">
                        <source srcset="/img/private-country-club/country-club_w_2085.jpg" media="(min-width: 1366px)">
                        <img src="/img/private-country-club/country-club_w_1366.jpg" alt="">
                    </picture>
                </figure>
            </div>

            <div class="c-block-content text-center" style="padding-right: 20px;">
                <div class="outer">
                    <div class="inner ms-animate" ms-scroll-trigger>
                        <h3 class="text-uppercase text-white">{!! Lang::get('messages.pcc_concept_title') !!}</h3>
                        <p class="text-white">
                            <span class="d-xl-block">{!! Lang::get('messages.pcc_concept_desc_p1_l1') !!}</span>
                            <span class="d-xl-block">{!! Lang::get('messages.pcc_concept_desc_p1_l2') !!}</span>
                            <span class="d-xl-block">{!! Lang::get('messages.pcc_concept_desc_p1_l3') !!}</span>
                        </p>
                        <p class="text-white">
                            <span class="d-xl-block">{!! Lang::get('messages.pcc_concept_desc_p2_l1') !!}</span>
                            <span class="d-xl-block">{!! Lang::get('messages.pcc_concept_desc_p2_l2') !!}</span>
                        </p>
                    </div>
                </div>
                <div class="private-country-club row-download">
                    <a href="{!! Lang::get('messages.pcc_concept_brochure') !!}">
                        <p class="p-download" >{!! Lang::get('messages.pcc_concept_download') !!}</p>
                        <img class="img-download" src="/img/luxury/download-icon.png" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Mahasamutr Lagoon< -->
<section id="mahasamutr-lagoon" class="mahasamutr-lagoon">
    <div class="container-fluid">
        <div class="c-block ratio-mahasamutr-lagoon">
            <figure ms-parallax-section>
                <img src="/img/private-country-club/mahasamutr-lagoon.jpg" alt="">
            </figure>
            <div class="c-block-content" ms-scroll-trigger>
                <h3 class="text-white text-uppercase">
                    <span class="txt-1">{!! Lang::get('messages.pcc_lagoon_title_l1') !!}</span>
                    <span class="txt-2">{!! Lang::get('messages.pcc_lagoon_title_l2') !!}</span>
                </h3>
                <p class="text-white">
                    <span class="d-lg-block">{!! Lang::get('messages.pcc_lagoon_desc_p1_l1') !!}</span>
                    <span class="d-lg-block">{!! Lang::get('messages.pcc_lagoon_desc_p1_l2') !!}</span>
                </p>
                <p class="text-white">
                    <span class="d-lg-block">{!! Lang::get('messages.pcc_lagoon_desc_p2_l1') !!}</span>
                    <span class="d-lg-block">{!! Lang::get('messages.pcc_lagoon_desc_p2_l2') !!}</span>
                    <span class="d-lg-block">{!! Lang::get('messages.pcc_lagoon_desc_p2_l3') !!}</span>
                    <span class="d-lg-block">{!! Lang::get('messages.pcc_lagoon_desc_p2_l4') !!}</span>
                </p>
            </div>
        </div>
        <div ms-parallax-section>
            <div ms-parallax-item>
                <div class="mahasamutr-lagoon-lists">
                    <div class="mahasamutr-lagoon-list">
                        <div class="c-block-content" ms-scroll-trigger>
                            <h4 class="text-uppercase">
                                <span class="txt-1">{!! Lang::get('messages.pcc_crystal_lagoon_title_l1') !!}</span>
                                <span class="txt-2">{!! Lang::get('messages.pcc_crystal_lagoon_title_l2') !!}</span>
                            </h4>
                            <p>{!! Lang::get('messages.pcc_crystal_lagoon_desc_p1') !!}</p>
                            <p>{!! Lang::get('messages.pcc_crystal_lagoon_desc_p2') !!}</p>
                            <p>{!! Lang::get('messages.pcc_crystal_lagoon_desc_p3') !!}</p>
                        </div>
                    </div>
                    <div class="mahasamutr-lagoon-list">
                        <div class="c-block-content" ms-scroll-trigger>
                            <h4 class="text-uppercase">
                                <span class="txt-1">{!! Lang::get('messages.pcc_water_features_title_l1') !!}</span>
                                <span class="txt-2">{!! Lang::get('messages.pcc_water_features_title_l2') !!}</span>
                            </h4>
                            <p class="text-white">{!! Lang::get('messages.pcc_water_features_desc_p1') !!}</p>
                            <p class="text-white">{!! Lang::get('messages.pcc_water_features_desc_p2') !!}</p>
                            <p class="text-white">{!! Lang::get('messages.pcc_water_features_desc_p3') !!}</p>
                        </div>
                    </div>
                    <div class="mahasamutr-lagoon-list">
                        <div class="c-block-content" ms-scroll-trigger>
                            <h4 class="text-uppercase">
                                <span class="txt-1">{!! Lang::get('messages.pcc_waterside_title_l1') !!}</span>
                                <span class="txt-2">{!! Lang::get('messages.pcc_waterside_title_l2') !!}</span>
                            </h4>
                            <p>{!! Lang::get('messages.pcc_waterside_desc_p1') !!}</p>
                            <p>{!! Lang::get('messages.pcc_waterside_desc_p2') !!}</p>
                            <p>{!! Lang::get('messages.pcc_waterside_desc_p3') !!}</p>
                            <p>{!! Lang::get('messages.pcc_waterside_desc_p4') !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Water sport -->
<section id="water-sport" class="water-sport">
    <div class="container-fluid">
        <div class="headline">
            <h3 class="text-uppercase" ms-scroll-trigger><span>{!! Lang::get('messages.pcc_sa_title_s1') !!}</span>{!! Lang::get('messages.pcc_sa_title_s2') !!}</h3>
        </div>
        <div class="c-block ratio-water-sport">
            <figure ms-parallax-section>
                <img src="/img/private-country-club/water-sport.jpg" alt="">
            </figure>
            <div class="c-block-content" ms-scroll-trigger>
                <h3 class="text-uppercase">{!! Lang::get('messages.pcc_water_sports_title') !!}</h3>
                <p>{!! Lang::get('messages.pcc_water_sports_desc_p1') !!}</p>
                <p>{!! Lang::get('messages.pcc_water_sports_desc_p2') !!}</p>
            </div>
        </div>
        <div ms-parallax-section>
            <div ms-parallax-item>
                <div id="water-sport-slider" class="water-sport-slider show-side-padding" b-slider data-slick='{"slidesToShow": 3, "autoplay":false, "infinite": false}' data-responsive='{"md":2, "sm":2, "xs":1}' ms-scroll-trigger>
                    <div class="c-block ratio-water-sport-slide">
                        <figure><img src="/img/private-country-club/water-sport01.jpg" alt="Paddle-Boarding">
                            <figcaption class="c-block-content text-white">
                                <div class="c-block-text">
                                    <h3 class="c-block-title text-uppercase">{!! Lang::get('messages.pcc_water_sports_paddle_boarding') !!}</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="c-block ratio-water-sport-slide">
                        <figure><img src="/img/private-country-club/water-sport02.jpg" alt="Windsurfing">
                            <figcaption class="c-block-content text-white">
                                <div class="c-block-text">
                                    <h3 class="c-block-title text-uppercase">{!! Lang::get('messages.pcc_water_sports_windsurfing') !!}</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="c-block ratio-water-sport-slide">
                        <figure><img src="/img/private-country-club/water-sport03.jpg" alt="KAYAKING">
                            <figcaption class="c-block-content">
                                <div class="c-block-text">
                                    <h3 class="c-block-title text-uppercase">{!! Lang::get('messages.pcc_water_sports_kayaking') !!}</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="c-block ratio-water-sport-slide">
                        <figure><img src="/img/private-country-club/water-sport04.jpg" alt="Swimming">
                            <figcaption class="c-block-content text-white">
                                <div class="c-block-text">
                                    <h3 class="c-block-title text-uppercase">{!! Lang::get('messages.pcc_water_sports_swimming') !!}</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="c-block ratio-water-sport-slide">
                        <figure><img src="/img/private-country-club/water-sport05.jpg" alt="Sailing">
                            <figcaption class="c-block-content text-white">
                                <div class="c-block-text">
                                    <h3 class="c-block-title text-uppercase">{!! Lang::get('messages.pcc_water_sports_sailing') !!}</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Other sport -->
<section id="other-sport" class="other-sport">
    <div class="container" ms-scroll-trigger>
        <div class="headline">
            <div class="row">
                <div class="col-lg-4 offset-lg-1 col-xl-3 offset-xl-2">
                    <h3 class="text-uppercase">
                        <span class="txt-1">{!! Lang::get('messages.pcc_other_sports_title_l1') !!}</span>
                        <span class="txt-2 text-nowrap">{!! Lang::get('messages.pcc_other_sports_title_l2') !!}</span>
                    </h3>
                </div>
                <div class="col-lg-6 col-xl-5">
                    <p><span class="d-md-block">{!! Lang::get('messages.pcc_other_sports_desc_l1') !!}</span> <span class="d-md-block">{!! Lang::get('messages.pcc_other_sports_desc_l2') !!}</span></p>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-lg-3 offset-lg-5">
                    <div class="other-sport-list">
                        <h6>{!! Lang::get('messages.pcc_other_sports_desc_indoor_subsection') !!}</h6>
                        <ul>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_indoor_list_aerobics') !!}</li>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_indoor_list_badminton') !!}</li>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_indoor_list_basketball') !!}</li>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_indoor_list_cardio_vascular_training') !!}</li>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_indoor_list_fitness_center') !!}</li>
                        </ul>
                        <ul>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_indoor_list_group_training') !!}</li>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_indoor_list_pilates_yoga') !!}</li>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_indoor_list_squash') !!}</li>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_indoor_list_volleyball') !!}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-6 col-lg-3">
                    <div class="other-sport-list">
                        <h6>{!! Lang::get('messages.pcc_other_sports_desc_outdoor_subsection') !!}</h6>
                        <ul>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_outdoor_list_jogging_path') !!}</li>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_outdoor_list_rugby') !!}</li>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_outdoor_list_football') !!}</li>
                            <li>{!! Lang::get('messages.pcc_other_sports_desc_outdoor_list_tennis') !!}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div id="other-sport-slider" class="other-sport-slider show-side-padding" b-slider data-slick='{"slidesToShow": 1, "autoplay":true, "infinite": true, "arrows": false, "dots": true}' ms-scroll-trigger>
            <div class="c-block ratio-other-sport-slide">
                <figure>
                    <img src="/img/private-country-club/other-sport01.jpg" alt="">
                </figure>
            </div>
            <div class="c-block ratio-other-sport-slide">
                <figure>
                    <img src="/img/private-country-club/other-sport02.jpg" alt="">
                </figure>
            </div>
            <div class="c-block ratio-other-sport-slide">
                <figure>
                    <img src="/img/private-country-club/other-sport03.jpg" alt="">
                </figure>
            </div>
            <div class="c-block ratio-other-sport-slide">
                <figure>
                    <img src="/img/private-country-club/other-sport04.jpg" alt="">
                </figure>
            </div>
        </div>
    </div>
</section>
<!-- Club Amenities -->
<section id="club-amenities" class="club-amenities">
    <div ms-parallax-section>
        <div ms-parallax-item>
            <div class="container-fluid">

                <div class="club-amenities-blocks">
                    <div class="c-block ratio-club-amenities">
                        <figure>
                            <img src="/img/private-country-club/club-amenities.jpg" alt="">
                        </figure>
                    </div>
                    <div class="c-block-content">
                        <div class="outer">
                            <div class="inner ms-animate" ms-scroll-trigger="">
                                <h3 class="text-white text-uppercase"><span class="txt-1">{!! Lang::get('messages.pcc_club_amenities_services_title_l1') !!}</span><span class="txt-2">{!! Lang::get('messages.pcc_club_amenities_services_title_l2') !!}</span></h3>
                                <p class="text-white">{!! Lang::get('messages.pcc_club_amenities_services_desc_p1') !!}</p>
                                <p class="text-white">{!! Lang::get('messages.pcc_club_amenities_services_desc_p2') !!}</p>
                                <p class="text-white">{!! Lang::get('messages.pcc_club_amenities_services_desc_p3') !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Memberships & Benefits -->
<section id="memberships-and-benefits" class="memberships-and-benefits" style="margin-bottom: 60px;">
    <div class="container">
        <div class="headline" ms-scroll-trigger>
            <h3 class="text-uppercase text-center"><span>{!! Lang::get('messages.pcc_membership_benefits_title_s1') !!}</span> {!! Lang::get('messages.pcc_membership_benefits_title_s2') !!}</h3>
            <div class="row">
                <div class="col-lg-7 push-lg-3 offset-lg-1 col-xl-6 push-xl-3 offset-xl-2">
                    <p>{!! Lang::get('messages.pcc_membership_benefits_desc_p1') !!}</p>
                    <p>{!! Lang::get('messages.pcc_membership_benefits_desc_p2') !!}</p>
                </div>
                <div class="col-lg-3 pull-lg-7 col-xl-3 pull-xl-6">
                    <div class="card">
                        <img src="/img/private-country-club/card.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="type-card">
            <div class="row">
                <div class="col-lg-4">
                    <div class="c-block ratio-memberships individual">
                        <figure>
                            <img src="/img/private-country-club/individual.jpg" alt="">
                        </figure>
                        <div class="c-block-content" ms-scroll-trigger>
                            <h5 class="text-white">{!! Lang::get('messages.pcc_individual_title') !!}</h5>
                            <p class="text-white"><em>{!! Lang::get('messages.pcc_individual_desc_p1') !!}</em></p>
                            <p class="text-white">{!! Lang::get('messages.pcc_individual_desc_p2') !!}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="c-block ratio-memberships family">
                        <figure>
                            <img src="/img/private-country-club/family.jpg" alt="">
                        </figure>
                        <div class="c-block-content" ms-scroll-trigger>
                            <h5 class="text-white">{!! Lang::get('messages.pcc_family_title') !!}</h5>
                            <p class="text-white"><em>{!! Lang::get('messages.pcc_family_desc_p1') !!}</em></p>
                            <p class="text-white">{!! Lang::get('messages.pcc_family_desc_p2') !!}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="c-block ratio-memberships corporate">
                        <figure>
                            <img src="/img/private-country-club/corporate.jpg" alt="">
                        </figure>
                        <div class="c-block-content" ms-scroll-trigger>
                            <h5 class="text-white">{!! Lang::get('messages.pcc_corporate_title') !!}</h5>
                            <p class="text-white"><em>{!! Lang::get('messages.pcc_corporate_desc_p1') !!}</em></p>
                            <p class="text-white">{!! Lang::get('messages.pcc_corporate_desc_p2') !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- FAQs -->
<section id="faqs" class="faqs" style="display: none">
    <div class="container" ms-scroll-trigger>
        <div class="headline">
            <h2>FAQs</h2>
        </div>
        <div class="faq-container">
            <ul ms-accordion>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>Is MahaSamutr Country Club a private country club?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>Yes, we are a closed Country Club and therefore we only welcome members who are registered and enrolled with us.  It is possible to bring along guests when accompanied by members to enjoy food & beverage outlets, spa, etc. Terms and conditions apply.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>How does MahaSamutr Country Club differ from other clubs?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>We are situated within MahaSamutr Hua Hin, and for the exclusive use of residents and members. The Country Club features a variety of benefits for members consisting of personalized classes, event calendar activities, entertainment options, wide array of dining and lifestyle amenities, together with personal service.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>What is operating hours for MahaSamutr Country Club?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>MahaSamutr Country Club opens daily.  Currently it is not yet open. During the construction period, our sales offices will be open Monday to Sunday from 10.00 – 18.00 hrs. Please call +66 32 907 900 for more information.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>What features does MahaSamutr Country Club offer to members?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>The Country Club features outstanding facilities in a safe, clean, and controlled environment including a wide-array of sporting activities supervised by professionals, and recreational calendar of activities all year round. The club also features social and entertainment components such as Meeting & Corporate Services, Children’s Zone, Sport Field Café, Member’s Lounge, Sports Diner & Bar, All Day Restaurant etc.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>Who is able to use the facilities at MahaSamutr Country Club?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>Members of MahaSamutr Country Club and the Villa owners of MahaSamutr Villas can access the facilities subject to the Club’s by-laws.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>What is the total size of Property Area?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>MahaSamutr Country Club is approximately 120,000 sq.m (75 Rai).</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>Who is the developer of MahaSamutr Country Club?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>PACE Country Club Co., Ltd.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>Who is managing the Country Club?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>MahaSamutr Country Club will be managed by PACE Country Club Company Limited which is subsidiary of PACE Development Corporation Plc. with the support from International Leisure Consultants (ILC).</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>What types of food & beverage outlets are provided within the club?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>There are several outlets offering dining service; Mediterranean cuisine and International all-day dining. There is also a Lounge & Bar , a Sport Café next to the Sports field and All Day Restaurant. Members will also enjoy the other 3 Food & Beverage service stations: Lagoon-Side Service, Room Service, Banquet & Event Service.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>Will there be any sports equipment such as tennis rackets or windsurfs available for usage or rental?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>Yes, the club will offer service for providing sports equipment to members. Equipment will be provided as complimentary or rental for specific items. Terms and conditions apply.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>Do members need to pay additional cost for personal training classes or other facility equipment?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>Yes, there will be some additional charges when using these services, but the rates will be special with promotions from time to time.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>How large is the MahaSamutr Lagoon?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>The lagoon is approximately 72,000 sq.m (45 Rai) in size and is Asia’s largest private man-made clear water lagoon.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>Is the lagoon fresh or salt water?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>The lagoon water is fresh. Members can swim in and play water sports all year round in a safe and clean environment. The water is legally sourced from the natural rain and local tap water.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>How is the MahaSamutr Lagoon special?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>MahaSamutr Lagoon is a large man-made lagoon that provides crystal clear water using Crystal Lagoons patented technology incorporated in more than 600 projects in different stages of development and negotiation in 60 countries worldwide creating water features of unimagined size. MahaSamutr Lagoon is Asia’s largest Crystal Lagoons.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>How does MahaSamutr and Crystal Lagoons treat the water in the lagoon?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>MahaSamutr uses Crystal Lagoons’ cutting-edge water monitoring system, controlled by Crystal Lagoons headquarter. Crystal Lagoons’ water management system applies eco-friendly and sustainable technology which uses up to 100 times less chemical additives than conventional swimming pool systems and as low as 2% of the energy required for traditional filtration systems.  This results in crystal clear swimming water which is perfect for all watersports activities.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>What is the tenure of membership?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>MahaSamutr Country Club offers Lifetime Membership</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>Can I sell or transfer my MahaSamutr membership to someone else?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>Yes, the membership is transferable after 5 years of the Country Club’s full operations.  Terms and Conditions apply.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>Does a Membership include privileges for spouse and children?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>The family membership includes one spouse and children who are below the age of 21. Once they reach the age of 21, a family plus category is an option for the children.</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>What should I do if I want to join MahaSamutr Country Club as member?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>Simply leave your contact information on our online form [here] and our sales representative will contact you back shortly. You can also call +66 32 907 900 or email info@mahasamutr.com</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>Who should I contact for a site visit? </span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>Site visits are by appointment only. You can reach our sales representative in Bangkok and Hua Hin by calling 032 907 900 or info@mahasamutr.com</p>
                        </div>
                    </div>
                </li>
                <li class="accordion-list">
                    <a class="accordion-title" href="#" ms-accordion-bt><span>Where is MahaSamutr Country Club located?</span></a>
                    <div class="accordion-content" ms-accordion-node>
                        <div class="accordion-content-inner">
                            <p>MahaSamutr Country Club is located on Soi Hua Hin 112. If you are traveling From Bangkok, you can use Phetkasem Road or use the Bypass Road. </p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>
@endsection

@section('script')
