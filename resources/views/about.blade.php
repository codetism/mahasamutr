@extends('layouts.main')

@section('title')
    About | MahaSamutr
@endsection

@section('header')

@section('content')
    <section class="section-hero">
        <div class="container-fluid">
            <div class="hero-block">
                <div class="hero-full">
                    <figure ms-parallax>
                        <picture>
                            <source srcset="/img/about/banner_w_2672.jpg" media="(min-width: 1920px)">
                            <source srcset="/img/about/banner_w_2261.jpg" media="(min-width: 1366px)">
                            <img src="/img/about/banner_w_1366.jpg" alt="">
                        </picture>
                    </figure>
                </div>
                <div class="scroll-down" b-scroll-to="#about-mahasamutr">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 34">
                        <path class="st0" d="M11,33L11,33C5.5,33,1,28.5,1,23V11C1,5.5,5.5,1,11,1h0c5.5,0,10,4.5,10,10v12C21,28.5,16.5,33,11,33z"></path>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                    </svg>
                    <div></div>
                </div>
            </div>
        </div>
    </section>
    <section id="about-mahasamutr" class="about-mahasamutr">
        <div class="about-mahasamutr-inner" style="height: auto">
            <div class="section-full">
                <figure>
                    <picture>
                        <source srcset="/img/about/about-mahasamutr_w_2732.jpg" media="(min-width: 1920px)">
                        <source srcset="/img/about/about-mahasamutr_w_2119.jpg" media="(min-width: 1366px)">
                        <img src="/img/about/about-mahasamutr_w_1366.jpg" alt="">
                    </picture>
                </figure>
            </div>
            <div class="section-wrapper">
                <div class="container-fluid">
                    <div class="about-mahasamutr-content text-center" ms-scroll-trigger>
                        <div class="about-mahasamutr-title">
                            <h3 class="text-white text-uppercase text-left d-inline-block"><span class="d-block">{!! Lang::get('messages.about_main_title_l1') !!}</span>
                                {!! Lang::get('messages.about_main_title_l2') !!}</h3>
                        </div>
                        <p class="text-white">
                            <span class="d-lg-block">{!! Lang::get('messages.about_main_desc_p1_l1') !!}</span>
                            <span class="d-lg-block">{!! Lang::get('messages.about_main_desc_p1_l2') !!}</span><br>
                            <span class="d-lg-block">{!! Lang::get('messages.about_main_desc_p2_l1') !!}</span>
                            <span class="d-lg-block">{!! Lang::get('messages.about_main_desc_p2_l2') !!}</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="own-a-luxury">
        <div class="container">
            <div class="luxury-container">
                <div class="row">
                    <div class="col-lg-6 d-flex align-items-center">
                        <div class="luxury-block" ms-scroll-trigger>
                            <h3 class="luxury-title text-uppercase"><span class="txt-1">{!! Lang::get('messages.about_own_title_l1') !!}</span><span class="txt-2">{!! Lang::get('messages.about_own_title_l2') !!}</span></h3>
                            <p>{!! Lang::get('messages.about_own_desc_p1') !!}</p>
                            <p>{!! Lang::get('messages.about_own_desc_p2') !!}</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="c-block ratio-luxury">
                            <figure>
                                <img src="/img/about/luxury-1.jpg">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="luxury-container">
                <div class="row">
                    <div class="col-lg-6 push-lg-6 d-flex align-items-center">
                        <div class="luxury-block" ms-scroll-trigger>
                            <div class="luxury-logo"><img src="/img/bg/screen-logo.png"> </div>
                            <p>{!! Lang::get('messages.about_own_desc_p3') !!}</p>
                            <p>{!! Lang::get('messages.about_own_desc_p4') !!}</p>
                        </div>
                    </div>
                    <div class="col-lg-6 pull-lg-6">
                        <div class="c-block ratio-luxury">
                            <figure>
                                <img src="/img/about/luxury-2.jpg">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
