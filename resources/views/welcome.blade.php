@extends('layouts.main')

@section('title')
	MahaSamutr
@endsection

@section('header')

@section('content')
	<section class="section-hero">
		<div class="container-fluid">
			<div class="hero-block desktop" ms-parallax>
				<div class="hero-full">
					<div class="hero-video">
						<div class="video">
							<video id="index-vdo" autoplay loop muted preload="none" poster="/video/mainhero.jpg">
								<source src="/video/MHSM-FOR-WEB-2017-03-09.mp4" type="video/mp4">
							</video>
						</div>
					</div>
				</div>
				<div class="hero-content-home wrapper-panel" ms-scroll-trigger>
					<h1 id="index-caption" class="text-white text-uppercase"><span class="txt-1 d-block">{!! Lang::get('messages.home_banner_l1') !!}</span><span class="txt-2 d-block">{!! Lang::get('messages.home_banner_l2') !!}</span><span class="txt-3 d-block">{!! Lang::get('messages.home_banner_l3') !!}</span></h1>
				</div>
				<div class="scroll-down" b-scroll-to="#section-about">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 34">
						<path class="st0" d="M11,33L11,33C5.5,33,1,28.5,1,23V11C1,5.5,5.5,1,11,1h0c5.5,0,10,4.5,10,10v12C21,28.5,16.5,33,11,33z"></path>
					</svg>
					<svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
						<polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
					</svg>
					<svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
						<polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
					</svg>
					<div></div>
				</div>
			</div>
			<div class="hero-block mobile">
				<div class="hero-full">
					<figure ms-parallax>
						<picture>
							<img src="/img/home/banner.jpg" alt="">
						</picture>
					</figure>
				</div>
				<div class="scroll-down" b-scroll-to="#section-about">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 34">
						<path class="st0" d="M11,33L11,33C5.5,33,1,28.5,1,23V11C1,5.5,5.5,1,11,1h0c5.5,0,10,4.5,10,10v12C21,28.5,16.5,33,11,33z"></path>
					</svg>
					<svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
						<polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
					</svg>
					<svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
						<polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
					</svg>
					<div></div>
				</div>
			</div>
		</div>
	</section>
	<section id="section-about" class="section-about">
		<div class="section-about-inner" style="height: auto">
			<div class="section-full">
				<figure>
					<img src="/img/home/about.png">
				</figure>
			</div>
			<?php
				$f_source='http://api.openweathermap.org/data/2.5/weather?id=1153269&units=metric&appid='.env("OPEN_WEATHER_API");
				$json_f = file_get_contents($f_source);
				$f_get_list = json_decode($json_f);
			?>
			<div class="section-wrapper">
				<div class="about-content section-block text-center" ms-scroll-trigger>
					<div class="container-fluid">
						<h3 class="text-white text-uppercase">{!! Lang::get('messages.home_about_title') !!}</h3>
						<p class="text-white"><span class="d-md-block">{!! Lang::get('messages.home_about_desc_p1_l1') !!}</span> <span class="d-md-block">{!! Lang::get('messages.home_about_desc_p1_l2') !!}</span> <span class="d-md-block">{!! Lang::get('messages.home_about_desc_p1_l3') !!}</span>
						</p>
						<p class="text-white"><span class="d-md-block">{!! Lang::get('messages.home_about_desc_p2_l1') !!}</span> <span class="d-md-block">{!! Lang::get('messages.home_about_desc_p2_l2') !!}</span> <span class="d-md-block">{!! Lang::get('messages.home_about_desc_p2_l3') !!}</span>
						</p>
						<a href="/about" class="btn text-white"><span>{!! Lang::get('messages.button_read_more') !!}</span></a>
					</div>
				</div>
				<div class="weather" ms-scroll-trigger>
					<div class="weather-item weather-top">
						<div class="weather-item-inner">
							<span id="current-time"></span>
							<hr>
							<p>{{ $f_get_list->weather[0]->description }}</p>
						</div>
					</div>
					<div class="weather-item weather-bottom">
						<div class="weather-item-inner">
							<span>{{ ceil($f_get_list->main->temp) }}°C</span>
							<p>MahaSamutr</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-location">
		<div class="container-fluid">
			<div class="c-block ratio-location" ms-scroll-trigger>
                <div ms-parallax-section>
                    <div ms-parallax-item>
                        <figure >
                            <picture>
                                <source srcset="/img/home/location_w_2672.jpg" media="(min-width: 1920px)">
                                <source srcset="/img/home/location_w_2220.jpg" media="(min-width: 1366px)">
                                <img src="/img/home/location_w_1366.jpg" alt="">
                            </picture>
                        </figure>
                    </div>
                </div>
				<div class="c-block-content">
					<h3 class="text-uppercase text-white"><span class="txt-1 d-block">{!! Lang::get('messages.home_location_title_l1') !!}</span><span class="txt-2 d-block">{!! Lang::get('messages.home_location_title_l2') !!}</span></h3>
					<p class="text-white"><span class="d-sm-block">{!! Lang::get('messages.home_location_desc_l1') !!}</span>
						<span class="d-sm-block">{!! Lang::get('messages.home_location_desc_l2') !!}</span>
						<span class="d-sm-block">{!! Lang::get('messages.home_location_desc_l3') !!}</span>
					</p>
					<a href="/location" class="btn"><span>{!! Lang::get('messages.button_read_more') !!}</span></a>
				</div>
			</div>
		</div>
	</section>
	<section class="section-country-club">
		<div class="section-block text-center" ms-scroll-trigger>
			<div class="container-fluid">
				<h3 class="text-uppercase">{!! Lang::get('messages.home_pcc_title') !!}</h3>
				<p>
					<span class="d-md-block">{!! Lang::get('messages.home_pcc_desc_l1') !!}</span>
					<span class="d-md-block">{!! Lang::get('messages.home_pcc_desc_l2') !!}</span>
					<span class="d-md-block">{!! Lang::get('messages.home_pcc_desc_l3') !!}</span>
				</p>
				<a href="/private-country-club" class="btn"><span>{!! Lang::get('messages.button_read_more') !!}</span></a>
			</div>
		</div>
		<div class="container-fluid">
			<div class="c-block ratio-country-club">
				<figure>
					<picture>
						<source srcset="/img/home/country-club_w_2672.jpg" media="(min-width: 1920px)">
						<source srcset="/img/home/country-club_w_2085.jpg" media="(min-width: 1366px)">
						<img src="/img/home/country-club_w_1366.jpg" alt="">
					</picture>
				</figure>
				<div class="c-block-content">
					<h1 class="text-uppercase text-white" ms-scroll-trigger>
						<span class="txt-1">{!! Lang::get('messages.home_gallery_l1') !!}</span>
						<span class="txt-2 d-block">{!! Lang::get('messages.home_gallery_l2') !!}</span>
					</h1>
				</div>
				<a href="/gallery#country-club">
					<div class="link-gallery">
						<p>
							<span>{!! Lang::get('messages.button_gallery') !!}</span>
						</p>
					</div>
				</a>
			</div>
			<div class="row no-gutters">
				<div class="col-lg-7">
					<div class="c-block ratio-villa">
						<figure>
							<picture>
								<source srcset="/img/home/villa-1_w_1550.jpg" media="(min-width: 1024px)">
								<img src="/img/home/villa-1_w_1024.jpg" alt="">
							</picture>
						</figure>
						<div class="c-block-content" ms-scroll-trigger id="luxury-text">
							<h3 class="text-white text-uppercase">{!! Lang::get('messages.home_lv_title') !!}</h3>
							<p class="text-white">
                                <span class="d-sm-block">{!! Lang::get('messages.home_lv_desc_l1') !!}</span>
								<span class="d-sm-block">{!! Lang::get('messages.home_lv_desc_l2') !!}</span>
								<span class="d-sm-block">{!! Lang::get('messages.home_lv_desc_l3') !!}</span>
							</p>
							<a href="/luxury-villas" class="btn text-white"><span>{!! Lang::get('messages.button_read_more') !!}</span></a>
						</div>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="villa-block">
						<div class="c-block ratio-villa-sm">
							<figure>
								<picture>
									<source srcset="/img/home/villa-2_w_1090.jpg" media="(min-width: 768px)">
									<img src="/img/home/villa-2_w_768.jpg" alt="">
								</picture>
							</figure>
						</div>
						<div class="c-block ratio-villa-sm">
							<figure>
								<picture>
									<source srcset="/img/home/villa-3_w_1090.jpg" media="(min-width: 768px)">
									<img src="/img/home/villa-3_w_768.jpg" alt="">
								</picture>
							</figure>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-news section-block">
		<div class="container-fluid">
			<div class="news-headline text-center" ms-scroll-trigger>
				<h3 class="text-uppercase">{!! Lang::get('messages.home_news_title') !!}</h3>
			</div>
		</div>
		<div class="news-block">
			<div class="row no-gutters">
				<div class="col-md-6 col-xl-4">
					<a href="news#events-and-activities">
						<div class="events-block">
							<div class="c-block c-block-hide-content ratio-news">
								<figure>
									<picture>
										<source srcset="{{ url($featured_news->gallery()->first()->image) }}" media="(min-width: 1024px)">
										<img src="{{ url($featured_news->gallery()->first()->image) }}" alt="">
									</picture>
								</figure>
								<figcaption class="c-block-content">
									<div class="c-block-text">
										<h3 class="c-block-title text-uppercase"><span class="txt-1">{!! Lang::get('messages.home_events_l1') !!}</span><span class="txt-2">{!! Lang::get('messages.home_events_l2') !!}</span></h3>
									</div>
								</figcaption>
							</div>
							<div class="c-block news-block-content">
								<div class="c-block-content" ms-scroll-trigger>
									<div class="c-block-text">
										<h3 class="c-block-title text-uppercase"><span class="txt-1">{!! Lang::get('messages.home_events_l1') !!}</span><span class="txt-2">{!! Lang::get('messages.home_events_l2') !!}</span></h3>
									</div>
									<h6 class="text-uppercase">{{ $featured_news->getTheDate() }}</h6>
									<p>
										<span class="d-lg-block">{{ $featured_news->excerpt() }}</span>
									</p>
								</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-6 col-xl-4 offset-xl-4">
					<a href="news#construction-update">
						<div class="construction-block">
							<div class="c-block c-block-hide-content ratio-news">
								<figure>
									<picture>
										<source srcset="{{ url($featured_update->thumbnail) }}" media="(min-width: 1024px)">
										<img src="{{ url($featured_update->thumbnail) }}" alt="">
									</picture>
								</figure>
								<figcaption class="c-block-content">
									<div class="c-block-text">
										<h3 class="c-block-title text-uppercase"><span class="txt-1">{!! Lang::get('messages.home_construction_l1') !!}</span><span class="txt-2">{!! Lang::get('messages.home_construction_l2') !!}</span></h3>
									</div>
								</figcaption>
							</div>
							<div class="c-block news-block-content" >
								<div class="c-block-content" ms-scroll-trigger>
									<div class="c-block-text">
										<h3 class="c-block-title text-uppercase"><span class="txt-1">{!! Lang::get('messages.home_construction_l1') !!}</span><span class="txt-2">{!! Lang::get('messages.home_construction_l2') !!}</span></h3>
									</div>
									<h6 class="text-white text-uppercase">{{ $featured_update->getTheDate() }}</h6>
									<p class="text-white">
										<span class="d-lg-block">{{ $featured_update->excerpt() }}</span>
									</p>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="about">
	</section>
@endsection

@section('script')
	<script>
		var myVar = setInterval(myTimer ,1000);
		function myTimer() {
			var d = new Date();
			var h = d.getUTCHours() + 7;
			h = (h <= 12)? h : h-12;
			var m = d.getUTCMinutes();
			var t = (d.getUTCHours() + 7 < 12)? 'AM' : 'PM';
			document.getElementById("current-time").innerHTML =  h + ':' + m + ' ' + t;
		}
	</script>
@endsection
