@extends('layouts.cms')

@section('title', 'Construction Update')

@section('header')
<meta name="cms-page-id" content="cms-page-construction_update"/>


@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        Construction Update
        <a class="btn btn-action-xs btn-info btn-xs pull-right" href="{{ action('Admin\ConstructionUpdateController@create') }}">New</a>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered dataTable">
            <thead>
            <tr>
                <th>ID</th>
                <th>Headline TH</th>
                <th>Headline EN</th>
                <th>Created Date</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($construction as $construction_item)
            <tr>
                <td>{!! $construction_item->id !!}</td>
                <td>{!! $construction_item->headline_thai !!}</td>
                <td>{!! $construction_item->headline_english !!}</td>
                <td>@if($construction_item->created_at){!! $construction_item->created_at->format('j F Y h:i:s A') !!}@endif</td>
                <td>@if($construction_item->is_published)
                    <a class="btn btn-action-xs btn-success btn-xs unpublishItem" id="{!! $construction_item->id !!}">Published</a>
                    @else
                    <a class="btn btn-action-xs btn-warning btn-xs publishItem" id="{!! $construction_item->id !!}">Draft</a>
                    @endif
                </td>
                <td>
                    <a class="btn btn-action-xs btn-primary  btn-xs" href="{{ action('Admin\ConstructionUpdateController@edit' , $construction_item->id) }}">Edit</a>
                    <a class="btn btn-action-xs btn-danger btn-xs deleteItem" id="{!! $construction_item->id !!}" name="{!! $construction_item->headline_thai !!}">Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

{!! Form::open(['method' => 'DELETE' , 'url' =>'#', 'class' =>'form-horizontal' , 'id' => 'DeleteForm']); !!}
{!! Form::close() !!}

{!! Form::open(['method' => 'POST' , 'url' => '#' , 'id' => 'PublishedForm']); !!}
{!! Form::Hidden('id', 0,  ['id' => 'service_id']) !!}
{!! Form::Hidden('is_published', 0 , ['id' => 'is_published']) !!}
{!! Form::close() !!}
@endsection

@section('script')
<script>
    $('div.alert').not('.alert-important').not('alert-danger').delay(3000).slideUp(1000);

    $('.deleteItem').click(function () {
        if (confirm('Are you sure you want to delete "' + $(this).attr("name") + '" ?')) {
            $('#DeleteForm').attr('action', "{{ action('Admin\ConstructionUpdateController@index') }}" + "/" + $(this).attr("id"));
            $('#DeleteForm').submit();
        }
    });

    $('.publishItem').click(function () {

        $('#PublishedForm').attr('action', "{{ action('Admin\ConstructionUpdateController@publish') }}");
        $('#is_published').val(1);
        $('#service_id').val($(this).attr("id"));
        $('#PublishedForm').submit();
    });

    $('.unpublishItem').click(function () {
        $('#PublishedForm').attr('action', "{{ action('Admin\ConstructionUpdateController@publish') }}");
        $('#is_published').val(0);
        $('#service_id').val($(this).attr("id"));
        $('#PublishedForm').submit();
    });

    $(document).ready(function () {
        $('.dataTable').dataTable({
            order: [
                [0, "desc"]
            ]
        });
    });
</script>
@endsection
