@extends('layouts.cms')
@section('title', 'News Detail')

@section('header')
<script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
<meta name="cms-page-id" content="cms-page-news"/>
@endsection

@section('content')
@include('admin.partials.flash')
@if($construction->id)
{!! Form::open(['method' => 'PATCH' , 'url' => action('Admin\ConstructionUpdateController@update' , $construction->id) , 'role'=>'search', 'files' => true]) !!}
@else
{!! Form::open(['url' => action('Admin\ConstructionUpdateController@store') , 'role'=>'search','files'=> true]) !!}
@endif

<div class="panel panel-default">
    <div class="panel-heading">News</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::Label('headline_thai', 'Headline (TH)' , ['class' => 'control-label']) !!}
                    {!! Form::Text('headline_thai', $construction->headline_thai , ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::Label('headline_english','Headline (EN)' , ['class' => 'control-label']) !!}
                    {!! Form::Text('headline_english', $construction->headline_english , ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="form-group">
                    {!! Form::Label('is_published', ' Published' ) !!}
                    {!! Form::checkbox('is_published','1', $construction->is_published, ['id' => 'is_published']) !!}
                    @if($construction->published_at)<div class="form-label">First Published Date: {!! $construction->published_at->format('j F Y h:i:s A') !!} </div>@endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::Label('detail_thai', 'Detail (TH)' ,['class' => 'control-label']) !!}
                    {!! Form::textarea('detail_thai', $construction->detail_thai , ['class' => 'ckeditor' , 'id' => 'detail_thai']) !!}
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::Label('detail_english','Detail (EN)' ,['class' => 'control-label']) !!}
                    {!! Form::textarea('detail_english', $construction->detail_english , ['class' => 'ckeditor' , 'id' => 'detail_english']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                {!! Form::label('excerpt_thai', 'Excerpt (TH)', ['class' => 'control-label']) !!}
                {!! Form::text('excerpt_thai', $construction->excerpt_thai, ['class' => 'form-control', 'placeholder' => 'Excerpt (TH)']) !!}
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('excerpt_english', 'Excerpt (EN)', ['class' => 'control-label']) !!}
                {!! Form::text('excerpt_english', $construction->excerpt_english, ['class' => 'form-control', 'placeholder' => 'Excerpt (EN)']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::label('display_th', 'Display TH', ['class' => 'control-label']) !!}
                    {!! Form::checkbox('display_th', '1', $construction->display_th) !!}
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::label('display_en', 'Display EN', ['class' => 'control-label']) !!}
                    {!! Form::checkbox('display_en', '1', $construction->display_en) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::Label('thumbnail', 'Thumbnail Image (1766 x 976 px)' , ['class' => 'control-label']) !!}
                    {!! Form::file('thumbnail' , ['accept'=> 'image/*' ]) !!}
                    @if($construction->thumbnail)
                        <br/>
                        <img src="{!! url($construction->thumbnail) !!}" class="img-small-display" style="width: 800px;">
                        <br/>{!! $construction->thumbnail !!}
                    @endif
                </div>
            </div>
        </div>

    </div>
    <div class="panel-footer clearfix">
        @if($construction->id)
        <div class="pull-left">
            {!! Form::Button('Delete', ['class' => 'btn btn-action btn-danger deleteItem' , 'id' => $construction->id]) !!}
        </div>
        @endif
        <div class="pull-right">
            {!! Form::Button('Cancel', ['class' => 'btn btn-action btn-default', 'id' => 'viewList' ]) !!}
            {!! Form::Submit('Save', ['class' => 'btn btn-action btn-info' , 'name' => 'save']) !!}
            {!! Form::Submit('Save and New ', ['class' => 'btn btn-action btn-success' , 'name' =>'saveAndNew'])!!}
            {!! Form::Submit('Save and Finish', ['class' => 'btn btn-action btn-primary' , 'name' =>'saveAndClose']) !!}
        </div>
    </div>
</div>
{!! Form::close() !!}

{!! Form::open(['method' => 'DELETE' , 'url' => action('Admin\ConstructionUpdateController@destroy', [$construction->id]) , 'id' => 'DeleteForm']) !!}
{!! Form::close() !!}

<div class="space50"></div>
@endsection


@section('script')
<script>

    CKEDITOR.replace('detail_english',
        {
            filebrowserBrowseUrl : '/kcfinder/browse.php',
            filebrowserImageBrowseUrl : '/kcfinder/browse.php?type=Images',
            filebrowserUploadUrl : '/kcfinder/upload.php',
            filebrowserImageUploadUrl : '/kcfinder/upload.php?type=Images'
        });
    CKEDITOR.replace('detail_thai',
        {
            filebrowserBrowseUrl : '/kcfinder/browse.php',
            filebrowserImageBrowseUrl : '/kcfinder/browse.php?type=Images',
            filebrowserUploadUrl : '/kcfinder/upload.php',
            filebrowserImageUploadUrl : '/kcfinder/upload.php?type=Images'
        });


    $('div.alert').not('.alert-important').not('alert-danger').delay(3000).slideUp(1000);

    $('.deleteItem').click(function () {
        if (confirm('Are you sure you want to delete this ?')) {
            $('#DeleteForm').submit();

        }
    });
    $('#viewList').click(function () {
        window.location = "{{ action('Admin\ConstructionUpdateController@index') }}";
    });
</script>
@endsection