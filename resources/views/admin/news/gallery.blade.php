@extends('layouts.cms')
@section('title', 'News Detail')
@section('header')
    <link rel="stylesheet" type="text/css" href="/jquery-ui/jquery-ui.css">
    <script src='/jquery-ui/jquery-ui.js'></script>
    <meta name="cms-page-id" content="cms-page-news"/>

<style>
    .img-small-display {
        padding: 4px;
        line-height: 1.42857;
        background-color: #FFF;
        border: 1px solid #DDD;
        border-radius: 4px;
        transition: all .2s ease-in-out;
        display: inline-block;
        width: auto;
        height: 100px;
    }

    #imageSelectable .ui-selected {
        border: 5px solid darkred;
    }
</style>
@endsection

@section('content')

    @include('admin.partials.flash')
    {!! Form::open(['method' => 'POST' ,'url' => action('Admin\GalleryNewsController@store') , 'id'=>'UploadImage','files'=> true]); !!}
    {!! Form::Hidden('news_id', $news->id ) !!}
    <div class="panel panel-default">
        <div class="panel-heading">Gallery for {!! $news->headline_thai !!}</div>
        <div class="panel-body">
            @include('admin.partials.error')
            <div class="row">
                <div class="col-sm-3 col-md-3">
                    <div id="selectImage">
                        <label>Select Image (1140x700 px)</label>
                        <br/>
                        <input class="form-control" type="file" name="image_file[]" multiple="multiple" id="fileInput" accept="image/*" required/>
                    </div>

                    <div id="UploadingImage">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <ul class="image-gallery" id="uploadingImages">

                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr id="line">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <label>Select delete image </label>
                    <ul id="imageSelectable" class="image-gallery">
                        @foreach($gallery as $image)
                            <img id="{{ $image->id }}" src="{{ url($image->image) }}" class="ui-state-default img-small-display">
                        @endforeach
                    </ul>


                </div>
            </div>

        </div>
        <div class="panel-footer clearfix">
            <div class="pull-left">
                {!! Form::Button('Delete', ['class' => 'btn btn-action btn-danger deleteItem' , 'id' =>$news->id])
                !!}
            </div>

            <div class="pull-right">
                {!! Form::Button('Done', ['class' => 'btn btn-action btn-success', 'id' => 'viewItem' ]) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}


    <div class="space50"></div>

    {!! Form::open(['method' => 'DELETE' , 'url' => action('Admin\GalleryNewsController@destroy' , $news->id) , 'id' => 'DeleteForm']); !!}
    {!! Form::Hidden('ids', '' , [ 'id' => 'updateIds' ]) !!}
    {!! Form::close() !!}

@endsection


@section('script')

    <script>
        $('div.alert').not('.alert-important').not('alert-danger').delay(3000).slideUp(1000);

        $(document).ready(function () {
            var selected = [];

            $('#UploadingImage').hide();

            $("#imageSelectable").selectable({
                        stop: function () {
                            selected = [];
                            $('#imageSelectable .ui-state-default.ui-selected').each(function (index) {
                                selected.push($(this).attr('id'));
                            });
                            $('#updateIds').val(selected.toString());
                        }
                    });
            $('.deleteItem').click(function () {
                if (selected.length == 0)
                    return alert('Please select item to delete. ');

                if (confirm('Are you sure you want to delete ' + selected.length + ' item(s) ?')) {
                    $('#DeleteForm').attr('action', "{{ action('Admin\GalleryNewsController@destroy' , $news->id) }}");
                    $('#DeleteForm').submit();

                }
            });
        });

        $('#viewItem').click(function () {
            window.location = "{{  action('Admin\NewsController@edit',$news->id) }}";
        });
    </script>
    <script>

        var selDiv = "";

        document.addEventListener("DOMContentLoaded", init, false);

        function init() {
            document.querySelector('#fileInput').addEventListener('change', function(){
                    $('#UploadImage').submit();
                    }, false);
            selDiv = document.querySelector("#uploadingImages");
        }

        function handleFileSelect(e) {
            console.dir(e);
            $('#UploadingImage').show();

            if (!e.target.files) return;

            selDiv.innerHTML = "";

            var files = e.target.files;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];

                //Only pics
                if (!file.type.match('image'))
                    continue;

                var picReader = new FileReader();

                picReader.addEventListener("load", function (event) {

                    var picFile = event.target;

                    selDiv.innerHTML += '<li id="1" class="ui-state-default" style="background-image: url(' +
                    picFile.result + ');"></li>';


                });

                //Read the image
                picReader.readAsDataURL(file);
            }

        }
    </script>

@endsection