@extends('layouts.cms')
@section('title', 'News Detail')

@section('header')
<script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
<meta name="cms-page-id" content="cms-page-news"/>
<style>
    .img-small-display {
        padding: 4px;
        line-height: 1.42857;
        background-color: #FFF;
        border: 1px solid #DDD;
        border-radius: 4px;
        transition: all .2s ease-in-out;
        display: inline-block;
        width: auto;
        height: 100px;
    }
</style>
@endsection

@section('content')
@include('admin.partials.flash')
@if($news->id)
{!! Form::open(['method' => 'PATCH' , 'url' => action('Admin\NewsController@update' , $news->id) , 'role'=>'search', 'files' => true]) !!}
@else
{!! Form::open(['url' => action('Admin\NewsController@store') , 'role'=>'search','files'=> true]) !!}
@endif

<div class="panel panel-default">
    <div class="panel-heading">News</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::Label('headline_thai', 'Headline (TH)' , ['class' => 'control-label']) !!}
                    {!! Form::Text('headline_thai', $news->headline_thai , ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::Label('headline_english','Headline (EN)' , ['class' => 'control-label']) !!}
                    {!! Form::Text('headline_english', $news->headline_english , ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="form-group">
                    {!! Form::Label('is_published', ' Published' ) !!}
                    {!! Form::checkbox('is_published','1', $news->is_published, ['id' => 'is_published']) !!}
                    @if($news->published_at)<div class="form-label">First Published Date: {!! $news->published_at->format('j F Y h:i:s A') !!} </div>@endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::Label('detail_thai', 'Detail (TH)' ,['class' => 'control-label']) !!}
                    {!! Form::textarea('detail_thai', $news->detail_thai , ['class' => 'ckeditor' , 'id' => 'detail_thai']) !!}
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::Label('detail_english','Detail (EN)' ,['class' => 'control-label']) !!}
                    {!! Form::textarea('detail_english', $news->detail_english , ['class' => 'ckeditor' , 'id' => 'detail_english']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                {!! Form::label('excerpt_thai', 'Excerpt (TH)', ['class' => 'control-label']) !!}
                {!! Form::text('excerpt_thai', $news->excerpt_thai, ['class' => 'form-control', 'placeholder' => 'Excerpt (TH)']) !!}
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('excerpt_english', 'Excerpt (EN)', ['class' => 'control-label']) !!}
                {!! Form::text('excerpt_english', $news->excerpt_english, ['class' => 'form-control', 'placeholder' => 'Excerpt (EN)']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::label('display_th', 'Display TH', ['class' => 'control-label']) !!}
                    {!! Form::checkbox('display_th', '1', $news->display_th) !!}
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="form-group">
                    {!! Form::label('display_en', 'Display EN', ['class' => 'control-label']) !!}
                    {!! Form::checkbox('display_en', '1', $news->display_en) !!}
                </div>
            </div>
        </div>
<!--        <div class="row">-->
<!--            <div class="col-sm-6 col-md-6">-->
<!--                <div class="form-group">-->
<!--                    {!! Form::Label('thumbnail', 'Thumbnail Image (1766 x 1560 px)' , ['class' => 'control-label']) !!}-->
<!--                    {!! Form::file('thumbnail' , ['accept'=> 'image/*' ]) !!}-->
<!--                    @if($news->thumbnail)-->
<!--                    <br/>-->
<!--                    <img src="{!! url($news->thumbnail) !!}" class="img-small-display" style="width: 800px;">-->
<!--                    <br/>{!! $news->thumbnail !!}-->
<!--                    @endif-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        @if($news->id)
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {!! Form::Label('', 'Image Gallery (1766 x 1560 px)' ,['class' => 'control-label']) !!}
                @if($news->gallery)
                <ul class="image-gallery">
                    @foreach($news->gallery as $image)
                        <img src="{{ url($image->image) }}" class="img-small-display">
                    @endforeach
                </ul>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <a class="btn btn-action-xs btn-warning btn-xs" id="updateImage" href="{!! route('adminNewsGallery', ['id' => $news->id]) !!}"> Edit</a>
                <div class="space10"></div>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {!! Form::Label('', 'Image Gallery (1766 x 1560 px)' ,['class' => 'control-label']) !!}<br/>
                {!! Form::Submit('Add Image', ['class' => 'btn btn-action btn-info' , 'name' => 'addImage']) !!}
            </div>
        </div>
        @endif

    </div>
    <div class="panel-footer clearfix">
        @if($news->id)
        <div class="pull-left">
            {!! Form::Button('Delete', ['class' => 'btn btn-action btn-danger deleteItem' , 'id' => $news->id]) !!}
        </div>
        @endif
        <div class="pull-right">
            {!! Form::Button('Cancel', ['class' => 'btn btn-action btn-default', 'id' => 'viewList' ]) !!}
            {!! Form::Submit('Save', ['class' => 'btn btn-action btn-info' , 'name' => 'save']) !!}
            {!! Form::Submit('Save and New ', ['class' => 'btn btn-action btn-success' , 'name' =>'saveAndNew'])!!}
            {!! Form::Submit('Save and Finish', ['class' => 'btn btn-action btn-primary' , 'name' =>'saveAndClose']) !!}
        </div>
    </div>
</div>
{!! Form::close() !!}

{!! Form::open(['method' => 'DELETE' , 'url' => action('Admin\NewsController@destroy', [$news->id]) , 'id' => 'DeleteForm']) !!}
{!! Form::close() !!}

<div class="space50"></div>
@endsection


@section('script')
<script>

    CKEDITOR.replace('detail_english',
        {
            filebrowserBrowseUrl : '/kcfinder/browse.php',
            filebrowserImageBrowseUrl : '/kcfinder/browse.php?type=Images',
            filebrowserUploadUrl : '/kcfinder/upload.php',
            filebrowserImageUploadUrl : '/kcfinder/upload.php?type=Images'
        });
    CKEDITOR.replace('detail_thai',
        {
            filebrowserBrowseUrl : '/kcfinder/browse.php',
            filebrowserImageBrowseUrl : '/kcfinder/browse.php?type=Images',
            filebrowserUploadUrl : '/kcfinder/upload.php',
            filebrowserImageUploadUrl : '/kcfinder/upload.php?type=Images'
        });


    $('div.alert').not('.alert-important').not('alert-danger').delay(3000).slideUp(1000);

    $('.deleteItem').click(function () {
        if (confirm('Are you sure you want to delete this ?')) {
            $('#DeleteForm').submit();

        }
    });
    $('#viewList').click(function () {
        window.location = "{{ action('Admin\NewsController@index') }}";
    });
</script>
@endsection