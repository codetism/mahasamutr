@extends('layouts.cms')

@section('title', 'News')

@section('header')
<meta name="cms-page-id" content="cms-page-news"/>
<style>
    .btn-action-xs {
        width: 70px;
    }
</style>

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        News
        <a class="btn btn-action-xs btn-info btn-xs pull-right" href="{{ action('Admin\NewsController@create') }}">New</a>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered dataTable">
            <thead>
            <tr>
                <th>ID</th>
                <th>Headline TH</th>
                <th>Headline EN</th>
                <th>Created Date</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($news as $news_item)
            <tr>
                <td>{!! $news_item->id !!}</td>
                <td>{!! $news_item->headline_thai !!}</td>
                <td>{!! $news_item->headline_english !!}</td>
                <td>@if($news_item->created_at){!! $news_item->created_at->format('j F Y h:i:s A') !!}@endif</td>
                <td>@if($news_item->is_published)
                    <a class="btn btn-action-xs btn-success btn-xs unpublishItem" id="{!! $news_item->id !!}">Published</a>
                    @else
                    <a class="btn btn-action-xs btn-warning btn-xs publishItem" id="{!! $news_item->id !!}">Draft</a>
                    @endif
                </td>
                <td>
                    <a class="btn btn-action-xs btn-primary  btn-xs" href="{{ action('Admin\NewsController@edit' , $news_item->id) }}">Edit</a>
                    <a class="btn btn-action-xs btn-danger btn-xs deleteItem" id="{!! $news_item->id !!}" name="{!! $news_item->headline_thai !!}">Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

{!! Form::open(['method' => 'DELETE' , 'url' =>'#', 'class' =>'form-horizontal' , 'id' => 'DeleteForm']); !!}
{!! Form::close() !!}

{!! Form::open(['method' => 'POST' , 'url' => '#' , 'id' => 'PublishedForm']); !!}
{!! Form::Hidden('id', 0,  ['id' => 'service_id']) !!}
{!! Form::Hidden('is_published', 0 , ['id' => 'is_published']) !!}
{!! Form::close() !!}
@endsection

@section('script')
<script>
    $('div.alert').not('.alert-important').not('alert-danger').delay(3000).slideUp(1000);

    $('.deleteItem').click(function () {
        if (confirm('Are you sure you want to delete "' + $(this).attr("name") + '" ?')) {
            $('#DeleteForm').attr('action', "{{ action('Admin\NewsController@index') }}" + "/" + $(this).attr("id"));
            $('#DeleteForm').submit();
        }
    });

    $('.publishItem').click(function () {

        $('#PublishedForm').attr('action', "{{ action('Admin\NewsController@publish') }}");
        $('#is_published').val(1);
        $('#service_id').val($(this).attr("id"));
        $('#PublishedForm').submit();
    });

    $('.unpublishItem').click(function () {
        $('#PublishedForm').attr('action', "{{ action('Admin\NewsController@publish') }}");
        $('#is_published').val(0);
        $('#service_id').val($(this).attr("id"));
        $('#PublishedForm').submit();
    });

    $(document).ready(function () {
        $('.dataTable').dataTable({
            order: [
                [0, "desc"]
            ]
        });
    });
</script>
@endsection
