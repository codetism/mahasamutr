@extends('layouts.cms')

@section('title')
Contact Message
@endsection

@section('header')
<meta name="cms-page-id" content="cms-contact"/>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Contact Message</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <table class="table table-striped table-bordered">
                    <tbody>
                    <tr>
                        <td>Created At</td>
                        <td>{{ $member->created_at }}</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>{{ $member->title }} {{ $member->first_name }} {{ $member->last_name }}</td>
                    </tr>
                    <tr>
                        <td>Mobile Number</td>
                        <td>{{ $member->mobile }}</td>
                    </tr>
                    <tr>
                        <td>E-mail</td>
                        <td>{{ $member->email }}</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>{{ $member->address }}</td>
                    </tr>
                    <tr>
                        <td>Interested In</td>
                        <td>{{ $member->interested }}</td>
                    </tr>
                    <tr>
                        <td>Hear From</td>
                        <td>{{ $member->known_from }}</td>
                    </tr>
                    <tr>
                        <td>Message</td>
                        <td>{{ $member->message }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-footer clearfix">
        <div class="pull-left">
            {!! Form::Button('Delete', ['class' => 'btn btn-action btn-danger deleteItem' , 'id' => $member->id]) !!}
        </div>
        <div class="pull-right">
            {!! Form::Button('Cancel', ['class' => 'btn btn-action btn-default', 'id' => 'viewList' ]) !!}
        </div>
    </div>
</div>
{!! Form::close() !!}

{!! Form::open(['method' => 'DELETE' , 'url' => action('Admin\MemberController@destroy', [$member->id]) , 'id' => 'DeleteForm']) !!}
{!! Form::close() !!}

<div class="space50"></div>
@endsection


@section('script')
<script>
    $('.deleteItem').click(function () {
        if (confirm('Are you sure you want to delete this message?')) {
            $('#DeleteForm').submit();
        }
    });
    $('#viewList').click(function () {
        window.location = "{{ action('Admin\MemberController@index') }}";
    });
</script>
@endsection