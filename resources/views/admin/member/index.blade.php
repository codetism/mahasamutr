@extends('layouts.cms')

@section('title')
    Member List
@endsection

@section('header')
    <meta name="cms-page-id" content="cms-member"/>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Member List
        </div>

        <div class="panel-body">
            <table class="table table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Interest</th>
                        <th>Known Form</th>
                        <th>Subscribed Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                @foreach($members as $member)
                    <tr id="data{{$member->id}}">
                        <td>{{$member->id}}</td>
                        <td>{{$member->title}} {{$member->first_name}} {{$member->last_name}}</td>
                        <td>{{$member->email}}</td>
                        <td>{{$member->mobile}}</td>
                        <td>{{$member->interested}}</td>
                        <td>{{$member->known_from}}</td>
                        <td>{{$member->created_at}}</td>
                        <td>
                            <a class="btn btn-action-xs btn-primary  btn-xs"
                               href="{{ action('Admin\MemberController@show' , $member->id) }}">Show</a>
                            <a class="btn btn-action-xs btn-danger btn-xs deleteItem" id="{!! $member->id !!}">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {!! Form::open(['method' => 'DELETE' , 'url' =>'#', 'class' =>'form-horizontal' , 'id' => 'DeleteForm']); !!}
    {!! Form::close() !!}
@stop

@section('script')
<script>
    $('div.alert').not('.alert-important').not('alert-danger').delay(3000).slideUp(1000);

    $('.deleteItem').click(function () {
        if (confirm('Are you sure you want to delete this message?')) {
            $('#DeleteForm').attr('action', "{{ action('Admin\MemberController@index') }}" + "/" + $(this).attr("id"));
            $('#DeleteForm').submit();
        }
    });
</script>
@endsection
