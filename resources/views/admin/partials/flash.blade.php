
@if(Session::has('flash_message'))
    <div class="row">
        <div class="col-md-12 ">
            <div class="alert alert-success {{ Session::has('flash_message_important') ? 'alert-important':'' }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_message') }}
            </div>
        </div>
    </div>
@endif

@if(Session::has('error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger {{ Session::has('error_important') ? 'alert-important':'' }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('error') }}
            </div>
        </div>
    </div>
@endif
