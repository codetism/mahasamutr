@if($errors->any())
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <ul class="alert alert-danger" style="padding-left: 50px">
                @foreach($errors->all() as $error)
                    <li> {{ $error }}</li>
                @endforeach

            </ul>
        </div>
    </div>
@endif