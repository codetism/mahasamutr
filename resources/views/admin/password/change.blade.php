<?php $action = 'Admin\PasswordController@postChange'; ?>

@extends('layouts.cms')

@section('content')
    <div class="row">
        @if(!$errors->isEmpty())
            <div class="row">
                <div class="col-md-3">
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        @if(Session::has('messages'))
            <div class="row">
                <div class="col-md-3">
                    <div class="alert alert-success" role="alert">
                        @foreach(Session::get('messages')->all() as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        {!! Form::open(compact('action')) !!}
        <div class="row">
            <div class="form-group col-md-3">
                {!! Form::label('current_password', 'Current Password *') !!}
                {!! Form::password('current_password', ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-3">
                {!! Form::label('new_password', 'New Password *') !!}
                {!! Form::password('new_password', ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-3">
                {!! Form::label('confirm_password', 'Confirm Password *') !!}
                {!! Form::password('confirm_password', ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
    </div>
@endsection