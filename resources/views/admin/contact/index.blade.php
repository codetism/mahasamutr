@extends('layouts.cms')

@section('title')
    Contact Message
@endsection

@section('header')
    <meta name="cms-page-id" content="cms-contact"/>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Contact Message
        </div>

        <div class="panel-body">
            <table class="table table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Message</th>
                        <th>Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($messages as $message)
                        <tr id="data{{$message->id}}">
                            <td>{{ $message->id }}</td>
                            <td>{{ $message->first_name }} {{ $message->last_name }}</td>
                            <td>{{ $message->email }}</td>
                            @if($message->read)
                                <td>{{ substr($message->message, 0, 100) }}</td>
                            @else
                                <td><i class="glyphicon glyphicon-asterisk"> <strong>{{ substr($message->message, 0, 100) }}</strong></td>
                            @endif
                            <td>{{ $message->created_at }}</td>
                            <td>
                                <a class="btn btn-action-xs btn-primary  btn-xs"
                                   href="{{ action('Admin\ContactController@show', $message->id) }}">View</a>
                                <a class="btn btn-action-xs btn-danger btn-xs deleteItem" id="{!! $message->id !!}"
                                   name="{!! $message !!}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {!! Form::open(['method' => 'delete' , 'url' =>'#', 'class' =>'form-horizontal' , 'id' => 'DeleteForm']); !!}
    {!! Form::close() !!}

@stop

@section('script')
    <script>
        $('div.alert').not('.alert-important').not('alert-danger').delay(3000).slideUp(1000);

        $('.deleteItem').click(function () {
            if (confirm('Are you sure you want to delete this message?')) {
                $('#DeleteForm').attr('action', "{{ action('Admin\ContactController@index') }}" + "/" + $(this).attr("id"));
                $('#DeleteForm').submit();
            }
        });
    </script>
@endsection
