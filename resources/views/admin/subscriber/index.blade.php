@extends('layouts.cms')

@section('title')
    Subscriber List
@endsection

@section('header')
    <meta name="cms-page-id" content="cms-subscriber"/>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Subscriber CMS - List
        </div>

        <div class="panel-body">
            <table class="table table-striped table-bordered datatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Email</th>
                    <th>Subscribed Date</th>
                </tr>
                </thead>
                <tbody>

                @foreach($subscribers as $subscriber)
                <tr id="data{{$subscriber->id}}">
                    <td>{{$subscriber->id}}</td>
                    <td>{{$subscriber->email}}</td>
                    <td>{{$subscriber->created_at}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
