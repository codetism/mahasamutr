@extends('layouts.main')

@section('title')
    Contact Us | MahaSamutr
@endsection

@section('header')

@section('content')
    <section class="section-hero">
        <div class="container-fluid">
            <div class="hero-block">
                <div class="hero-full">
                    <figure ms-parallax>
                        <picture>
                            <source srcset="/img/contact/banner-correct.jpg" media="(min-width: 1920px)">
                            <source srcset="/img/contact/banner-correct.jpg" media="(min-width: 1366px)">
                            <img src="/img/contact/banner-correct.jpg" alt="">
                        </picture>
                    </figure>
                </div>
                <div class="scroll-down" b-scroll-to="#contact">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 34">
                        <path class="st0" d="M11,33L11,33C5.5,33,1,28.5,1,23V11C1,5.5,5.5,1,11,1h0c5.5,0,10,4.5,10,10v12C21,28.5,16.5,33,11,33z"></path>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                    </svg>
                    <div></div>
                </div>
            </div>
        </div>
    </section>
    <section id="contact" class="contact">
        <div class="container">
            <div class="contact-headline text-center" ms-scroll-trigger>
                <h2 class="text-uppercase">{!! Lang::get('messages.contact_info_title') !!}</h2>
                <p>{!! Lang::get('messages.contact_info_desc') !!}</p>
            </div>
            <div class="contact-wrapper" ms-scroll-trigger>
                <div class="row">
                    <div class="col-md-4 push-md-8 col-lg-3 push-lg-8">
                        <div class="contact-address">
                            <div class="contact-screen-logo"><img src="/img/bg/screen-logo.png"></div>
                            <div class="contact-address-item">
                                <h4>{!! Lang::get('messages.contact_cc_enquiries_title') !!}</h4>
                                <address>
                                    <p><strong class="text-uppercase">{!! Lang::get('messages.contact_bkk_title') !!}</strong><br> {!! Lang::get('messages.contact_bkk_address_l1') !!}<br> {!! Lang::get('messages.contact_bkk_address_l2') !!}<br> {!! Lang::get('messages.contact_bkk_address_l3') !!}<br> {!! Lang::get('messages.contact_bkk_address_l4') !!}<br> {!! Lang::get('messages.contact_bkk_address_l5') !!}<br> {!! Lang::get('messages.contact_bkk_address_l6') !!}
                                    </p>
                                </address>
                                <address>
                                    <p><strong class="text-uppercase">{!! Lang::get('messages.contact_huahin_title') !!}</strong> <br> {!! Lang::get('messages.contact_huahin_address_l1') !!}<br> {!! Lang::get('messages.contact_huahin_address_l2') !!}<br> {!! Lang::get('messages.contact_huahin_address_l3') !!}<br> {!! Lang::get('messages.contact_huahin_address_l4') !!}<br> {!! Lang::get('messages.contact_huahin_address_l5') !!}
                                    </p>
                                </address>
                            </div>
                            <div class="contact-address-item">
                                <h4>{!! Lang::get('messages.contact_huahin_enquiries_title') !!}</h4>
                                <address>
                                    <p>{!! Lang::get('messages.contact_huahin_lv_address_l1') !!}<br> {!! Lang::get('messages.contact_huahin_lv_address_l2') !!}<br> {!! Lang::get('messages.contact_huahin_lv_address_l3') !!}<br> {!! Lang::get('messages.contact_huahin_lv_address_l4') !!}
                                    </p>
                                </address>
                            </div>
                            <div class="developed">
                                <p>Project developed by <img src="/img/contact/pace-logo.svg">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 pull-md-4 col-lg-7 offset-lg-1 pull-lg-3">
                        <div class="contact-form">
                            <form action="/send" method="post" accept-charset="UTF-8" autocomplete="off">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>{!! Lang::get('messages.contact_section_title') !!}*</label>
                                            <select name="c-title" b-select-picker data-select-picker='{"placeholder":"Title"}' required>
                                                <option value="" selected>{!! Lang::get('messages.contact_section_title') !!}*</option>
                                                <option value="Mr.">{!! Lang::get('messages.contact_section_mr') !!}</option>
                                                <option value="Mrs.">{!! Lang::get('messages.contact_section_mrs') !!}</option>
                                                <option value="Miss">{!! Lang::get('messages.contact_section_miss') !!}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <label>{!! Lang::get('messages.contact_section_firstname') !!}*</label>
                                            <input name="c-firstname" type="text" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>{!! Lang::get('messages.contact_section_lastname') !!}*</label>
                                            <input name="c-lastname" type="text" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{!! Lang::get('messages.contact_section_mobile') !!}*</label>
                                            <input name="c-mobile" type="tel" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{!! Lang::get('messages.contact_section_email') !!}*</label>
                                            <input name="c-email" type="email" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>{!! Lang::get('messages.contact_section_address') !!}</label>
                                            <textarea name="c-address" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{!! Lang::get('messages.contact_section_interested') !!}*</label>
                                            <select name="c-interest" b-select-picker data-select-picker='{"placeholder":"Interested in"}' required>
                                                <option value="" selected>Interested in*</option>
                                                <option value="Luxury Villas">{!! Lang::get('messages.contact_section_lv') !!}</option>
                                                <option value="Private Country Club - Lifetime Membership">{!! Lang::get('messages.contact_section_pcc') !!}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>{!! Lang::get('messages.contact_section_hear') !!}*</label>
                                            <select name="c-hear" b-select-picker data-select-picker='{"placeholder":"How did you hear about us?"}' required>
                                                <option value="" selected>How did you hear about us?</option>
                                                <option value="Newspaper/Magazine">{!! Lang::get('messages.contact_section_news_mags') !!}</option>
                                                <option value="Site Fence/Billboard">{!! Lang::get('messages.contact_section_billboard') !!}</option>
                                                <option value="Friend">{!! Lang::get('messages.contact_section_friend') !!}</option>
                                                <option value="Social Media">{!! Lang::get('messages.contact_section_social') !!}</option>
                                                <option value="Website">{!! Lang::get('messages.contact_section_website') !!}</option>
                                                <option value="Other">{!! Lang::get('messages.contact_section_other') !!}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>{!! Lang::get('messages.contact_section_message') !!}</label>
                                            <textarea name="c-message" class="form-control" rows="3" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact-form-bottom clearfix">
                                    <span class="contact-field float-left">*Compulsory fields</span>
                                    <button type="submit" class="btn-contact float-right">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection

@section('script')
