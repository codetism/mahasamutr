<!DOCTYPE html>
<html>
    <body>
        <h3>{{ $title }} {{ $first_name }} {{ $last_name }} is now registered to MahaSamutr Country Club & Luxury Villas Hua Hin.</h3>
        <hr>
        <h4>Member's detail</h4>
        <table>
            <tr>
                <td><strong>Name:</strong></td>
                <td>{{ $title }} {{ $first_name }} {{ $last_name }}</td>
            </tr>
            <tr>
                <td><strong>Mobile:</strong></td>
                <td>{{ $mobile }}</td>
            </tr>
            <tr>
                <td><strong>Email:</strong></td>
                <td>{{ $email }}</td>
            </tr>
            <tr>
                <td><strong>Address:</strong></td>
                <td>{{ $address }}</td>
            </tr>
            <tr>
                <td><strong>Interested In:</strong></td>
                <td>{{ $interested }}</td>
            </tr>
            <tr>
                <td><strong>Hear Us From:</strong></td>
                <td>{{ $known_from }}</td>
            </tr>
        </table>
        <hr>
        {{ $content }}
        <hr>
        <p>&copy; 2017 MahaSamutr Country Club & Luxury Villas Hua Hin. All rights reserved.</p>
    </body>
</html>