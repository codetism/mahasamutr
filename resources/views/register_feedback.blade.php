<!DOCTYPE html>
<html>
    <body>
        <p>Dear {{ $first_name }} {{ $last_name }}</p>
        <p>Your registration is currently being processed. We appreciate your interest in MahaSamutr Country Club & Luxury Villas. For any further enquiries, please contact +66 (0) 32 907 900 or info@mahasamutr.com.</p>
        <hr>
        <p>&copy; 2017 MahaSamutr Country Club & Luxury Villas Hua Hin. All rights reserved.</p>
    </body>
</html>