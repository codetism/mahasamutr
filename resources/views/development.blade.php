@extends('layouts.main')

@section('title')
    Development | MahaSamutr
@endsection

@section('header')

@section('content')
    <section class="section-hero">
        <div class="container-fluid">
            <div class="hero-block">
                <div class="hero-full">
                    <figure ms-parallax>
                        <picture>
                            <source srcset="/img/development/banner_w_1366.jpg" media="(min-width: 1366px)">
                            <img src="/img/development/banner_w_1366.jpg" alt="">
                        </picture>
                    </figure>
                </div>
                <div class="scroll-down" b-scroll-to="#development">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 34">
                        <path class="st0" d="M11,33L11,33C5.5,33,1,28.5,1,23V11C1,5.5,5.5,1,11,1h0c5.5,0,10,4.5,10,10v12C21,28.5,16.5,33,11,33z"></path>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                    </svg>
                    <div></div>
                </div>
            </div>
        </div>
    </section>
    <section id="development" class="development">
        <div class="container">
            <div class="development-block text-center" ms-scroll-trigger>
                <div class="development-title">
                    <h2 class="text-uppercase"><span class="txt-1">pace development</span> <span class="txt-2">corporation PLC.</span><span class="txt-3">developer</span></h2>
                </div>
                <p><span class="d-lg-block">{!! Lang::get('messages.dev_pace_desc_p1_l1') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_pace_desc_p1_l2') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_pace_desc_p1_l3') !!}</span>
                </p>
                <p><span class="d-lg-block">{!! Lang::get('messages.dev_pace_desc_p2_l1') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_pace_desc_p2_l2') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_pace_desc_p2_l3') !!}</span>
                </p>
                <p><span class="d-lg-block">{!! Lang::get('messages.dev_pace_desc_p3_l1') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_pace_desc_p3_l2') !!}</span>
                    <span class="d-block"><a href="http://www.pacedev.com" target="_blank" class="link">www.pacedev.com</a></span>
                </p>
            </div>
            <div id="development-slider" class="development-slider show-side-padding" b-slider data-slick='{"slidesToShow": 3, "autoplay":false, "infinite": false}' data-responsive='{"md":2, "sm":2, "xs":1}' ms-scroll-trigger>
                <div class="c-block ratio-development">
                    <figure><img src="/img/development/development01.jpg" alt="Mahanakhon">
                        <figcaption class="c-block-content text-white">
                            <div class="c-block-text">
                                <h3 class="c-block-title text-uppercase">Mahanakhon</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-development">
                    <figure><img src="/img/development/VIEW-01---VIEW-ACROSS-LUMPINI-PARK.png" alt="Nimit">
                        <figcaption class="c-block-content text-white">
                            <div class="c-block-text">
                                <h3 class="c-block-title text-uppercase">Nimit Langsuan</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-development">
                    <figure><img src="/img/development/development03.jpg" alt="Dean & Deluca">
                        <figcaption class="c-block-content">
                            <div class="c-block-text">
                                <h3 class="c-block-title text-uppercase">Dean & Deluca</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-development">
                    <figure><img src="/img/development/development04.jpg" alt="Saladaeng Residences">
                        <figcaption class="c-block-content text-white">
                            <div class="c-block-text">
                                <h3 class="c-block-title text-uppercase">Saladaeng Residences</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                <div class="c-block ratio-development">
                    <figure><img src="/img/development/development05.jpg" alt="Ficus Lane">
                        <figcaption class="c-block-content text-white">
                            <div class="c-block-text">
                                <h3 class="c-block-title text-uppercase">Ficus Lane</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </section>
    <section id="team" class="team">
        <div class="container">
            <div class="team-container">
                <div class="team-headline text-center" ms-scroll-trigger>
                    <div class="team-title">
                        <h2 class="text-uppercase">{!! Lang::get('messages.dev_team_title') !!}</h2>
                    </div>
                    <p><span class="d-lg-block">{!! Lang::get('messages.dev_team_desc_l1') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_team_desc_l2') !!}</span>
                    </p>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xl-5 offset-xl-1">
                        <div class="team-block team-block-1" ms-scroll-trigger>
                            <div class="team-logo"><img src="/img/development/crystal-lagoons-logo.png" alt="Crystal Lagoons Corporation" width="208">
                            </div>
                            <p><strong class="text-uppercase d-block">{!! Lang::get('messages.dev_crystal_lagoon_title') !!}</strong> <span class="d-lg-block">{!! Lang::get('messages.dev_crystal_lagoon_desc_l1') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_crystal_lagoon_desc_l2') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_crystal_lagoon_desc_l3') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_crystal_lagoon_desc_l4') !!}</span>
                                <span class="d-block">{!! Lang::get('messages.dev_shared_visit') !!}<a href="http://www.crystal-lagoons.com" target="_blank" class="link">{!! Lang::get('messages.dev_crystal_lagoon_desc_l5') !!}</a></span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-5 offset-xl-1">
                        <div class="team-block team-block-2" ms-scroll-trigger>
                            <div class="team-logo"><img src="/img/development/thai-obayashi-logo.png" alt="Thai Obayashi" width="181">
                            </div>
                            <p><strong class="text-uppercase d-block">{!! Lang::get('messages.dev_thai_obayashi_title') !!}</strong> <span class="d-lg-block">{!! Lang::get('messages.dev_thai_obayashi_desc_l1') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_thai_obayashi_desc_l2') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_thai_obayashi_desc_l3') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_thai_obayashi_desc_l4') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_thai_obayashi_desc_l5') !!}</span>
                                <span class="d-block">{!! Lang::get('messages.dev_shared_visit') !!}<a href="http://www.thaiobayashi.co.th" target="_blank" class="link">{!! Lang::get('messages.dev_thai_obayashi_desc_l6') !!}</a></span>
                            </p>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6 col-xl-5 offset-xl-1">
                        <div class="team-block team-block-3" ms-scroll-trigger>
                            <div class="team-logo"><img src="/img/development/ILC-logo.png" alt="International Leisure Consultants (ILC)" width="60">
                            </div>
                            <p><strong class="text-uppercase d-block">{!! Lang::get('messages.dev_ilc_title') !!}</strong> <span class="d-lg-block">{!! Lang::get('messages.dev_ilc_desc_l1') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_ilc_desc_l2') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_ilc_desc_l3') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_ilc_desc_l4') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_ilc_desc_l5') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_ilc_desc_l6') !!}</span> <span class="d-lg-block">{!! Lang::get('messages.dev_ilc_desc_l7') !!}</span>
                                <span class="d-block">{!! Lang::get('messages.dev_shared_visit') !!}<a href="http://www.ilc-world.com" target="_blank" class="link">{!! Lang::get('messages.dev_ilc_desc_l8') !!}</a></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="fact-sheet" class="fact-sheet">
        <div class="section-full">
            <figure>
                <picture>
                    <source srcset="/img/development/fact-sheet_w_2732.jpg" media="(min-width: 1920px)">
                    <source srcset="/img/development/fact-sheet_w_2147.jpg" media="(min-width: 1366px)">
                    <img src="/img/development/fact-sheet_w_1366.jpg" alt="">
                </picture>
            </figure>
        </div>
        <div class="section-wrapper">
            <div class="fact-sheet-block section-block" ms-scroll-trigger>
                <div class="fact-sheet-title">
                    <h2 class="text-white text-uppercase">{!! Lang::get('messages.dev_factsheet_title') !!}</h2>
                </div>
                <div class="fact-sheet-item">
                    <h6 class="text-white text-uppercase">{!! Lang::get('messages.dev_factsheet_cc_title') !!}</h6>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_developer_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_developer_s2') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_location_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_location_s2') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_project_type_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_project_type_s2') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_tenure_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_tenure_s2') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_property_area_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_property_area_s1') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_lagoon_area_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_lagoon_area_s2') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_club_house_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_cc_club_house_s2') !!}</p>
                        </div>
                    </div>
                </div>
                <div class="fact-sheet-item">
                    <h6 class="text-white text-uppercase"><p>{!! Lang::get('messages.dev_factsheet_lv_title') !!}</p></h6>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_developer_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_developer_s2') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_location_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_location_s2') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_project_type_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_project_type_s2') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_tenure_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_tenure_s2') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_property_area_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_property_area_s2') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_total_villas_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_total_villas_s2') !!}</p>
                        </div>
                    </div>
                    <div class="fact-sheet-lists">
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_villa_details_s1') !!}</p>
                        </div>
                        <div class="fact-sheet-list">
                            <p>{!! Lang::get('messages.dev_factsheet_lv_villa_details_s2') !!}</p>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection

@section('script')
