<!DOCTYPE html>
<html>
    <body>
        <br>
        <h3>{{ $title }} {{ $first_name }} {{ $last_name }}</h3>
        <p>PHONE - {{ $phone }}</p>
        <p>EMAIL - {{ $email }}</p>
        <p>ADDRESS - {{ $address }}</p>
        <p>INTEREST - {{ $interest }}</p>
        <p>HEAR FROM - {{ $hear }}</p>
        <hr>
        <p>{{$content}}</p>
        <br>
        <p>-- Sent from MahaSamutr website</p>
    </body>
</html>