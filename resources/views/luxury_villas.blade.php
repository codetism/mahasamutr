@extends('layouts.main')

@section('title')
    Luxury Villa | MahaSamutr
@endsection

@section('header')

@section('content')
    <!-- luxury banner -->
    <section class="section-hero luxury-banner">
        <div class="container-fluid">
            <div class="hero-block" ms-scroll-trigger>
                <div class="hero-full" ms-parallax>
                    <figure>
                        <picture>
                            <source srcset="/img/luxury/banner.jpg" media="(min-width: 1920px)" />
                            <source srcset="/img/luxury/banner.jpg" media="(min-width: 1366px)" />
                            <img src="/img/luxury/banner.jpg" alt="" />
                        </picture>
                    </figure>
                    <div class="wrapper-panel">
                        <div class="hero-content" >
                            <h1 class="text-white text-uppercase">
                                <span class="txt-1 d-block">{!! Lang::get('messages.lv_banner_l1') !!}</span><span class="txt-2 d-block">{!! Lang::get('messages.lv_banner_l2') !!}</span>
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="scroll-down" b-scroll-to="#villa-concept">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 34">
                        <path class="st0" d="M11,33L11,33C5.5,33,1,28.5,1,23V11C1,5.5,5.5,1,11,1h0c5.5,0,10,4.5,10,10v12C21,28.5,16.5,33,11,33z"></path>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <polygon points="8,12.6 0.3,4.9 1.7,3.4 8,9.7 14.3,3.4 15.7,4.9 "></polygon>
                    </svg>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- luxury concept -->
    <section id="villa-concept" class="luxury-concept" style="padding-bottom: 0px;">
        <div class="luxury-logo">
            <img src="/img/bg/screen-logo.png">
        </div>
        <div class="container" ms-scroll-trigger >
            <div class="row">
                <div class="col-lg-5 col-xl-4 offset-xl-1">
                    <h3 class="text-uppercase text-center">{!! Lang::get('messages.lv_villa_concept_title') !!}</h3>
                </div>
                <div class="col-lg-7 col-xl-6">
                    <p>{!! Lang::get('messages.lv_villa_concept_desc_p1') !!}</p>
                    <p>{!! Lang::get('messages.lv_villa_concept_desc_p2') !!}</p>
                </div>
            </div>
            <div class="row" style="margin-top: 47px;">
                <div class="col-lg-5 col-xl-4 offset-xl-1"></div>
                <div class="col-lg-7 col-xl-6 row-download">
                    <a href="/downloads/MHSM Villas_Big Brochure Final_20180122.pdf">
                        <img class="img-download" src="/img/luxury/download-icon.png" alt=""/>
                        <p class="p-download">{!! Lang::get('messages.pcc_concept_download') !!}</p>
                    </a>

                </div>
            </div>
        </div>
    </section>
    <!-- luxury award -->
    <section class="luxury-award">
        <div class="container-fluid">
            <div class="c-block ratio-luxury-award">
                <figure>
                    <picture>
                        <source srcset="/img/luxury/award.jpg" media="(min-width: 1920px)" />
                        <source srcset="/img/luxury/award.jpg" media="(min-width: 1366px)" />
                        <img src="/img/luxury/award.jpg" alt="" />
                    </picture>
                </figure>
                <div class="c-block-content" ms-scroll-trigger>
                    <h1 class="text-white text-uppercase"><span class="txt-1 d-block">{!! Lang::get('messages.lv_award_title_l1') !!}</span><span class="txt-2 d-block">{!! Lang::get('messages.lv_award_title_l2') !!}</span></h1>
                </div>
            </div>
            <div class="luxury-award-content" ms-scroll-trigger>
                <div class="row">
                    <div class="col-lg-5 col-xl-4 offset-xl-1">
                        <div class="award-items">
                            <div class="award-item">
                                <img src="/img/luxury/award-huahin.svg" alt="">
                            </div>
                            <div class="award-item">
                                <img src="/img/luxury/award-design.svg" alt="">
                            </div>
                            <div class="award-item">
                                <img src="/img/luxury/award-thailand.svg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-xl-6">
                        <div class="luxury-award-detail">
                            <p>{!! Lang::get('messages.lv_award_desc_p1') !!}</p>
                            <h5>{!! Lang::get('messages.lv_award_desc_th_award_title') !!}</h5>
                            <ul>
                                <li>{!! Lang::get('messages.lv_award_desc_th_award_1') !!}</li>
                                <li>{!! Lang::get('messages.lv_award_desc_th_award_2') !!}</li>
                                <li>{!! Lang::get('messages.lv_award_desc_th_award_3') !!}</li>
                            </ul>
                            <h5>{!! Lang::get('messages.lv_award_desc_se_award_title') !!}</h5>
                            <ul>
                                <li>{!! Lang::get('messages.lv_award_desc_se_award_1') !!}</li>
                                <li>{!! Lang::get('messages.lv_award_desc_se_award_2') !!}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="luxury-award-lists-block">
                <div class="luxury-award-lists">
                    <div class="luxury-award-list" ms-scroll-trigger>
                        <h4>{!! Lang::get('messages.lv_design_concept_title') !!}</h4>
                        <p>{!! Lang::get('messages.lv_design_concept_desc_p1') !!}</p>
                        <p>{!! Lang::get('messages.lv_design_concept_desc_p2') !!}</p>
                        <p>{!! Lang::get('messages.lv_design_concept_desc_p3') !!}</p>
                        <p>{!! Lang::get('messages.lv_design_concept_desc_p4') !!}</p>
                    </div>
                    <div class="luxury-award-list" ms-scroll-trigger>
                        <h4>{!! Lang::get('messages.lv_benefits_title') !!}</h4>
                        <p>{!! Lang::get('messages.lv_benefits_desc_p1') !!}</p>
                        <p>{!! Lang::get('messages.lv_benefits_desc_p2') !!}</p>
                    </div>
                </div>
            </div>
            <div ms-parallax-section>
                <div ms-parallax-item>
                    <div class="luxury-award-gallery">

                        <div class="luxury-award-gallery-blocks">
                            <div class="luxury-award-gallery-block">
                                <div class="c-block ratio-luxury-award-gallery">
                                    <figure>
                                        <img src="/img/luxury/gallery/1.jpg" alt="">
                                    </figure>
                                </div>
                            </div>
                            <div class="luxury-award-gallery-block">
                                <div class="c-block ratio-luxury-award-gallery">
                                    <figure>
                                        <img src="/img/luxury/gallery/2.jpg" alt="">
                                    </figure>
                                </div>
                            </div>
                            <div class="luxury-award-gallery-block">
                                <div class="c-block ratio-luxury-award-gallery">
                                    <figure>
                                        <img src="/img/luxury/gallery/3.jpg" alt="">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <a href="/gallery#luxury-villas">
                            <div class="link-gallery">
                                <p>
                                    <span>{!! Lang::get('messages.button_gallery') !!}</span>
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- plots-layout -->
    <section id="plots-layout" class="plots-layout">
        <div class="container-fluid" ms-scroll-trigger>
            <div class="headline text-center">
                <h2 class="text-uppercase">
                    <span>{!! Lang::get('messages.lv_plots_layouts_title_s1') !!}</span> {!! Lang::get('messages.lv_plots_layouts_title_s2') !!}
                </h2>
            </div>
            <div ms-tab>
                <ul class="tab-header-container">
                    <li><a href="#" class="btn" data-trigger="#layout-master"><span>MASTER PLAN</span></a></li>
                    <li><a href="#" class="btn" data-trigger="#layout-gable"><span>VILLA GABLE</span></a></li>
                    <li><a href="#" class="btn" data-trigger="#layout-triangle"><span>VILLA TRIANGLE</span></a></li>
                    <li><a href="#" class="btn" data-trigger="#layout-gable-os"><span>VILLA GABLE ON SLOPE</span></a></li>
                    <li><a href="#" class="btn" data-trigger="#layout-triangle-os"><span>VILLA TRIANGLE ON SLOPE</span></a></li>
                </ul>
                <div class="tab-content-container">
                    <div id="layout-master" ms-tab-content>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-1">
                                    <div class="tab-caption">
                                        <h5>MASTER PLAN</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="layout">
                                <div class="layout-inner">
                                    <img src="/img/luxury/layouts/master-plan.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="layout-gable" ms-tab-content>
                        <div class="container">
                            <div class="row no-gutters">
                                <div class="col-md-4 push-md-8 col-xl-4 push-xl-7 offset-xl-1">
                                    <div class="tab-caption">
                                        <h5>VILLA GABLE</h5>
                                        <p>4 {!! Lang::get('messages.lv_plots_layouts_bedrooms') !!} | 5 {!! Lang::get('messages.lv_plots_layouts_bathrooms') !!}<br>{!! Lang::get('messages.lv_plots_layouts_parking') !!} 3 {!! Lang::get('messages.lv_plots_layouts_parking_unit') !!}</p>
                                        <p>{!! Lang::get('messages.lv_plots_layouts_usable_area') !!}: 447.2 {!! Lang::get('messages.lv_plots_layouts_sqm') !!}</p>
                                        <p>{!! Lang::get('messages.lv_plots_layouts_land_area') !!}: 166.34 - 225.29 {!! Lang::get('messages.lv_plots_layouts_sqw') !!}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="layout">
                                <div class="layout-inner">
                                    <img class="" src="/img/luxury/layouts/villa-gable.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="layout-triangle" ms-tab-content>
                        <div class="container">
                            <div class="row no-gutters">
                                <div class="col-md-4 push-md-8 col-xl-4 push-xl-7 offset-xl-1">
                                    <div class="tab-caption">
                                        <h5>VILLA TRIANGLE</h5>
                                        <p>4 {!! Lang::get('messages.lv_plots_layouts_bedrooms') !!} | 5 {!! Lang::get('messages.lv_plots_layouts_bathrooms') !!}<br>{!! Lang::get('messages.lv_plots_layouts_parking') !!} 3 {!! Lang::get('messages.lv_plots_layouts_parking_unit') !!} | 1 {!! Lang::get('messages.lv_plots_layouts_maid_rooms') !!}</p>
                                        <p>{!! Lang::get('messages.lv_plots_layouts_usable_area') !!}: 469.9 {!! Lang::get('messages.lv_plots_layouts_sqm') !!}</p>
                                        <p>{!! Lang::get('messages.lv_plots_layouts_land_area') !!}: 159.18 - 250.52 {!! Lang::get('messages.lv_plots_layouts_sqw') !!}</p>
                                    </div>
                                </div>
                                <div class="col-md-8 pull-md-4 col-xl-7 pull-xl-4">
                                </div>
                            </div>
                            <div class="layout">
                                <div class="layout-inner">
                                    <img src="/img/luxury/layouts/villa-triangle.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="layout-gable-os" ms-tab-content>
                        <div class="container">
                            <div class="row no-gutters">
                                <div class="col-md-4 push-md-8 col-xl-4 push-xl-7 offset-xl-1">
                                    <div class="tab-caption">
                                        <h5>VILLA GABLE ON SLOPE</h5>
                                        <p>4 {!! Lang::get('messages.lv_plots_layouts_bedrooms') !!} | 5 {!! Lang::get('messages.lv_plots_layouts_bathrooms') !!}<br>{!! Lang::get('messages.lv_plots_layouts_parking') !!} 3 {!! Lang::get('messages.lv_plots_layouts_parking_unit') !!} | 2 {!! Lang::get('messages.lv_plots_layouts_maid_rooms') !!}</p>
                                        <p>{!! Lang::get('messages.lv_plots_layouts_usable_area') !!}: 523.88 {!! Lang::get('messages.lv_plots_layouts_sqm') !!}</p>
                                        <p>{!! Lang::get('messages.lv_plots_layouts_land_area') !!}: 170.08 - 203.99 {!! Lang::get('messages.lv_plots_layouts_sqw') !!}</p>
                                    </div>
                                </div>
                                <div class="col-md-8 pull-md-4 col-xl-7 pull-xl-4">
                                </div>
                            </div>
                            <div class="layout">
                                <div class="layout-inner">
                                    <img src="/img/luxury/layouts/villa-gable-on-slope.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="layout-triangle-os" ms-tab-content>
                        <div class="container">
                            <div class="row no-gutters">
                                <div class="col-md-4 push-md-8 col-xl-4 push-xl-7 offset-xl-1">
                                    <div class="tab-caption">
                                        <h5>VILLA TRIANGLE ON SLOPE</h5>
                                        <p>4 {!! Lang::get('messages.lv_plots_layouts_bedrooms') !!} | 5 {!! Lang::get('messages.lv_plots_layouts_bathrooms') !!}<br>{!! Lang::get('messages.lv_plots_layouts_parking') !!} 3 {!! Lang::get('messages.lv_plots_layouts_parking_unit') !!} | 2 {!! Lang::get('messages.lv_plots_layouts_maid_rooms') !!}</p>
                                        <p>{!! Lang::get('messages.lv_plots_layouts_usable_area') !!}: 461.94 {!! Lang::get('messages.lv_plots_layouts_sqm') !!}</p>
                                        <p>{!! Lang::get('messages.lv_plots_layouts_land_area') !!}: 161.85 - 213.43 {!! Lang::get('messages.lv_plots_layouts_sqw') !!}</p>
                                    </div>
                                </div>
                                <div class="col-md-8 pull-md-4 col-xl-7 pull-xl-4">
                                </div>
                            </div>
                            <div class="layout">
                                <div class="layout-inner">
                                    <img src="/img/luxury/layouts/villa-triangle-on-slope.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- luxury-vdo -->
    <section class="luxury-vdo">
        <div class="container-fluid" ms-scroll-trigger>
            <div class="headline text-center">
                <h2 class="text-uppercase">
                    <span>{!! Lang::get('messages.lv_vdo_title_s1') !!}</span>{!! Lang::get('messages.lv_vdo_title_s2') !!}
                </h2>
            </div>
            <div ms-tab>
                <ul class="tab-header-container tab-header-button">
                    <li><a href="#" class="btn" data-trigger="#vdo-gable"><span>VILLA GABLE</span></a></li>
                    <li><a href="#" class="btn" data-trigger="#vdo-triangle"><span>VILLA TRIANGLE</span></a></li>
                </ul>
                <div class="tab-content-container">
                    <div id="vdo-gable" ms-tab-content>
                        <div class="tab-vdo">
                            <div class="c-block ratio-video">
                                <iframe data-src="https://www.youtube.com/embed/D-6k6VsjsUU?showinfo=0&rel=0&autoplay=0&enablejsapi=1" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                    <div id="vdo-triangle" ms-tab-content>
                        <div class="tab-vdo">

                            <div class="c-block ratio-video">
                                <iframe data-src="https://www.youtube.com/embed/0Inh-ZrNiJ8?showinfo=0&rel=0&autoplay=0&enablejsapi=1" frameborder="0"></iframe>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- luxury-faq -->
    <section class="luxury-faq faqs">
        <div class="container" style="display: none" ms-scroll-trigger>
            <div class="headline">
                <h2 class="text-uppercase">
                    FAQS
                </h2>
            </div>
            <div class="faq-container">
                <ul ms-accordion>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt>
                            <span>How is the MahaSamutr Villas development different from others?</span>
                        </a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>MahaSamutr Villas are a resort style villa featuring bespoke design and landscaped elements. Every Villa is carefully placed to be on the water’s edge, enjoying stunning view of MahaSamutr Lagoon. </p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>What is the total area of Villas Development</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>Approximately 86,400 sq.m (54 Rai).</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>How many villas are there in the project?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>MahaSamutr consists a total of 80 award-winning luxury villas.</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>What is the Villas tenure?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>Freehold</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>Who is the developer of MahaSamutr Villas?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>PACE Development Corporation Plc.</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>Who is the main contractor for MahaSamutr?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>Thai Obayashi.</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>Who is the architect of the MahaSamutr Villas?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>A49</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>Who is the interior designer for the MahaSamutr Villas?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>IA49</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>How many Villa types are there?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>There are 4 villa types, defined by the roof design f and layers of land. They are Gable, Gable on Slope, Triangle and Triangle on Slope</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>How many rooms and parking spaces are available for each Villa?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>4 Bedrooms / 5 Bathrooms and 3 Parking Spaces</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>What is Total Villa Area?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>159-250 sq.wah</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>What is Total Usable Area?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>447-587 sq.m </p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>Is the Villa sold fully furnished?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>TEach villa is sold fully fitted. Villa owners have the option to design and decorate their villa based on their preferences.</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>When is the MahaSamutr Villas development expected to be completed?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>MahaSamutr Villas is expected to be completed approximately Q4/2017</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>When will the Villas be ready for owner move-in?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>MahaSamutr Villas will start ownership transfer in Q3/2017.</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>What is the starting price of the Villa?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>Villa prices start from 51 Million THB</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>How much is a Maintenance Fee?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>THB 100 per sq.wah</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>How much is a Sinking Fund?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>THB 2,000 per sq.wah</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>Will there be any membership maintenance fee apply for villa owners?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>Villa owners will be automatically endorsed as MahaSamutr Country Club members without the entry fee charged. Nonetheless, there will be an annual fee apply for membership maintenance.</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>Who should I contact for a site visit?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>You may contact our agent for a site visit by calling 032 907 900 or email mahasamutr@cbre.co.th</p>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-list">
                        <a class="accordion-title" href="#" ms-accordion-bt><span>Can I purchase a villa directly from PACE Development, or do I need to buy through the sole agent?</span></a>
                        <div class="accordion-content" ms-accordion-node>
                            <div class="accordion-content-inner">
                                <p>MahaSamutr Villas are sold through our sole agent, CBRE Thailand. Please contact CBRE at www.cbre.co.th or Tel. + 66 (0) 32 907 900 or email mahasamutr@cbre.co.th </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
@endsection

@section('script')

@section('modal')
