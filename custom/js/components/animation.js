var b_animation = {
  controller: '',
  defaultTrigger: 0.8,
  animSelector: '[b-animation]',
  triggerAttr: 'b-anim-trigger',
  replaySelector: '[b-anim-replay]',
  activeClass: 'active',
  deactiveClass: 'de-active',
  init: function(){
    var triggerSelector = '[' + b_animation.triggerAttr + ']';

    if($(triggerSelector).length){
      b_animation.controller = new ScrollMagic.Controller();

      $(triggerSelector).each(function(){
        var trigger = $(this);
        var triggerHook = B_.GET_NUMBER(trigger.attr(b_animation.triggerAttr),b_animation.defaultTrigger);
        var anim = trigger.find(b_animation.animSelector).add(trigger.filter(b_animation.animSelector));
        var attentionAnim = anim.filter('*:not([b-animation*="in"],[b-animation*="out"])');

        var scene = new ScrollMagic.Scene({
                  triggerElement: this,
                  triggerHook: triggerHook
                })
                .on("enter leave", function (e) {
                  switch(e.type){
                    case "enter":
                      anim.addClass(b_animation.activeClass);
                      if(trigger.is(b_animation.replaySelector)){
                        anim.removeClass(b_animation.deactiveClass);
                      }
                      break;
                    case "leave":
                      if(trigger.is(b_animation.replaySelector) && e.scrollDirection == "REVERSE"){
                        anim.addClass(b_animation.deactiveClass);
                        attentionAnim.removeClass(b_animation.activeClass).removeClass(b_animation.deactiveClass);
                      }
                      break;
                  }
      					})
                // .addIndicators({name: "1 (duration: 0)"})
                .addTo(b_animation.controller);
      });
    }
  }
}

site.load.push(b_animation.init);
