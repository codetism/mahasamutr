var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
    "jquery": "./vendor/bower_components/jquery/dist/",
    "bootstrap": "./vendor/bower_components/bootstrap-sass-official/assets/",
    "datatables": "./vendor/bower_components/datatables/media/",
    "datatables_bs3": "./vendor/bower_components/datatables-bootstrap3/BS3/assets/",
    "boostrap_tokenfield": './vendor/bower_components/bootstrap-tokenfield/dist/',
    "jquery_ui": './vendor/bower_components/jquery-ui/',
    "select2": './vendor/bower_components/select2/dist/',
    "home_assets": './resources/assets/',
};

elixir(function(mix) {
    mix.less('app.less');

    /**
     * jquery ui
     **/
    mix.scripts('jquery-ui.js', 'public/jquery-ui/jquery-ui.js', paths.jquery_ui+'ui/');
    mix.styles('jquery-ui.css', 'public/jquery-ui/jquery-ui.css',paths.jquery_ui + 'themes/base/');
    mix.copy(paths.jquery_ui + 'themes/base/images/', 'public/jquery-ui/images');

    /**
     * Custom
     **/
    mix.copy(paths.home_assets + 'css', 'public/css');
    mix.copy(paths.home_assets + 'favicon', 'public/favicon');
    mix.copy(paths.home_assets + 'fonts', 'public/fonts');
    mix.copy(paths.home_assets + 'img', 'public/img');
    mix.copy(paths.home_assets + 'js', 'public/js');
    mix.copy(paths.home_assets + 'video', 'public/video');
    mix.copy(paths.home_assets + 'custom', 'public/custom');
    mix.copy(paths.home_assets + 'download', 'public/download');

    mix.copy('public/css', 'css');
    mix.copy('public/favicon', 'favicon');
    mix.copy('public/fonts', 'fonts');
    mix.copy('public/img', 'img');
    mix.copy('public/js', 'js');
    mix.copy('public/video', 'video');
    mix.copy('public/custom', 'custom');
    mix.copy('public/download', 'download');

    mix.phpUnit();
});
